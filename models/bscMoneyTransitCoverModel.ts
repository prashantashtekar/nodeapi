import mongoose, { Document, Model, Schema } from "mongoose";
import { ITenantAware, tenantAwareModel, tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IBscMoneyTransitCover:
 *       type: object
 *       required:
 *         - transitFrom
 *         - transitTo
 *         - singleCarryingLimit
 *         - annualTurnover
 *         - total
 *       properties:
 *         transitFrom:
 *           type: string
 *         transitTo:
 *           type: string
 *         singleCarryingLimit:
 *           type: number
 *         annualTurnover:
 *           type: number
 *         total:
 *           type: number
 */

// Base interface
export interface IBscMoneyTransitCover {
  transitFrom: string;
  transitTo: string;
  singleCarryingLimit: Number;
  annualTurnover: Number;
  total: Number;
  clientLocationId: Schema.Types.ObjectId;
  quoteId:Schema.Types.ObjectId;
}

// IDocument
export interface IBscMoneyTransitCoverDocument extends IBscMoneyTransitCover, ITenantAware, Document {}

// IModel
export interface IBscMoneyTransitCoverModel extends Model<IBscMoneyTransitCoverDocument> {}

// Schema
const BscMoneyTransitCoverSchema = new Schema<IBscMoneyTransitCoverDocument, IBscMoneyTransitCoverModel>({
  transitFrom: {
    type: String,
    required: [true, "Money In Transit should have Transit From"]
  },
  transitTo: {
    type: String,
    required: [true, "Money In Transit should have Transit To"]
  },
  singleCarryingLimit: {
    type: Number
  },
  annualTurnover: {
    type: Number
  },
  total: {
    type: Number
  },
  clientLocationId: {
    type: Schema.Types.ObjectId,
    ref: "ClientLocation",
    required: [true, "Client location is required"]
  },
  quoteId: {
    type: Schema.Types.ObjectId,
    ref: "QuoteSlip",
    required: [true, "Quote is required"]
  },
});

// Model
// export const CityMaster = mongoose.model<IBscFireLossOfProfitCoverDocument, IBscFireLossOfProfitCoverModel>('BscFireLossOfProfitCover', BscFireLossOfProfitCoverSchema);
export const BscMoneyTransitCover = tenantAwareModel<IBscMoneyTransitCoverDocument, IBscMoneyTransitCoverModel>("BscMoneyTransitCover", BscMoneyTransitCoverSchema);

// Initialize the model once.
BscMoneyTransitCover({ skipTenant: true });
