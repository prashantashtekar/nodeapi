import mongoose, { Document, Model, Schema } from "mongoose";
import validator from "validator";
import { tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IProductMaster:
 *       type: object
 *       required:
 *         - type
 *         - category
 *         - status
 *       properties:
 *         type:
 *           type: string
 *         category:
 *           type: string
 *         status:
 *           type: boolean
 */

// Base interface
export interface IProductMaster {
  type: string;
  category: string;
  status: boolean;
}

// IDocument
/** Contains the instance methods as and when we add them. */
export interface IProductMasterDocument extends IProductMaster, Document {}

// IModel
/** Contains the static methods as and when we add them. */
export interface IProductMasterModel extends Model<IProductMasterDocument> {}

// Schema
const ProductMasterSchema = new Schema<IProductMasterDocument, IProductMasterModel>({
  type: {
    type: String,
    unique: true
  },
  category: {
    type: String
  },
  status: {
    // type: String // TODO: Requires boolean in list component to show active icon
    type: Boolean
  }
});

// Model
export const ProductMaster = tenantUnawareModel<IProductMasterDocument, IProductMasterModel>("ProductMaster", ProductMasterSchema);

// Initialize the model once.
ProductMaster();
