import mongoose, { Document, Model, Schema } from "mongoose";
import validator from "validator";
import { ITenantAware, tenantAwareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IQuoteSlip:
 *       type: object
 *       required:
 *         - quoteNo
 *         - quoteType
 *         - renewalPolicyPeriod
 *         - insurredBusiness
 *         - status
 *         - deductiblesExcess
 *         - existingBrokerCurrentYear
 *         - otherTerms
 *         - additionalInfo
 *         - claim1NoOfClaims
 *         - claim1Nature
 *         - approvedBy
 *         - approvedOn
 *         - createdBy
 *         - createdOn
 *         - clientAddress
 *         - totalIndictiveQuoteAmt
 *         - brokerages
 *         - targetPremium
 *         - claimYear1
 *         - claimToYear1
 *         - claim1PremiumPaid
 *         - claim1ClaimAmount
 *         - revNo
 *         - sameAsPremium
 *         - quoteSubmissionDate
 *         - sectorId
 *         - clientId
 *         - productId
 *       properties:
 *         quoteNo:
 *           type: string
 *         quoteType:
 *           type: Schema.Types.ObjectId
 *         renewalPolicyPeriod:
 *           type: string
 *         insurredBusiness:
 *           type: string
 *         status:
 *           type: string
 *         deductiblesExcess:
 *           type: string
 *         existingBrokerCurrentYear:
 *           type: string
 *         otherTerms:
 *           type: string
 *         additionalInfo:
 *           type: string
 *         claim1NoOfClaims:
 *           type: string
 *         claim1Nature:
 *           type: string
 *         approvedBy:
 *           type: string
 *         approvedOn:
 *           type: string
 *         createdBy:
 *           type: string
 *         createdOn:
 *           type: string
 *         clientAddress:
 *           type: string
 *         totalIndictiveQuoteAmt:
 *           type: number
 *         brokerages:
 *           type: number
 *         targetPremium:
 *           type: number
 *         claimYear1:
 *           type: number
 *         claimToYear1:
 *           type: number
 *         claim1PremiumPaid:
 *           type: number
 *         claim1ClaimAmount:
 *           type: number
 *         revNo:
 *           type: number
 *         sameAsPremium:
 *           type: boolean
 *         quoteSubmissionDate:
 *           type: date
 *         sectorId:
 *           type: Schema.Types.ObjectId
 *         clientId:
 *           type: Schema.Types.ObjectId
 *         productId:
 *           type: Schema.Types.ObjectId
 */
// Base interface
export interface IQuoteSlip {
  quoteNo: string;
  quoteType: string;
  renewalPolicyPeriod: string;
  insurredBusiness: string;
  status: string;
  deductiblesExcess: string;
  existingBrokerCurrentYear: string;
  otherTerms: string;
  additionalInfo: string;
  claim1NoOfClaims: string;
  claim1Nature: string;
  approvedBy: string;
  approvedOn: string;
  createdBy: string;
  createdOn: string;
  clientAddress: string;
  totalIndictiveQuoteAmt: number;
  brokerages: number;
  targetPremium: number;
  claimYear1: number;
  claimToYear1: number;
  // remaining from ClaimYear2 to Claim3Nature, ParentId, PreferredInsurer
  claim1PremiumPaid: number;
  claim1ClaimAmount: number;
  revNo: number;

  sameAsPremium: boolean;

  quoteSubmissionDate: Date;

  sectorId: Schema.Types.ObjectId;
  clientId: Schema.Types.ObjectId;
  productId: Schema.Types.ObjectId;
  // pincodeId: Schema.Types.ObjectId;
  // preferredInsurer: Schema.Types.ObjectId;
  ageOfBuilding: string;
  constructionType: string;
  yearLossHistory: string;
  fireProtection: string;
  amcFireProtection: Boolean;
  distanceBetweenFireBrigade: String;
  premises: string;
}

// IDocument
/** Contains the instance methods as and when we add them. */
export interface IQuoteSlipDocument extends IQuoteSlip, Document, ITenantAware {}

// IModel
/** Contains the static methods as and when we add them. */
export interface IQuoteSlipModel extends Model<IQuoteSlipDocument> {}

// Schema
const QuoteSlipSchema = new Schema<IQuoteSlipDocument, IQuoteSlipModel>({
  quoteNo: {
    type: String
    // unique: true,
  },
  quoteType: {
    type: String,
    required: [true, "Quote type is required"],
    enum: {
      values: ["new", "existing"],
      message: "Quote type is either: new or existing."
    }
  },
  renewalPolicyPeriod: {
    type: String
  },
  insurredBusiness: {
    type: String
  },
  deductiblesExcess: {
    type: String
  },
  existingBrokerCurrentYear: {
    type: String
  },
  otherTerms: String,
  additionalInfo: String,
  claim1NoOfClaims: String,
  claim1Nature: String,
  approvedBy: String,
  approvedOn: String,
  createdBy: String,
  createdOn: String,
  clientAddress: String,
  status: String,

  totalIndictiveQuoteAmt: {
    type: Number
    // min: 1,
    // max: 5,
  },
  brokerages: {
    type: Number
  },
  targetPremium: Number,
  claimYear1: Number,
  claimToYear1: Number,
  claim1PremiumPaid: Number,
  claim1ClaimAmount: Number,
  revNo: Number,
  sameAsPremium: Boolean,
  quoteSubmissionDate: Date,

  sectorId: {
    type: Schema.Types.ObjectId,
    // ref: "Sector", // TODO: Relation Collection Require SectorMaster
    ref: "SectorMaster",
    required: [true, "Sector is required"]
  },
  clientId: {
    type: Schema.Types.ObjectId,
    ref: "Client",
    required: [true, "Client is required"]
  },
  productId: {
    type: Schema.Types.ObjectId,
    ref: "Product"
  }
  // pincodeId: {
  //   type: Schema.Types.ObjectId,
  //   ref: 'PincodeMaster',
  //   required: [true, 'Partner must have a pincode'],
  // },
});


// Model
// export const Partner = mongoose.model<IPartnerDocument, IPartnerModel>('Partner', PartnerSchema);
export const QuoteSlip = tenantAwareModel<IQuoteSlipDocument, IQuoteSlipModel>("QuoteSlip", QuoteSlipSchema);

// Initialize the model once.
QuoteSlip({ skipTenant: true });
