import mongoose, { Document, Model, Schema } from "mongoose";
import { ITenantAware, tenantAwareModel, tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IBscLiabilitySectionCover:
 *       type: object
 *       required:
 *         - riskType
 *         - riskDescription
 *         - sumInsured
 *         - total
 *       properties:
 *         riskType:
 *           type: string
 *         riskDescription:
 *           type: string
 *         sumInsured:
 *           type: number
 *         total:
 *           type: number
 */

// Base interface
export interface IBscLiabilitySectionCover {
  riskType: string;
  riskDescription: string;
  sumInsured: Number;
  total: Number;
  clientLocationId: Schema.Types.ObjectId;
  quoteId:Schema.Types.ObjectId;
}

// IDocument
export interface IBscLiabilitySectionCoverDocument extends IBscLiabilitySectionCover, ITenantAware, Document {}

// IModel
export interface IBscLiabilitySectionCoverModel extends Model<IBscLiabilitySectionCoverDocument> {}

// Schema
const BscLiabilitySectionCoverSchema = new Schema<IBscLiabilitySectionCoverDocument, IBscLiabilitySectionCoverModel>({
  riskType: {
    type: String,
    required: [true, "Risk Type is required"]
  },
  riskDescription: {
    type: String
  },
  sumInsured: {
    type: Number
  },
  total: {
    type: Number
  },
  clientLocationId: {
    type: Schema.Types.ObjectId,
    ref: "ClientLocation",
    required: [true, "Client location is required"]
  },
  quoteId: {
    type: Schema.Types.ObjectId,
    ref: "QuoteSlip",
    required: [true, "Quote is required"]
  },
});

// Model
export const BscLiabilitySectionCover = tenantAwareModel<IBscLiabilitySectionCoverDocument, IBscLiabilitySectionCoverModel>(
  "BscLiabilitySectionCover",
  BscLiabilitySectionCoverSchema
);

// Initialize the model once.
BscLiabilitySectionCover({ skipTenant: true });
