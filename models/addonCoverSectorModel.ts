import { Document, Model, Schema } from "mongoose";
import { ITenantAware, tenantAwareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IAddOnCoverSector:
 *       type: object
 *       required:
 *         - addOnCoverId
 *         - sectorId
 *         - isApplicable
 *         - status
 *       properties:
 *         addOnCoverId:
 *           type: Schema.Types.ObjectId
 *         sectorId:
 *           type: Schema.Types.ObjectId
 *         isApplicable:
 *           type: boolean
 *         status:
 *           type: boolean
 */

export interface IAddOnCoverSector {
    addOnCoverId: Schema.Types.ObjectId;
    sectorId: Schema.Types.ObjectId;
    isApplicable: boolean;
    status: boolean;
}

export interface IAddOnCoverSectorDocument extends IAddOnCoverSector, ITenantAware, Document {}

// IModel
export interface IAddOnCoverSectorModel extends Model<IAddOnCoverSectorDocument> {}

const AddOnCoverSectorSchema = new Schema<IAddOnCoverSectorDocument, IAddOnCoverSectorModel>({
    addOnCoverId: {
        type: Schema.Types.ObjectId,
        ref: "AddOnCover",
        required: [true, "Add on coverage ID is required"]
    },
    sectorId: {
        type: Schema.Types.ObjectId,
        // ref: "Sector", // TODO: Relation Collection Require SectorMaster
        ref: "SectorMaster",
        required: [true, "Sector is required"]
    },
    isApplicable: {
        type: Boolean,
        // required: [true],
        trim: true
    },
    status: {
        type: Boolean
        // required: [true, 'Free paid flag is required'],
    }
});

// Model
export const AddOnCoverSector = tenantAwareModel<IAddOnCoverSectorDocument, IAddOnCoverSectorModel>("AddOnCoverSector", AddOnCoverSectorSchema);

// Initialize the model once.
AddOnCoverSector({ skipTenant: true });
