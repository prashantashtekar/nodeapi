import mongoose, { Document, Model, Schema } from "mongoose";
import { ITenantAware, tenantAwareModel, tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IBscFidelityGuaranteeCover:
 *       type: object
 *       required:
 *         - riskType
 *         - riskDescription
 *         - sumInsured
 *         - total
 *       properties:
 *         riskType:
 *           type: string
 *         riskDescription:
 *           type: string
 *         sumInsured:
 *           type: number
 *         total:
 *           type: number
 */

// Base interface
export interface IBscFidelityGuaranteeCover {
  riskType: string;
  riskDescription: string;
  sumInsured: Number;
  total: Number;
  clientLocationId: Schema.Types.ObjectId;
  quoteId:Schema.Types.ObjectId;
}

// IDocument
export interface IBscFidelityGuaranteeCoverDocument extends IBscFidelityGuaranteeCover, ITenantAware, Document {}

// IModel
export interface IBscFidelityGuaranteeCoverModel extends Model<IBscFidelityGuaranteeCoverDocument> {}

// Schema
const BscFidelityGuaranteeCoverSchema = new Schema<IBscFidelityGuaranteeCoverDocument, IBscFidelityGuaranteeCoverModel>({
  riskType: {
    type: String,
    required: [true, "Risk Type is required"]
  },
  riskDescription: {
    type: String
  },
  sumInsured: {
    type: Number
  },
  total: {
    type: Number
  },
  clientLocationId: {
    type: Schema.Types.ObjectId,
    ref: "ClientLocation",
    required: [true, "Client location is required"]
  },
  quoteId: {
    type: Schema.Types.ObjectId,
    ref: "QuoteSlip",
    required: [true, "Quote is required"]
  },
});

// Model
export const BscFidelityGuaranteeCover = tenantAwareModel<IBscFidelityGuaranteeCoverDocument, IBscFidelityGuaranteeCoverModel>(
  "BscFidelityGuaranteeCover",
  BscFidelityGuaranteeCoverSchema
);

// Initialize the model once.
BscFidelityGuaranteeCover({ skipTenant: true });
