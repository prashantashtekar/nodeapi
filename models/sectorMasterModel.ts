import { Document, Model, Schema } from "mongoose";
import { tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     ISectorMaster:
 *       type: object
 *       required:
 *         - name
 *       properties:
 *         name:
 *           type: string
 */
// Base interface
export interface ISectorMaster {
  name: string;
}

// IDocument
export interface ISectorMasterDocument extends ISectorMaster, Document {}

// IModel
export interface ISectorMasterModel extends Model<ISectorMasterDocument> {}

// Schema
const SectorMasterSchema = new Schema<ISectorMasterDocument, ISectorMasterModel>({
  name: {
    type: String,
    required: [true, "A Sector Master must have a name"],
    trim: true
  }
});

// Model
// export const SectorMaster = mongoose.model<ISectorMasterDocument, ISectorMasterModel>('SectorMaster', SectorMasterSchema);
export const SectorMaster = tenantUnawareModel<ISectorMasterDocument, ISectorMasterModel>("SectorMaster", SectorMasterSchema);

// Initialize the model once.
SectorMaster();
