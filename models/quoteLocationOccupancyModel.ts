import mongoose, { Aggregate, Document, Model, Schema } from "mongoose";
import validator from "validator";
import pincodeMasterController from "../controllers/pincodeMasterController";
import { AddOnCoverSector, IAddOnCoverSectorModel } from "./addonCoverSectorModel";
import { AddOnCover, IAddOnCoverModel } from "./addonCoversModel";
import { ClientLocation, IClientLocationModel } from "./clientLocationModel";
import { ITenantAware, tenantAwareModel } from "./common";
import { PincodeMaster, IPincodeMasterModel } from "./pincodeMasterModel";
import { IOccupancyDocument, IOccupancyModel, Occupancy } from "./occupancyModel";
import { IQuoteLocationAddonCoversModel, QuoteLocationAddonCovers } from "./quoteLocationAddonCoversModel";
import { IQuoteSlip, IQuoteSlipModel, QuoteSlip } from "./quoteSlipModel";
import { EarthquakeRateMaster } from "./earthquakeRateMasterModel";
import { IndustryTypeMaster } from "./industryTypeModel";
import { TerrorismRateMaster } from "./terrorismRateMasterModel";

/**
 * @swagger
 * components:
 *   schemas:
 *     IQuoteLocationOccupancy:
 *       type: object
 *       required:
 *         - clientLocationId
 *         - occupancyId
 *         - quoteId
 *         - sumAssured
 *       properties:
 *         clientLocationId:
 *           type: Schema.Types.ObjectId
 *         occupancyId:
 *           type: Schema.Types.ObjectId
 *         quoteId:
 *           type: Schema.Types.ObjectId
 *         sumAssured:
 *           type: number
 */

// Base interface
export interface IQuoteLocationOccupancy {
  clientLocationId: Schema.Types.ObjectId;
  occupancyId: Schema.Types.ObjectId;
  quoteId: Schema.Types.ObjectId;
  sumAssured: number;
  flexaPremium: number;
  STFIPremium: number;
  earthquakePremium: number;
  terrorismPremium: number;
  totalPremium: number;
}

// IDocument
/** Contains the instance methods as and when we add them. */
export interface IQuoteLocationOccupancyDocument extends IQuoteLocationOccupancy, Document, ITenantAware {
  // Instance methods go here for type checking.
  computeFlexaPremium: () => Promise<number>;
  computeStfiPremium: () => Promise<number>;
  computeEarthquakePremium: () => Promise<number>;
  computeTerrorismPremium: () => Promise<number>;
}

// IModel
/** Contains the static methods as and when we add them. */
export interface IQuoteLocationOccupancyModel extends Model<IQuoteLocationOccupancyDocument> {}

// Schema
const QuoteLocationOccupancySchema = new Schema<IQuoteLocationOccupancyDocument, IQuoteLocationOccupancyModel>({
  clientLocationId: {
    type: Schema.Types.ObjectId,
    ref: "ClientLocation",
    required: [true, "Client location is required"]
  },
  occupancyId: {
    type: Schema.Types.ObjectId,
    ref: "Occupancy",
    required: [true, "Occupancy is required"]
  },
  quoteId: {
    type: Schema.Types.ObjectId,
    ref: "QuoteSlip",
    required: [true, "Quote is required"]
  },
  sumAssured: {
    type: Number,
    required: [true, "Sum assured is required"]
  },
  flexaPremium: {
    type: Number
  },
  STFIPremium: {
    type: Number
  },
  earthquakePremium: {
    type: Number
  },
  terrorismPremium: {
    type: Number
  },
  totalPremium: {
    type: Number
  }
});

// DOCUMENT MIDDLEWARE
QuoteLocationOccupancySchema.post<IQuoteLocationOccupancyDocument>("save", async function () {
  // this points to the Quote Location Occupancy.

  const quote = await QuoteSlip({ skipTenant: false, specifiedPartnerId: this.partnerId }).findOne({ _id: this.quoteId });
  const addonCoverSectors = await AddOnCoverSector({ skipTenant: false, specifiedPartnerId: this.partnerId }).find({ sectorId: quote.sectorId });
  if (addonCoverSectors) {
    for (let i = 0; i < addonCoverSectors.length; i++) {
      const addonCoverSector = addonCoverSectors[i];
      const addonCover = await AddOnCover({ skipTenant: false, specifiedPartnerId: this.partnerId })
        .applicableBetween()
        .find({ _id: addonCoverSector.addOnCoverId, isFree: true, productId: quote.productId });
      if (addonCover) {
        await QuoteLocationAddonCovers({ skipTenant: false, specifiedPartnerId: this.partnerId }).create({
          quoteId: this.quoteId,
          locationId: this.clientLocationId,
          addOnCoverId: addonCoverSector.addOnCoverId
        });
        const flexaPremium = await this.computeFlexaPremium();
        const STFIPremium = await this.computeStfiPremium();
        const earthquakePremium = await this.computeEarthquakePremium();
        const terrorismPremium = await this.computeTerrorismPremium();
        const totalPremium = flexaPremium + STFIPremium + earthquakePremium + terrorismPremium;
        await QuoteLocationAddonCovers({ skipTenant: false, specifiedPartnerId: this.partnerId }).findByIdAndUpdate(this.id, {
          flexaPremium: flexaPremium,
          STFIPremium: STFIPremium,
          earthquakePremium: earthquakePremium,
          terrorismPremium: terrorismPremium,
          totalPremium: totalPremium
        });
      }
    }
  }
});

// INSTANCE METHODS
QuoteLocationOccupancySchema.methods.computeFlexaPremium = async function () {
  const occupancy = await Occupancy({ skipTenant: false, specifiedPartnerId: this.partnerId }).applicableBetween().findOne({ _id: this.occupancyId });
  if (occupancy) {
    const flexa = (this.sumAssured * occupancy.flexaiib) / 100;
    return flexa;
  } else {
    return 0;
  }
};

QuoteLocationOccupancySchema.methods.computeStfiPremium = async function () {
  const occupancy = await Occupancy({ skipTenant: false, specifiedPartnerId: this.partnerId }).applicableBetween().findOne({ _id: this.occupancyId });
  if (occupancy) {
    const stfi = (this.sumAssured * occupancy.stfiRate) / 100;
    return stfi;
  } else {
    return 0;
  }
};

QuoteLocationOccupancySchema.methods.computeEarthquakePremium = async function () {
  const quote = await QuoteSlip({ skipTenant: false, specifiedPartnerId: this.partnerId }).findOne({ _id: this.quoteId });
  const clientLocation = await ClientLocation({ skipTenant: false, specifiedPartnerId: this.partnerId }).findOne({ _id: this.clientLocationId });
  const pincode = await PincodeMaster().findOne({ _id: clientLocation.pincodeId });
  const occupancy = await Occupancy({ skipTenant: false, specifiedPartnerId: this.partnerId }).applicableBetween().findOne({ _id: this.occupancyId });
  const industryType = await IndustryTypeMaster().findOne({ _id: occupancy.industryTypeId });
  const earthquakeZone = await EarthquakeRateMaster({ skipTenant: false, specifiedPartnerId: this.partnerId }).applicableBetween().findOne({
    zone: pincode.earthquakeZone,
    industryTypeId: industryType._id,
    productId: quote.productId
  });
  if (earthquakeZone) {
    const earthquake = (this.sumAssured * earthquakeZone.rate) / 100;
    return earthquake;
  } else {
    return 0;
  }
};

QuoteLocationOccupancySchema.methods.computeTerrorismPremium = async function () {
  const quote = await QuoteSlip({ skipTenant: false, specifiedPartnerId: this.partnerId }).findOne({ _id: this.quoteId });
  const terrorismRate = await TerrorismRateMaster({ skipTenant: false, specifiedPartnerId: this.partnerId })
    .applicableBetween()
    .findOne({
      fromSI: { $lte: this.sumAssured },
      toSI: { $gte: this.sumAssured },
      productId: quote.productId
    });
  if (terrorismRate) {
    const terrorism = (this.sumAssured * terrorismRate.ratePerMile) / 100;
    return terrorism;
  } else {
    return 0;
  }
};

// Model
export const QuoteLocationOccupancy = tenantAwareModel<IQuoteLocationOccupancyDocument, IQuoteLocationOccupancyModel>("QuoteLocationOccupancy", QuoteLocationOccupancySchema);

// Initialize the model once.
QuoteLocationOccupancy({ skipTenant: true });
