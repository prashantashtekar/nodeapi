import mongoose, { Document, Model, Schema } from "mongoose";
import validator from "validator";
import { tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IStateMaster:
 *       type: object
 *       required:
 *         - name
 *         - countryId
 *       properties:
 *         name:
 *           type: string
 *         countyId:
 *           type: Schema.Types.ObjectId
 */
// Base interface
export interface IStateMaster {
  name: string;
  countryId: Schema.Types.ObjectId;
}

// IDocument
export interface IStateMasterDocument extends IStateMaster, Document {}

// IModel
export interface IStateMasterModel extends Model<IStateMasterDocument> {}

// Schema
const StateMasterSchema = new Schema<IStateMasterDocument, IStateMasterModel>({
  name: {
    type: String,
    required: [true, "A State must have a name"],
    unique: true,
    trim: true,
    maxLength: [128, "A State name must have less than or equal to 128 characters."]
  },
  countryId: {
    type: Schema.Types.ObjectId,
    ref: "CountryMaster",
    required: [true, "State must have a country"]
  }
});

// Model
// export const StateMaster = mongoose.model<IStateMasterDocument, IStateMasterModel>('StateMaster', StateMasterSchema);
export const StateMaster = tenantUnawareModel<IStateMasterDocument, IStateMasterModel>("StateMaster", StateMasterSchema);

// Initialize the model once.
StateMaster();
