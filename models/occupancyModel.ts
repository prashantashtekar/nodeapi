import { Document, Model, Query, Schema } from "mongoose";
import { applicableBetween, IApplicableDateRangeAware, ITenantAware, tenantAwareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IOccupancy:
 *       type: object
 *       required:
 *         - occupancyType
 *         - hazardCategoryId
 *         - gradedRetention
 *         - tacCode
 *         - flexaiib
 *         - stfiRate
 *         - section
 *         - riskCode
 *         - rateCode
 *         - policyType
 *         - specialRemark
 *         - specialExcessGIC
 *         - iibOccupancyCode
 *         - effectiveFrom
 *         - effectiveTo
 *         - productId
 *         - applicableFrom
 *         - applicableTo
 *       properties:
 *         occupancyType:
 *           type: string
 *         hazardCategoryId:
 *           type: Schema.Types.ObjectId
 *         gradedRetention:
 *           type: boolean
 *         tacCode:
 *           type: string
 *         flexaiib:
 *           type: number
 *         stfiRate:
 *           type: number
 *         section:
 *           type: string
 *         riskCode:
 *           type: number
 *         rateCode:
 *           type: number
 *         policyType:
 *           type: string
 *         specialRemark:
 *           type: string
 *         specialExcessGIC:
 *           type: string
 *         iibOccupancyCode:
 *           type: string
 *         effectiveFrom:
 *           type: date
 *         effectiveTo:
 *           type: date
 *         productId:
 *           type: Schema.Types.ObjectId
 *         applicableFrom:
 *           type: date
 *         applicableTo:
 *           type: date
 */

// Base interface
export interface IOccupancy {
  occupancyType: string;
  hazardCategoryId: Schema.Types.ObjectId;
  gradedRetention: Boolean;
  tacCode: string;
  flexaiib: number;
  stfiRate: number;
  section: string;
  riskCode: number;
  rateCode: number;
  policyType: string;
  specialRemark: string;
  specialExcessGIC: string;
  iibOccupancyCode: string;
  effectiveFrom: Date;
  effectiveTo: Date;
  productId: Schema.Types.ObjectId;
  industryTypeId: Schema.Types.ObjectId;
}

// IDocument
export interface IOccupancyDocument extends IOccupancy, Document, ITenantAware,IApplicableDateRangeAware {}

// IModel
export interface IOccupancyModel extends Model<IOccupancyDocument> {
  findByContainsSearch: (search: string, limit: number) => Promise<IOccupancyDocument[]>;
  applicableBetween: () => Query<IOccupancyDocument[], IOccupancyDocument>;
}


const OccupancySchema = new Schema<IOccupancyDocument, IOccupancyModel>({
  // Parent referencing...
  occupancyType: {
    type: String,
    required: [true, "Every Occupancy must be Type"]
  },
  hazardCategoryId: {
    type: String,
    ref: "HazardCategoryMaster",
    required: [true, "Occupancy must have a HAzard Category"]
  },
  gradedRetention: {
    type: Boolean
  },
  tacCode: {
    type: String
  },
  flexaiib: {
    type: Number
  },
  stfiRate: {
    type: Number
  },
  section: {
    type: String,
    required: [true, "Every Occupancy must be Section"]
  },
  riskCode: {
    type: Number
  },
  rateCode: {
    type: Number
  },
  policyType: {
    type: String
  },
  specialRemark: {
    type: String
  },
  specialExcessGIC: {
    type: String
  },
  effectiveFrom: Date,
  effectiveTo: Date,
  productId: {
    type: Schema.Types.ObjectId,
    ref: "ProductMaster"
  }, 
  industryTypeId: {
    type: Schema.Types.ObjectId,
    ref: "IndustryTypeMaster"
  }
});

// STATIC METHODS

OccupancySchema.statics.findByContainsSearch = function (search: string, limit: number) {
  return this.find({ occupancyType: { $regex: ".*" + search + ".*" } }).limit(limit);
};

OccupancySchema.statics.applicableBetween = applicableBetween;

export const Occupancy = tenantAwareModel<IOccupancyDocument, IOccupancyModel>("Occupancy", OccupancySchema);

// Initialize the model once.
Occupancy({ skipTenant: true });
