import mongoose, { Document, Model, Schema } from "mongoose";
import { BscCover } from "./bscCoverModel";
import { BscDiscountCover } from "./bscDiscountCoverModel";
import { ITenantAware, tenantAwareModel, tenantUnawareModel } from "./common";
import { QuoteSlip } from "./quoteSlipModel";

/**
 * @swagger
 * components:
 *   schemas:
 *     IBscBurglaryHousebreakingCover:
 *       type: object
 *       required:
 *         - locationId
 *         - firstLoss
 *         - stocks
 *         - otherContents
 *         - firstLossSumInsured
 *         - rsmd
 *         - theft
 *         - total
 *       properties:
 *         locationId:
 *           type: Schema.Types.ObjectId
 *         firstLoss:
 *           type: Number
 *         stocks:
 *           type: Number
 *         otherContents:
 *           type: Number
 *         firstLossSumInsured:
 *           type: Number
 *         rsmd:
 *           type: boolean
 *         theft:
 *           type: boolean
 *         total:
 *           type: Number
 */

// Base interface
export interface IBscBurglaryHousebreakingCover {
  locationId: Schema.Types.ObjectId;
  firstLoss: Number;
  stocks: Number;
  otherContents: Number;
  firstLossSumInsured: Number;
  rsmd: Boolean;
  theft: Boolean;
  total: Number;
  clientLocationId: Schema.Types.ObjectId;
  quoteId: Schema.Types.ObjectId;
}

// IDocument
export interface IBscBurglaryHousebreakingCoverDocument extends IBscBurglaryHousebreakingCover, ITenantAware, Document {
  computeBurglarytotalPremium: () => Promise<number>;
}

// IModel
export interface IBscBurglaryHousebreakingCoverModel extends Model<IBscBurglaryHousebreakingCoverDocument> {}

// Schema
const BscBurglaryHousebreakingCoverSchema = new Schema<IBscBurglaryHousebreakingCoverDocument, IBscBurglaryHousebreakingCoverModel>({
  LocationId: {
    type: Schema.Types.ObjectId,
    ref: "ClientLocation",
    required: [true, "Client location is required"]
  },
  firstLoss: {
    type: Number
  },
  stocks: {
    type: Number
  },
  otherContents: {
    type: Number
  },
  firstLossSumInsured: {
    type: Number
  },
  rsmd: {
    type: Boolean
  },
  theft: {
    type: Boolean
  },
  total: {
    type: Number
  },
  clientLocationId: {
    type: Schema.Types.ObjectId,
    ref: "ClientLocation",
    required: [true, "Client location is required"]
  },
  quoteId: {
    type: Schema.Types.ObjectId,
    ref: "QuoteSlip",
    required: [true, "Quote is required"]
  }
});

// INSTANCE METHODS
BscBurglaryHousebreakingCoverSchema.methods.computeBurglarytotalPremium = async function () {
  const quote = await QuoteSlip({ skipTenant: false, specifiedPartnerId: this.partnerId }).findOne({ _id: this.quoteId });
  const bscCover = await BscCover({ skipTenant: false, specifiedPartnerId: this.partnerId })
    .applicableBetween()
    .findOne({ bscType: "Burglary & Housebreaking",productId: quote.productId, fromSI: { $lte: this.firstLossSumInsured }, toSI: { $gte: this.firstLossSumInsured } });
  // if (bscCover && this.rsmd === true && this.theft === true ) {
  //   let burglaryTotal = (Number(this.firstLossSumInsured) * Number(bscCover.ratePerMile)) / 1000;
  //   const bscDiscountCover = await BscDiscountCover({ skipTenant: false, specifiedPartnerId: this.partnerId })
  //   .applicableBetween()
  //   .findOne({ bscType: "Burglary & Housebreaking", fromSI: { $lte: this.firstLossSumInsured }, toSI: { $gte: this.firstLossSumInsured } });
  //   if (bscDiscountCover){
  //     let burglaryTotal = (Number(bscDiscountCover.discountPercentagePerMile)/1000)
  //   }

  //   return burglaryTotal;
  // }
  if (bscCover) {
    const burglaryTotal = (Number(this.firstLossSumInsured) * Number(bscCover.ratePerMile)) / 1000;
    return burglaryTotal;
  } else {
    return 0;
  }
};

// Model
// export const CityMaster = mongoose.model<IBscFireLossOfProfitCoverDocument, IBscFireLossOfProfitCoverModel>('BscFireLossOfProfitCover', BscFireLossOfProfitCoverSchema);
export const BscBurglaryHousebreakingCover = tenantAwareModel<IBscBurglaryHousebreakingCoverDocument, IBscBurglaryHousebreakingCoverModel>(
  "BscBurglaryHousebreakingCover",
  BscBurglaryHousebreakingCoverSchema
);

// Initialize the model once.
BscBurglaryHousebreakingCover({ skipTenant: true });
