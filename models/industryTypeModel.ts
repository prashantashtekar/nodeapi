import mongoose, { Document, Model, Schema } from "mongoose";
import validator from "validator";
import { tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IIndustryTypeMaster:
 *       type: object
 *       required:
 *         - industryTypeName
 *       properties:
 *         industryTypeName:
 *           type: string
 */

export interface IIndustryTypeMaster {
  industryTypeName: string;
}

export interface IIndustryTypeMasterDocument extends IIndustryTypeMaster, Document {}

export interface IIndustryTypeMasterModel extends Model<IIndustryTypeMasterDocument> {}

const IndustryTypeMasterSchema = new Schema<IIndustryTypeMasterDocument, IIndustryTypeMasterModel>({
  industryTypeName: {
    type: String
  }
});

// export const IndustryTypeMaster = mongoose.model<IIndustryTypeMasterDocument, IIndustryTypeMasterModel>('IndustryTypeMaster', IndustryTypeMasterSchema);
export const IndustryTypeMaster = tenantUnawareModel<IIndustryTypeMasterDocument, IIndustryTypeMasterModel>("IndustryTypeMaster", IndustryTypeMasterSchema);

// Initialize the model once.
IndustryTypeMaster();
