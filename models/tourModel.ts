import mongoose, { Aggregate, Model, Query, Schema, Types, Document } from "mongoose";
import slugify from "slugify";
import validator from "validator";
import { ITenantAware, tenantAwareModel } from "./common";

export interface ITourLocation {
  type: {
    type: string;
  };
  coordinates: Array<number>;
  address?: string;
  description?: string;
  day?: Number;
}

export interface ITour {
  name: string;
  slug: string;
  duration: number;
  maxGroupSize: number;
  difficulty: string;
  ratingsAverage: number;
  ratingsQuantity: number;
  price: number;
  priceDiscount: number;
  summary: string;
  description: string;
  imageCover: string;
  tourImages: Array<string>;
  createdAt: Date;
  startDates: Array<Date>;
  secretTour: Boolean;
  start: Date;
  tourStartLocation: ITourLocation;
  tourLocations: Array<ITourLocation>;
  guides: Array<Schema.Types.ObjectId>;
}

export interface ITourDocument extends ITour, ITenantAware, Document {
  tourImages: Types.Array<string>;
  startDates: Types.Array<Date>;
}

export interface ITourModel extends Model<ITourDocument> {}

// Create the tour schema.
const TourSchema = new Schema<ITourDocument, ITourModel>(
  {
    name: {
      type: String,
      required: [true, "A tour must have a name"],
      unique: true,
      trim: true,
      maxLength: [40, "A tour name must have less than or equal to 40 characters."],
      minLength: [10, "A tour name must have at-least 10 characters."],
      validate: {
        validator: val => validator.isAlpha(val, ["en-US"], { ignore: " " })
      }
    },
    slug: {
      type: String
    },
    duration: {
      type: Number,
      required: [true, "A tour must have a duration"]
    },
    maxGroupSize: {
      type: Number,
      required: [true, "A tour must have a group size"]
    },
    difficulty: {
      type: String,
      required: [true, "A tour must have a difficulty"],
      enum: {
        values: ["easy", "medium", "difficult"],
        message: "Difficulty is either: easy, medium or difficult."
      }
    },
    ratingsAverage: {
      type: Number,
      default: 4.5,
      min: [1, "Rating must be above 1"],
      max: [5, "Rating must be below 5"],
      set: val => Math.round(val * 10) / 10
    },
    ratingsQuantity: {
      type: Number,
      default: 0
    },
    price: {
      type: Number,
      required: [true, "A tour must have a price"]
    },
    priceDiscount: {
      validate: {
        validator: function (val) {
          // this only points to current doc on NEW document creation.
          return val < this.price;
        },
        message: "Discount price ({VALUE}) should be below regular price!"
      },
      type: Number
    },
    summary: {
      type: String,
      trim: true,
      required: [true, "A tour must have a description"]
    },
    description: {
      type: String,
      trim: true
    },
    imageCover: {
      type: String,
      required: [true, "A tour must have a cover image"]
    },
    images: [String],
    createdAt: {
      type: Date,
      default: Date.now(),
      select: false
    },
    startDates: [Date],
    secretTour: {
      type: Boolean
    },
    // Embedding.
    tourLocations: [
      {
        type: {
          type: String,
          default: "Point",
          enum: ["Point"]
        },
        coordinates: [Number],
        address: String,
        description: String,
        day: Number
      }
    ],
    // Embedding.
    tourStartLocation: {
      // GeoJSON: Basically to specify geo spatial information we need to create a new object like this.
      // And this nested object should have at-least 2 fields: type & coordinates.
      type: {
        type: String,
        default: "Point",
        enum: ["Point"]
      },
      // Longitude, Latitude.
      // In GeoJSON the longitude comes first, unlike what we are normally used to when using Google maps etc.
      coordinates: [Number],
      address: String,
      description: String
    },
    // Child referencing...
    guides: [
      {
        type: Schema.Types.ObjectId,
        ref: "User"
      }
    ]
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
  }
);

// Create the index.
// 1: sorting the price index asc
// -1: sorting the price index desc
TourSchema.index({ slug: 1 });
TourSchema.index({ price: 1, ratingsAverage: -1 });

// VIRTUALS
TourSchema.virtual("durationWeeks").get(function (this: ITourDocument) {
  return this.duration / 7;
});

TourSchema.virtual("reviews", {
  ref: "Review",
  // This is the field which is a parent refernce in the Review model.
  foreignField: "tour",
  // The parent reference in the Review model comes from the "_id" field in the local (current) model.
  localField: "_id"
});

// DOCUMENT MIDDLEWARE: runs before .save() and .create(), but not before insertMany()
TourSchema.pre<ITourDocument>("save", function (next) {
  // console.log(this);
  this.slug = slugify(this.name, { lower: true });
  next();
});

// TourSchema.pre<ITourDocument>('save', async function (next) {
//     const guidesPromises = this.guides.map(async guide => await User.findById(guide));
//     this.guides = await Promise.all(guidesPromises);
//     next();
// })

// QUERY MIDDLEWARE
// TourSchema.pre('find', function(next) {
TourSchema.pre<Query<ITourDocument, ITourDocument>>(/^find/, function (next) {
  this.find({ secretTour: { $ne: true } });
  // this.start = new Date();
  next();
});

TourSchema.pre<Query<ITourDocument, ITourDocument>>(/^find/, function (next) {
  this.populate({
    path: "guides",
    select: "-__v -passwordChangedAt"
  });

  next();
});

TourSchema.post<Query<ITourDocument, ITourDocument>>(/^find/, function (docs, next) {
  // console.log(`Query took: ${Date.now()-this.start}ms!`);
  // console.log(docs);
  next();
});

// AGGREGATION MIDDLEWARE
TourSchema.pre<Aggregate<ITourDocument>>("aggregate", function (next) {
  // this: points to the current aggregation object.
  // console.log(this);
  console.log("Pipeline before:", this.pipeline());

  this.pipeline().unshift({ $match: { secretTour: { $ne: true } } });

  console.log("Pipeline after:", this.pipeline());
  next();
});

// INSTANCE METHODS

// STATIC METHODS

// Now create the model
export const Tour = tenantAwareModel<ITourDocument, ITourModel>("Tour", TourSchema);

// Initialize the model once.
Tour({ skipTenant: true });
