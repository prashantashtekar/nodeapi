export class IndustryType {
  id: string;
  type: string;
}

export class BusinessInterruption {
  grossProfitFlop: number = 0
  indmendityPeriodFlop: number = 0
  grossProfitMblop: number = 0
  indmendityPeriodMblop: number = 0
}

export class SelectedInsurer {
  id: string = "";
  name: string = ""
  iCEntityId: string = ""
  product: Product = new Product();
  warrantyExclusion: WarrantyExclusion[] = [];
  insurrerDiscount: number = 0;
  insurrerTerms: string = ""
  status: string = ""
  isQuotePlaced: boolean = false;
  approverType: string = ""
  icApproversLst: ICApprovers[] = []
  rmContactId: string = ""
  rmContactEmail: string = ""
  isRmApproved: boolean = true;
}
export class WarrantyExclusion {
    id: string;
    name: string;
    type: string;
    isSelected: boolean;
}
export class ICApprovers {
  level: number = 1;
  isApproved: boolean = false;
  status: string = ""
}

export class ProductMaster {
  id: string = "";
  type: string = "";
  status: boolean = true;
  category: string = "";
}

export class Product {
  productId: string = "";
  productName: string = "";
  //quoteSlipDetails
  productLoctionOccpuancy: ProductLoctionOccpuancy[] = [];
  totalSumAssured: number = 0;
}


export class RFQGapAnalysis {
  id: string;
  riskTypeId: string;
  industryId: string;
  rFQCount: number;
  sectoralAvg: number;
  marketAvg: number;
}

export class CoverageGapAnalysis {
  id: string;
  riskTypeId: string;
  industryId: string;
  coveragesId: string;
  count: number;
  sectoralAvg: number;
  marketAvg: number;
}

export class ProductLoctionOccpuancy {
  address: string = "";
  locationId: string = "";
  cityId: string = "";
  city: string = "";
  districtId: string = "";
  district: string = "";
  pinCode: string = "";
  occupancyId: string = "";
  occupancy: string = "";
  section: string;
  sumAssured: number = 0;
  riskCode: string = "";
  isFire: boolean = true;
  isEarthQuake: boolean = true;
  isSTFI: boolean = true;
  isTerrorism: boolean = true;
  isChecked: boolean = false;
  filteredSectionWiseOccupancyList: OccupancyMaster[] = [];
  locationWiseAddons: AddOnCoverage[] = [];
  locationWiseAddonsBi: AddOnCoverage[] = [];
  locationwiseDeclaration: Declaration[] = [];
  locationWiseRiksManagement: RiskMgmtFeature[] = [];
  flexaiib: number = 0;
  stfiRate: number = 0;
  eqRate: number = 0;
  terrorismRate: number = 0
  firePremium: number = 0;
  eqPremium: number = 0;
  terrorismPremium: number = 0;
  stfiPremium: number = 0;
  totalPremium: number = 0;

  policyRate: number = 0
  isHighestLocaion: boolean = true;

  businessInterruption: BusinessInterruption = new BusinessInterruption()
  reductionSumInsured: number = 0
  reductionSumInsuredDiscount: number = 0
}
export class HypothecationDetail {
  details: string = "";
}

export class ClientLocationMaster {
  id: string;
  clientId: string;
  address: string;
  cityId: string;
  districtId: string;
  stateId: string;
  countryId: string;
  pinCode: string;
  eqZone: string;
}
export class OccupancyMaster {
  id: string = "";
  occupancyType: string = "";
  hazardCategoryId: string = "";
  gradedRetention: boolean = false;
  tacCode: string = "";
  flexaiib: number = 0;
  stfiRate: number = 0;
  section: string = "";
  riskCode: number = 0;
  rateCode: number = 0;
  policyType: string = "";
  specialRemark: string = "";
  specialExcessGIC: string = "";
  iIBOccupancyCode: string = "";
  effectiveFrom: Date;
  effectiveTo: Date;
}
export class AllMastersModel {
  addOnCoverage: AddOnCoverage[] = [];
  declaration: Declaration[] = [];
  riskMgmtFeature: RiskMgmtFeature[] = [];
  occupancyMaster: OccupancyMaster[] = [];
  industryType: IndustryType[] = [];
  product: ProductMaster[] = [];
  pincodeEqMaster: PincodeEqMaster[] = [];
  policyRateMaster: PolicyRateMaster[] = [];
  freeUpToMaster: FreeUpToMaster[] = [];
  terrorismRateMaster: TerrorismRateMaster[] = [];
  hazardCategoryMaster: HazardCategoryMaster[] = [];
  entity: Entity[] = [];
  earthQuakeRateMaster: EarthQuakeRateMaster[] = [];

}
export class AddOnCoverage {
  public id: string = "";
  public policyType: string = "";
  public addonType: string = "";
  public category: string = "";
  public subCategory: string = "";
  public name: string = "";
  public wording: string = "";
  public freePaidFlag: string = "";
  public isTariff: boolean = false;
  public claimClause: string = "";
  public monthOrDays: boolean = false;
  public sILimit: boolean = false;
  public rate: string = "";
  public categoryOfImportance: string = "";
  public sequenceNo: number = 0;
  public entityTypeId: string = "";
  public sumInsured: number = 0;
  public isSelected: boolean = false;
  public rateType: number = 0;
  public rateParamater: number = 0;
  public freeUptoFlag: number = 0;
  public freeUptoParamater: number = 0;
  public calculatedIndicativePremium: number = 0;
  addOnCoverageIndustry: AddOnCoverageIndustryChild[] = [];
}

export class AddOnCoverageIndustryChild {
  public industryTypeId: string = "";
  public industryType: string = "";
  public isApplicable: boolean = false;
  public status: boolean = false;
}

export class LocationWiseAddons {
  locationid: string = "";
  city: string = "";
  isFire: boolean = true;
  isEarthQuake: boolean = true;
  isSTFI: boolean = true;
  isTerrorism: boolean = true;
  fireRate: number = 0;
  stfiRate: number = 0;
  eqRate: number = 0;
  terrorismRate: number = 0;
  addOnCoverage: AddOnCoverage[] = [];
  gid: string = "";
}

export class CityMaster {
  id: string = "";
  name: string = "";
  districtId: string = "";
  pinCode: string = "";
  eqZone: string = "";
  status: string = "";
}

export class DistrictMaster {
  id: string;
  stateId: string;
  name: string;
  status: boolean;
  eqZone: string;
}

export class StateMaster {
  id: string;
  countryId: string;
  name: string;
  code: string;
  status: boolean;
}

export class RiskMgmtFeature {
  id: string;
  name: string;
  industryTypeId: string;
  sequenceNumber: number;
  isSelected: boolean = false;
  locationId: string = "";
  city: string = "";
  gid: string = "";
}
export class LocationWiseRiskMgmtFeature {
  locationid: string = "";
  city: string = "";
  pincode: string = "";
  gid: string = "";
  locationRiskMgmt: RiskMgmtFeature[] = [];
}

export class Declaration {
  id: string;
  category: string;
  name: string;
  cost: number;
  wording: string;
  sequenceNumber: number;
  isBma: boolean = false;
}

export class SubCategoryHead {
  subCategory: string = "";
  subCost: number = 0;
  types: TypeCost[];
  gid: string = "";
}
export class TypeCost {
  gid: string = "";
  name: string;
  cost: number;
}

export class locationWiseDeclaration {
  locationid: string = "";
  city: string = "";
  pincode: string = "";
  gid: string = "";
  sumAssured: number = 0;
  locDeclaration: Declaration[] = [];
}

export class EarthQuakeRateMaster {
  id: string = '';
  industryType: string = '';
  zone: string = '';
  rate: number = 0;
}

export class PincodeEqMaster {
  id: string;
  pincode: string;
  earthquakeZone: string;
}

export class PolicyRateMaster {
  id: string;
  rateFlag: number;
  rateType: string;
  rateValue: number;
}

export class FreeUpToMaster {
  id: string;
  flag: number;
  description: string;
}

export class TerrorismRateMaster {
  id: string;
  industryType: string;
  fromSI: number;
  toSI: number;
  ratePerMile: number;
  addValue: number;
  remark: string;
}

export interface HazardCategoryMaster {
  id: string;
  category: string;
  score: number;
  status: boolean;
}

export class ClientGroupMaster {
  id: string;
  vINNumber: string;
  clientGroupName: string;
  clientTitle: string;
  clientGroupContactName: string;
  address: string;
  cityId: string = "";
  city: string;
  pinCode: string;
  email: string;
  isActive: boolean;
  approverUserId: string;
}

export class User {

  id: string;
  entityObjId: string;
  entityName: string;
  type: string;
  typeName: string;
  reportTo: string;
  name: string;
  email: string;
  contact: string;
  status: boolean;
  password: string;
  strId: string;
  oTP: number;
  oTPDate: Date | string;
  roleId: string;
  roleName: string;
  branchId: string;
  branchName: string;
  verticleId: string;
  verticleType: string;
  productTypeId: string;
  productType: string;
  brokerPrivileges: BrokerPrivileges = new BrokerPrivileges;
  insurrerPrivileges: InsurrerPrivileges = new InsurrerPrivileges;
  preferredInsuranceCompanyId: string = ""
}

export class BrokerPrivileges {

  BranchId: string;
  BranchName: string;
  VerticleId: string;
  VerticleType: string;
  ProductTypeId: string;
  ProductType: string;
}

export class InsurrerPrivileges {
  ProductTypeId: string;
  ProductType: string;
  GeographyId: string;
  Geography: string;
  ApproverTypeId: string;
  ApproverType: string;
}

export class Role {
  id: string;
  name: string;
  description: string;
}


export class Deductible {
  id: string;
  deductibleName: string;
  riskType: string;
  flatOrPercentage: string;
}

export class Brokerage {

  id: string;
  brokerageName: string;
  riskType: string;
  flatOrPercentage: string;
}

export class ICApproverLevels {

  id: string = "";
  riskType: string = "";
  minSI: number = 0;
  maxSI: number = 0;
  level: number = 0;
  finalLevel: number = 0;
  riskTypeId: string = ""
  entityId: string = ""
  entityName: string = ""
}

export class Entity {
  id: string = "";
  type: string = "";
  typeName: string = "";
  name: string = "";
  shortName: string = ""
  address: string = "";
  city: string = "";
  district: string = "";
  state: string = "";
  country: string = "";
  pincode: string = "";
  pAN: string = "";
  gstin: string = "";
  cin: string = "";
  contactPerson: string = "";
  designation: string = "";
  mobile: string = "";
  satus: boolean = true;
}

export class Attachment {
  public id: string = ""
  public filePath: string = ""
  public fileName: string = ""
  public file: File | null = null
  public documentType: string = ''
  public quoteid: string = ""
  public uploadedby: string = ""
  public addedDateTime: Date
  public isDeleted: boolean = false;
  public uniqueFileName: string = ""


}
export const allowedFileExtensions = ['.jpg', '.jpeg', '.png', '.pdf', '.xlsx']
export class UploadModel {
  public attachmentViewModels: Attachment[] = []
}
