import mongoose, { Document, Model, Schema } from "mongoose";
import { BscCover } from "./bscCoverModel";
import { ITenantAware, tenantAwareModel, tenantUnawareModel } from "./common";
import { QuoteSlip } from "./quoteSlipModel";

/**
 * @swagger
 * components:
 *   schemas:
 *     IBscMoneySafeTillCover:
 *       type: object
 *       required:
 *         - locationId
 *         - occupancyId
 *         - moneySafe
 *         - moneyTillCounter
 *         - total
 *       properties:
 *         locationId:
 *           type: Schema.Types.ObjectId
 *         occupancyId:
 *           type: Schema.Types.ObjectId
 *         moneySafe:
 *           type: number
 *         moneyTillCounter:
 *           type: number
 *         total:
 *           type: number
 */

// Base interface
export interface IBscMoneySafeTillCover {
  locationId: Schema.Types.ObjectId;
  occupancyId: Schema.Types.ObjectId;
  moneySafe: Number;
  moneyTillCounter: Number;
  total: Number;
  clientLocationId: Schema.Types.ObjectId;
  quoteId:Schema.Types.ObjectId;
}

// IDocument
export interface IBscMoneySafeTillCoverDocument extends IBscMoneySafeTillCover, ITenantAware, Document {}

// IModel
export interface IBscMoneySafeTillCoverModel extends Model<IBscMoneySafeTillCoverDocument> {}

// Schema
const BscMoneySafeTillCoverSchema = new Schema<IBscMoneySafeTillCoverDocument, IBscMoneySafeTillCoverModel>({
  LocationId: {
    type: Schema.Types.ObjectId,
    ref: "ClientLocation",
    required: [true, "Client location is required"]
  },
  occupancyId: {
    type: Schema.Types.ObjectId,
    ref: "Occupancy",
    required: [true, "Occupancy is required"]
  },
  moneySafe: {
    type: Number
  },
  moneyTillCounter: {
    type: Number
  },
  total: {
    type: Number
  },
  clientLocationId: {
    type: Schema.Types.ObjectId,
    ref: "ClientLocation",
    required: [true, "Client location is required"]
  },
  quoteId: {
    type: Schema.Types.ObjectId,
    ref: "QuoteSlip",
    required: [true, "Quote is required"]
  },
});


// INSTANCE METHODS
BscMoneySafeTillCoverSchema.methods.computeMoneySafeTilltotalPremium = async function () {
  const quote = await QuoteSlip({ skipTenant: false, specifiedPartnerId: this.partnerId }).findOne({ _id: this.quoteId });
  const bscCover = await BscCover({ skipTenant: false, specifiedPartnerId: this.partnerId })
    .applicableBetween()
    .findOne({ bscType: "Money In Safe/Till",productId: quote.productId, fromSI: { $lte: this.moneyTillCounter }, toSI: { $gte: this.moneyTillCounter } }); 
  if (bscCover) {
    const moneySafeTillTotal = (Number(this.moneyTillCounter) * Number(bscCover.ratePerMile)) / 1000;
    return moneySafeTillTotal;
  } else {
    return 0;
  }
};

// Model
// export const CityMaster = mongoose.model<IBscFireLossOfProfitCoverDocument, IBscFireLossOfProfitCoverModel>('BscFireLossOfProfitCover', BscFireLossOfProfitCoverSchema);
export const BscMoneySafeTillCover = tenantAwareModel<IBscMoneySafeTillCoverDocument, IBscMoneySafeTillCoverModel>("BscMoneySafeTillCover", BscMoneySafeTillCoverSchema);

// Initialize the model once.
BscMoneySafeTillCover({ skipTenant: true });
