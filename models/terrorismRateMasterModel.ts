import mongoose, { Document, Model, Query, Schema } from "mongoose";
import validator from "validator";
import { applicableBetween, IApplicableDateRangeAware, ITenantAware, tenantAwareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     ITerrorismRateMaster:
 *       type: object
 *       required:
 *         - industryType
 *         - fromSI
 *         - toSI
 *         - ratePerMile
 *         - addValue
 *         - remark
 *         - productId
 *         - applicableFrom
 *         - applicableTo
 *       properties:
 *         industryType:
 *           type: string
 *         fromSI:
 *           type: number
 *         toSI:
 *           type: number
 *         ratePerMile:
 *           type: number
 *         addValue:
 *           type: number
 *         remark:
 *           type: string
 *         productId:
 *           type: Schema.Types.ObjectId
 *         applicableFrom:
 *           type: date
 *         applicableTo:
 *           type: date
 */

// Base interface
export interface ITerrorismRateMaster {
  // industryType: string;
  industryTypeId: Schema.Types.ObjectId;
  fromSI: number;
  toSI: number;
  ratePerMile: number;
  addValue: number;
  remark: string;
  productId: Schema.Types.ObjectId;
}

// IDocument
/** Contains the instance methods as and when we add them. */
export interface ITerrorismRateMasterDocument extends ITerrorismRateMaster, Document, ITenantAware, IApplicableDateRangeAware {}

// IModel
/** Contains the static methods as and when we add them. */
export interface ITerrorismRateMasterModel extends Model<ITerrorismRateMasterDocument> {
  applicableBetween: () => Query<ITerrorismRateMasterDocument[], ITerrorismRateMasterDocument>;
}

// Schema
const TerrorismRateMasterSchema = new Schema<ITerrorismRateMasterDocument, ITerrorismRateMasterModel>({
  // industryType: {
  //   type: String,
  //   enum: {
  //     values: ["Non Industrial", "Industrial", "Residential Society"],
  //     message: "Industry type is either: Residential Society, Non Industrial or Industrial."
  //   }
  // },
  industryTypeId: {
    type: Schema.Types.ObjectId,
    ref: "IndustryTypeMaster"
  },
  fromSI: Number,
  toSI: Number,
  ratePerMile: Number,
  addValue: Number,
  remark: String,
  productId: {
    type: Schema.Types.ObjectId,
    ref: "ProductMaster"
  },
});

// STATIC METHODS
TerrorismRateMasterSchema.statics.applicableBetween = applicableBetween;

// Model
export const TerrorismRateMaster = tenantAwareModel<ITerrorismRateMasterDocument, ITerrorismRateMasterModel>("TerrorismRateMaster", TerrorismRateMasterSchema);

// Initialize the model once.
TerrorismRateMaster({ skipTenant: true });
