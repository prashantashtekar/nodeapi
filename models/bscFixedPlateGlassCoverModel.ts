import mongoose, { Document, Model, Schema } from "mongoose";
import { BscCover } from "./bscCoverModel";
import { ITenantAware, tenantAwareModel, tenantUnawareModel } from "./common";
import { QuoteSlip } from "./quoteSlipModel";

/**
 * @swagger
 * components:
 *   schemas:
 *     IBscFixedPlateGlassCover:
 *       type: object
 *       required:
 *         - locationId
 *         - plateGlassType
 *         - description
 *         - sumInsured
 *         - total
 *       properties:
 *         locationId:
 *           type: Schema.Types.ObjectId
 *         plateGlassType:
 *           type: string
 *         description:
 *           type: string
 *         sumInsured:
 *           type: number
 *         total:
 *           type: number
 */

// Base interface
export interface IBscFixedPlateGlassCover {
  locationId: Schema.Types.ObjectId;
  plateGlassType: string;
  description: string;
  sumInsured: Number;
  total: Number;
  clientLocationId: Schema.Types.ObjectId;
  quoteId:Schema.Types.ObjectId;
}

// IDocument
export interface IBscFixedPlateGlassCoverDocument extends IBscFixedPlateGlassCover, ITenantAware, Document {}

// IModel
export interface IBscFixedPlateGlassCoverModel extends Model<IBscFixedPlateGlassCoverDocument> {}

// Schema
const BscFixedPlateGlassCoverSchema = new Schema<IBscFixedPlateGlassCoverDocument, IBscFixedPlateGlassCoverModel>({
  LocationId: {
    type: Schema.Types.ObjectId,
    ref: "ClientLocation",
    required: [true, "Fixed Plate Glass is required"]
  },
  plateGlassType: {
    type: String
  },
  description: {
    type: String
  },
  sumInsured: {
    type: Number
  },
  total: {
    type: Number
  },
  clientLocationId: {
    type: Schema.Types.ObjectId,
    ref: "ClientLocation",
    required: [true, "Client location is required"]
  },
  quoteId: {
    type: Schema.Types.ObjectId,
    ref: "QuoteSlip",
    required: [true, "Quote is required"]
  },
});


// INSTANCE METHODS
BscFixedPlateGlassCoverSchema.methods.computeFixedPlateGlasstotalPremium = async function () {
  const quote = await QuoteSlip({ skipTenant: false, specifiedPartnerId: this.partnerId }).findOne({ _id: this.quoteId });
  const bscCover = await BscCover({ skipTenant: false, specifiedPartnerId: this.partnerId })
    .applicableBetween()
    .findOne({ bscType: "Fixed Plate Glass",productId: quote.productId, fromSI: { $lte: this.sumInsured }, toSI: { $gte: this.sumInsured } }); 
  if (bscCover) {
    const fixedPlateGlassTotal = (Number(this.sumInsured) * Number(bscCover.ratePerMile)) / 1000;
    return fixedPlateGlassTotal;
  } else {
    return 0;
  }
};

// Model
export const BscFixedPlateGlassCover = tenantAwareModel<IBscFixedPlateGlassCoverDocument, IBscFixedPlateGlassCoverModel>(
  "BscFixedPlateGlassCover",
  BscFixedPlateGlassCoverSchema
);

// Initialize the model once.
BscFixedPlateGlassCover({ skipTenant: true });
