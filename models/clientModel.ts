import mongoose, { Document, Model, Schema } from "mongoose";
import validator from "validator";
import { IClientResolutionDto } from "../dtos/clientDtos";
import diffHistory from "../utils/diffHistory";
import { ClientGroupMaster } from "./clientGroupMasterModel";
import { ClientKycMaster } from "./clientKycMasterModel";
import { ITenantAware, tenantAwareModel, tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IClient:
 *       type: object
 *       required:
 *         - clientType
 *         - name
 *         - shortName
 *         - active
 *         - clientGroupId
 *         - pan
 *         - copyOfPan
 *         - vin
 *         - leadGenerator
 *         - natureOfBusiness
 *         - creationDate
 *         - claimsManager
 *         - referralRM
 *         - referredCompany
 *         - employeeStrength
 *         - sameAddressVerification
 *         - contactPerson
 *         - designation
 *         - phone
 *         - email
 *         - mobile
 *         - address
 *         - stateId
 *         - cityId
 *         - pincodeId
 *         - visitingCard
 *         - narration
 *         - clientKycMasterId
 *       properties:
 *         clientType:
 *           type: string
 *         name:
 *           type: string
 *         shortName:
 *           type: string
 *         active:
 *           type: boolean
 *         clientGroupId:
 *           type: Schema.Types.ObjectId
 *         pan:
 *           type: string
 *         copyOfPan:
 *           type: string
 *         vin:
 *           type: string
 *         leadGenerator:
 *           type: string
 *         natureOfBusiness:
 *           type: string
 *         creationDate:
 *           type: date
 *         claimsManager:
 *           type: string
 *         referralRM:
 *           type: string
 *         referredCompany:
 *           type: string
 *         employeeStrength:
 *           type: string
 *         sameAddressVerification:
 *           type: boolean
 *         contactPerson:
 *           type: string
 *         designation:
 *           type: string
 *         phone:
 *           type: string
 *         email:
 *           type: string
 *         mobile:
 *           type: string
 *         address:
 *           type: string
 *         stateId:
 *           type: Schema.Types.ObjectId
 *         cityId:
 *           type: Schema.Types.ObjectId
 *         pincodeId:
 *           type: Schema.Types.ObjectId
 *         visitingCard:
 *           type: string
 *         narration:
 *           type: string
 *         clientKycMasterId:
 *           type: Schema.Types.ObjectId
 */

export interface IClient {
  clientType: string;
  name: string;
  shortName: string;
  active: Boolean;

  // Business details
  clientGroupId: Schema.Types.ObjectId;
  pan: string;
  copyOfPan: string;
  vin: string;
  leadGenerator: string;
  natureOfBusiness: string;
  creationDate: Date;
  claimsManager: string;
  referralRM: string;
  referredCompany: string;

  // Client Details

  employeeStrength: string;

  // Address Details: (Field: "Is your address same as added in client group master") Checkbox

  sameAddressVerification: Boolean;

  // Contact Details

  contactPerson: string;
  designation: string;
  phone: string;
  email: string;
  mobile: string;
  address: string;
  stateId: Schema.Types.ObjectId;
  cityId: Schema.Types.ObjectId;
  pincodeId: Schema.Types.ObjectId;
  visitingCard: string;
  narration: string;
  clientKycMasterId: Schema.Types.ObjectId;

  // selectApprover: (PENDING)
}

// IDocument
export interface IClientDocument extends IClient, ITenantAware, Document {}

// IModel
export interface IClientModel extends Model<IClientDocument> {
  resolveClient: (data: IClientResolutionDto) => Promise<IClientDocument>;
  findByContainsSearch: (search: string) => Promise<IClientDocument[]>;
}

const ClientSchema = new Schema<IClientDocument, IClientModel>({
  clientType: {
    type: String,
    required: [true, "A Client must have a type"]
    // unique: true
  },
  name: {
    type: String,
    required: [true, "A client must have a name"],
    trim: true
  },
  shortName: {
    type: String,
    trim: true
  },
  active: {
    type: Boolean
    // required: [true, 'A Pincode must have a name'], #----DOUBT-----
  },

  clientGroupId: {
    type: Schema.Types.ObjectId,
    ref: "ClientGroupMaster",
    required: [true, "Client group must be selected"]
  },
  pan: {
    type: String
  },
  copyOfPan: {
    String
  },
  vin: {
    type: String,
    required: [true, "VIN number required"],
    // unique: true,
    trim: true
  },
  leadGenerator: String,
  natureOfBusiness: {
    type: String,
    required: [true, "Nature of business must be selected"]
  },
  creationDate: {
    type: Date
  },
  claimsManager: {
    type: String
  },
  referralRM: String,
  referredCompany: String,
  employeeStrength: {
    type: String,
    required: [true, "Employee strength is required"]
  },
  sameAddressVerification: Boolean,
  contactPerson: {
    type: String,
    required: [true, "Contact person required"]
  },

  designation: {
    type: String
  },
  phone: {
    type: String,
    required: [true, "Phone number reequired"]
  },
  email: {
    type: String,
    required: [true, "Email id is required"]
  },
  mobile: {
    type: String,
    required: [true, "Mobile number is required"]
  },
  address: {
    type: String,
    required: [true, "Address is required"]
  },
  stateId: {
    type: Schema.Types.ObjectId,
    ref: "StateMaster",
    required: [true, "Please select state"]
  },
  cityId: {
    type: Schema.Types.ObjectId,
    ref: "CityMaster",
    required: [true, "Please select city"]
  },
  pincodeId: {
    type: Schema.Types.ObjectId,
    ref: "PincodeMaster",
    required: [true, "Please enter pincode"]
  },
  visitingCard: {
    type: String,
    required: [true, "Visiting card is required"]
  },
  narration: {
    type: String
  }

  // selectApprover: (PENDING)
});

ClientSchema.plugin(diffHistory.plugin);

// STATIC METHODS
ClientSchema.statics.findByContainsSearch = function (search: string) {
  return this.find({
    $or: [{ name: { $regex: ".*" + search + ".*" } }, { pan: { $regex: ".*" + search + ".*" } }]
  }).limit(20);
};

ClientSchema.statics.resolveClient = async function (data: IClientResolutionDto) {
  const clientKycMasterModel = ClientKycMaster();

  // 1. Use the data.client.pan to check if a document exists on the ClientKycMaster Collection.
  // TODO: Hasan to ask client if we need to do de-duplication first on PAN, and then also on GST.
  const clientKycMaster = await clientKycMasterModel.findOne({
    pan: data.client.pan
  });

  // 2. If kyc entry does not exist.
  if (!clientKycMaster) {
    const clientGroupMaster = await ClientGroupMaster().findOne({
      _id: data.clientGroupMasterId
    });

    const clientKycMaster = await clientKycMasterModel.create({
      clientGroupName: clientGroupMaster.clientGroupName,
      clientName: data.client.name,
      pan: data.client.pan
      // TODO: Add the GST field in the client model.
      // gst: data.client.gst,
    });

    data.client.clientKycMasterId = clientKycMaster._id;
  }

  return this.create(data.client);
};

export const Client = tenantAwareModel<IClientDocument, IClientModel>("Client", ClientSchema);

// Initialize the model once.
Client({ skipTenant: true });
