import mongoose, { Document, Model, Schema } from "mongoose";
import validator from "validator";
import { ITenantAware, tenantAwareModel, tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IQuoteLocationAddonCovers:
 *       type: object
 *       required:
 *         - quoteId
 *         - locationId
 *         - addOnCoverId
 *       properties:
 *         quoteId:
 *           type: Schema.Types.ObjectId
 *         locationId:
 *           type: Schema.Types.ObjectId
 *         addOnCoverId:
 *           type: Schema.Types.ObjectId
 *
 */

// Base interface
export interface IQuoteLocationAddonCovers {
  quoteId: Schema.Types.ObjectId;
  locationId: Schema.Types.ObjectId;
  addOnCoverId: Schema.Types.ObjectId;
}

// IDocument
export interface IQuoteLocationAddonCoversDocument extends IQuoteLocationAddonCovers, Document, ITenantAware {}

// IModel
export interface IQuoteLocationAddonCoversModel extends Model<IQuoteLocationAddonCoversDocument> {}

// Schema
const QuoteLocationAddonCoversSchema = new Schema<IQuoteLocationAddonCoversDocument, IQuoteLocationAddonCoversModel>({
  quoteId: {
    type: Schema.Types.ObjectId,
    ref: "QuoteSlip",
    required: [true, "Quote required"]
  },
  locationId: {
    type: Schema.Types.ObjectId,
    ref: "ClientLocation",
    required: [true, "Location required"]
  },
  addOnCoverId: {
    type: Schema.Types.ObjectId,
    ref: "AddOnCover",
    required: [true, "Location required"]
  }
});

// Model
export const QuoteLocationAddonCovers = tenantAwareModel<IQuoteLocationAddonCoversDocument, IQuoteLocationAddonCoversModel>(
  "QuoteLocationAddonCovers",
  QuoteLocationAddonCoversSchema
);

// Initialize the model once.
QuoteLocationAddonCovers({ skipTenant: true });
