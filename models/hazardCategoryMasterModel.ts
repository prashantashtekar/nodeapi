import mongoose, { Document, Model, Schema } from "mongoose";
import validator from "validator";
import { tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IHazardCategoryMaster:
 *       type: object
 *       required:
 *         - category
 *         - score
 *         - status
 *       properties:
 *         category:
 *           type: string
 *         score:
 *           type: string
 *         status:
 *           type: boolean
 */

// Base interface
export interface IHazardCategoryMaster {
  category: string;
  score: string;
  status: Boolean;
}

// IDocument
export interface IHazardCategoryMasterDocument extends IHazardCategoryMaster, Document {}

// IModel
export interface IHazardCategoryMasterModel extends Model<IHazardCategoryMasterDocument> {}

// Schema
const HazardCategoryMasterSchema = new Schema<IHazardCategoryMasterDocument, IHazardCategoryMasterModel>({
  category: {
    type: String,
    required: [true, "Category required"],
    trim: true
  },
  score: {
    type: String,
    ref: "StateMaster",
    required: [true, "Score required"]
  },
  status: {
    type: Boolean,
    required: [true, "Status required"]
  }
});

// Model
// export const CityMaster = mongoose.model<ICityMasterDocument, ICityMasterModel>('CityMaster', CityMasterSchema);
export const HazardCategoryMaster = tenantUnawareModel<IHazardCategoryMasterDocument, IHazardCategoryMasterModel>("HazardCategoryMaster", HazardCategoryMasterSchema);

// Initialize the model once.
HazardCategoryMaster();
