import mongoose, { Document, Model, Schema } from "mongoose";
import validator from "validator";
import { tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     ICityMaster:
 *       type: object
 *       required:
 *         - name
 *         - stateId
 *       properties:
 *         name:
 *           type: string
 *           description: Name.
 *         stateId:
 *           type: Schema.Types.ObjectId
 */

// Base interface
export interface ICityMaster {
  name: string;
  stateId: Schema.Types.ObjectId;
}

// IDocument
export interface ICityMasterDocument extends ICityMaster, Document {}

// IModel
export interface ICityMasterModel extends Model<ICityMasterDocument> {}

// Schema
const CityMasterSchema = new Schema<ICityMasterDocument, ICityMasterModel>({
  name: {
    type: String,
    required: [true, "A City must have a name"],
    unique: true,
    trim: true,
    maxLength: [128, "A City name must have less than or equal to 128 characters."]
  },
  stateId: {
    type: Schema.Types.ObjectId,
    ref: "StateMaster",
    required: [true, "City must have a state"]
  }
});

// Model
// export const CityMaster = mongoose.model<ICityMasterDocument, ICityMasterModel>('CityMaster', CityMasterSchema);
export const CityMaster = tenantUnawareModel<ICityMasterDocument, ICityMasterModel>("CityMaster", CityMasterSchema);

// Initialize the model once.
CityMaster();
