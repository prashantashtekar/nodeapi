import mongoose, { Document, Model, Schema } from "mongoose";
import validator from "validator";
import diffHistory from "../utils/diffHistory";
import { tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IClientKycMaster:
 *       type: object
 *       required:
 *         - clientGroupName
 *         - clientName
 *         - pan
 *         - gst
 *       properties:
 *         clientGroupName:
 *           type: string
 *         clientName:
 *           type: string
 *         pan:
 *           type: string
 *         gst:
 *           type: string
 */

export interface IClientKycMaster {
  clientGroupName: string;
  clientName: string;
  pan: string;
  gst: string;
}

export interface IClientKycMasterDocument extends IClientKycMaster, Document {}

// IModel
export interface IClientKycMasterModel extends Model<IClientKycMasterDocument> {}

const ClienKycMasterSchema = new Schema<IClientKycMasterDocument, IClientKycMasterModel>({
  clientGroupName: {
    type: String,
    required: [true, "Client group name required"],
    trim: true
  },
  clientName: {
    type: String,
    required: [true, "Client name required"]
  },
  pan: {
    type: String,
    required: [true, "Pan is required"],
    unique: true
  },
  gst: {
    type: String,
    required: [true, "GST is required"],
    unique: true
  }
});

ClienKycMasterSchema.plugin(diffHistory.plugin);

// export const KYCMaster = mongoose.model<IClientKycMasterDocument, IClientKycMasterModel>('ClientLocation', KYCMasterSchema);
export const ClientKycMaster = tenantUnawareModel<IClientKycMasterDocument, IClientKycMasterModel>("ClientKycMaster", ClienKycMasterSchema);

// Initialize the model once.
ClientKycMaster();
