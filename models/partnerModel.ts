import mongoose, { Document, Model, Schema } from "mongoose";
import validator from "validator";
import diffHistory from "../utils/diffHistory";
import { tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IPartner:
 *       type: object
 *       required:
 *         - partnerType
 *         - name
 *         - shortname
 *         - address
 *         - cityId
 *         - districtId
 *         - stateId
 *         - pincodeId
 *         - countryId
 *         - pan
 *         - gstin
 *         - cin
 *         - contactPerson
 *         - designation
 *         - mobileNumber
 *         - status
 *
 *       properties:
 *         partnerType:
 *           type: string
 *           description: ObjectId of the IUser record this user reports to.
 *         name:
 *           type: string
 *         shortName:
 *           type: string
 *         address:
 *           type: string
 *         cityId:
 *           type: string
 *         districtId:
 *           type: string
 *         stateId:
 *           type: string
 *         pincodeId:
 *           type: string
 *         countryId:
 *           type: string
 *         pan:
 *           type: string
 *         gstin:
 *           type: string
 *         cin:
 *           type: string
 *         contactPerson:
 *           type: string
 *         designation:
 *           type: string
 *         mobileNumber:
 *           type: string
 *         status:
 *           type: string
 */

// Base interface
export interface IPartner {
  partnerType: string;
  name: string;
  shortName: string;
  address: string;

  cityId: Schema.Types.ObjectId;
  districtId: Schema.Types.ObjectId;
  stateId: Schema.Types.ObjectId;
  pincodeId: Schema.Types.ObjectId;
  countryId: Schema.Types.ObjectId;

  pan: string;
  gstin: string;
  cin: string;

  // SPOC: single point of contact.
  contactPerson: string;
  designation: string;
  mobileNumber: string;
  status: string;
}

// IDocument
/** Contains the instance methods as and when we add them. */
export interface IPartnerDocument extends IPartner, Document {}

// IModel
/** Contains the static methods as and when we add them. */
export interface IPartnerModel extends Model<IPartnerDocument> {
  fetchSelfPartner: () => Promise<IPartnerDocument>;
}

// Schema
const PartnerSchema = new Schema<IPartnerDocument, IPartnerModel>({
  partnerType: {
    type: String,
    required: [true, "A tour must have a difficulty"],
    enum: {
      values: ["self", "insurer", "broker", "agent", "banca", "corporate-agent"],
      message: "Partner type is either: insurer, broker, agent, banca or corporate-agent."
    }
  },
  name: {
    type: String,
    required: [true, "A partner must have a name"],
    unique: true,
    trim: true,
    maxLength: [128, "A partner name must have less than or equal to 128 characters."]
  },
  shortName: {
    type: String,
    required: [true, "A partner must have a short name"],
    unique: true,
    trim: true,
    maxLength: [25, "A partner short name must have less than or equal to 25 characters."]
  },
  address: String,
  cityId: {
    type: Schema.Types.ObjectId,
    ref: "CityMaster",
    required: [true, "Partner must have a city"]
  },
  districtId: {
    type: Schema.Types.ObjectId,
    ref: "DistrictMaster",
    required: [true, "Partner must have a district"]
  },
  stateId: {
    type: Schema.Types.ObjectId,
    ref: "StateMaster",
    required: [true, "Partner must have a state"]
  },
  countryId: {
    type: Schema.Types.ObjectId,
    ref: "CountryMaster",
    required: [true, "Partner must have a country"]
  },
  pincodeId: {
    type: Schema.Types.ObjectId,
    ref: "PincodeMaster",
    required: [true, "Partner must have a pincode"]
  },
  pan: {
    type: String,
    required: [true, "A partner must have a PAN"]
  },
  gstin: {
    type: String,
    required: [true, "A partner must have a GST number"]
  },
  cin: {
    type: String,
    required: [true, "A partner must have a CIN number"]
  },
  contactPerson: String,
  designation: String,
  mobileNumber: String,
  status: String
});

PartnerSchema.plugin(diffHistory.plugin);

// STATIC METHODS
PartnerSchema.statics.fetchSelfPartner = function (search: string) {
  return this.findOne({ partnerType: "self" });
};

// Model
export const Partner = tenantUnawareModel<IPartnerDocument, IPartnerModel>("Partner", PartnerSchema);

// Initialize the model once.
Partner();
