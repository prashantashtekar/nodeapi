import mongoose, { Document, Model, Query, Schema } from "mongoose";
import validator from "validator";
import { applicableBetween, IApplicableDateRangeAware, ITenantAware, tenantAwareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IAddOnCover:
 *       type: object
 *       required:
 *         - category
 *         - subCategory
 *         - name
 *         - isFree
 *         - isTariff
 *         - claimClause
 *         - monthOrDays
 *         - siLimit
 *         - rate
 *         - categoryOfImportance
 *         - sequenceNo
 *         - policyType
 *         - addOnType
 *         - sumInsured
 *         - isAddonSelected
 *         - rateType
 *         - rateParameter
 *         - freeUpToFlag
 *         - freeUpToParameter
 *         - productId
 *         - calculatedIndicativePremium
 *       properties:
 *         category:
 *           type: string
 *         subCategory:
 *           type: string
 *         name:
 *           type: string
 *         isFree:
 *           type: boolean
 *         isTariff:
 *           type: boolean
 *         claimClause:
 *           type: string
 *         monthOrDays:
 *           type: boolean
 *         siLimit:
 *           type: boolean
 *         rate:
 *           type: number
 *         categoryOfImportance:
 *           type: string
 *         sequenceNo:
 *           type: string
 *         policyType:
 *           type: string
 *         addOnType:
 *           type: string
 *         sumInsured:
 *           type: number
 *         isAddonSelected:
 *           type: boolean
 *         rateType:
 *           type: string
 *         rateParameter:
 *           type: string
 *         freeUpToFlag:
 *           type: string
 *         freeUpToParameter:
 *           type: string
 *         productId:
 *           type: Schema.Types.ObjectId
 *         calculatedIndicativePremium:
 *           type: number
 */

export interface IAddOnCover {
  category: string;
  subCategory: string;
  name: string;
  isFree: boolean;
  isTariff: Boolean;
  claimClause: string;
  monthOrDays: Boolean;
  siLimit: Boolean;
  rate: number;
  categoryOfImportance: string;
  sequenceNo: string;
  // entityTypeId: string;
  policyType: string;
  addOnType: string;
  sumInsured: number;
  isAddonSelected: Boolean;
  rateType: string;
  rateParameter: string;
  freeUpToFlag: string;
  freeUpToParameter: string;
  productId: Schema.Types.ObjectId;
  calculatedIndicativePremium: number;
}

export interface IAddOnCoverDocument extends IAddOnCover, Document, ITenantAware, IApplicableDateRangeAware {}

// IModel
export interface IAddOnCoverModel extends Model<IAddOnCoverDocument> {
  applicableBetween: () => Query<IAddOnCoverDocument[], IAddOnCoverDocument>;
}

const AddOnCoverSchema = new Schema<IAddOnCoverDocument, IAddOnCoverModel>({
  category: {
    type: String,
    // required: [true, 'Category required'],
    trim: true
  },
  subCategory: {
    type: String
    // required: [true, 'Sub Category is required'],
  },
  name: {
    type: String,
    required: [true, "Name is required"],
    trim: true
  },
  isFree: {
    type: Boolean,
    default: false,
    required: [true, "Free paid flag is required"]
  },
  isTariff: {
    type: Boolean
  },
  claimClause: {
    type: String
    // required: [true, 'Claim clause required'],
  },
  monthOrDays: {
    type: Boolean
    // required: [true, 'Month or days required'],
  },
  siLimit: {
    type: Boolean
    // required: [true, 'SI Limit required'],
  },
  rate: {
    type: Number
    // required: [true, 'rate is required'],
  },
  categoryOfImportance: {
    type: String,
    required: [true, "Category of importance required"]
  },
  sequenceNo: {
    type: String
    // required: [true, 'Sequence Number is required'],
  },
  // entityTypeId: {
  //   type: String,
  //   required: [true, 'Entity type ID is required'],
  // },
  policyType: {
    type: String
    // required: [true, 'Policy type required'],
  },
  addOnType: {
    type: String
    // required: [true, 'Add on type required'],
  },
  sumInsured: {
    type: Number
    // required: [true, 'Sum Insured required'],
  },
  isAddonSelected: {
    type: Boolean
    // required: [true],
  },
  rateType: {
    type: String
    // required: [true, 'Rate Type required'],
  },
  rateParameter: {
    type: String
    // required: [true, 'Rate parameter required'],
  },
  freeUpToFlag: {
    type: String
    // required: [true, 'Free up to flag required'],
  },
  freeUpToParameter: {
    type: String
    // required: [true, 'Free upto parameter required'],
  },
  calculatedIndicativePremium: {
    type: Number
    // required: [true, 'calculatedIndicativePremium required'],
  },
  productId: {
    type: Schema.Types.ObjectId,
    ref: "ProductMaster"
  }
});

// STATIC METHODS
AddOnCoverSchema.statics.applicableBetween = applicableBetween;

// Model
export const AddOnCover = tenantAwareModel<IAddOnCoverDocument, IAddOnCoverModel>("AddOnCover", AddOnCoverSchema);

// Initialize the model once.
AddOnCover({ skipTenant: true });
