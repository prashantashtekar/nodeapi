import mongoose, { Document, Model, Query, Schema } from "mongoose";
import { applicableBetween, IApplicableDateRangeAware, ITenantAware, tenantAwareModel, tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IBscDiscountCover:
 *       type: object
 *       required:
 *         - bscType
 *         - fromSI
 *         - toSI
 *         - ratePerMile
 *         - productId
 *         - applicableFrom
 *         - applicableTo
 *       properties:
 *         bscType:
 *           type: string
 *         fromSI:
 *           type: number
 *         toSI:
 *           type: number
 *         ratePerMile:
 *           type: number
 *         productId:
 *           type: Schema.Types.ObjectId
 *         applicableFrom:
 *           type: date
 *         applicableTo:
 *           type: date
 */
// Base interface
export interface IBscDiscountCover {
  bscType: string;
  fromSI: Number;
  toSI: Number;
  discountPercentagePerMile: Number;
  productId: Schema.Types.ObjectId;
}

// IDocument
export interface IBscDiscountCoverDocument extends IBscDiscountCover, ITenantAware, Document, IApplicableDateRangeAware {}

// IModel
export interface IBscDiscountCoverModel extends Model<IBscDiscountCoverDocument> {
  applicableBetween: () => Query<IBscDiscountCoverDocument[],IBscDiscountCoverDocument>
}

// Schema
const BscDiscountCoverSchema = new Schema<IBscDiscountCoverDocument, IBscDiscountCoverModel>({
  bscType: {
    type: String,
    enum: {
      values: [
        "Fire Loss of Profit",
        "Burglary & Housebreaking",
        "Money In Safe/Till",
        "Money In Transit",
        "Electronic Equipments",
        "Portable Equipments",
        "Fixed Plate Glass",
        "Accompanied Baggage",
        "Fidelity guarantee",
        "Signage",
        "Liability section"
      ],
      message:
        "Business Suraksha Dicound Cover is either: Non Fire Loss of Profit or Burglary & Housebreaking or Money In Safe/Till or Money In Transit or Electronic Equipments or Portable Equipments or Fixed Plate Glass or Accompanied Baggage or Fidelity guarantee or Signage or Liability section."
    },
    required: [true, "Business Suraksha Dicound Cover is required"]
  },
  fromSI: Number,
  toSI: Number,
  discountPercentagePerMile: Number,
  productId: {
    type: Schema.Types.ObjectId,
    ref: "ProductMaster"
  },
});

// STATICS METHODS

BscDiscountCoverSchema.statics.applicableBetween = applicableBetween;


// Model
export const BscDiscountCover = tenantAwareModel<IBscDiscountCoverDocument, IBscDiscountCoverModel>("BscDiscountCover", BscDiscountCoverSchema);

// Initialize the model once.
BscDiscountCover({ skipTenant: true });
