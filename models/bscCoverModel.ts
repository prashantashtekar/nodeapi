import { METHODS } from "http";
import mongoose, { Document, Model, Query, Schema } from "mongoose";
import { sumInsuredBetween, ISumInsuredBetweenAware, applicableBetween, IApplicableDateRangeAware, ITenantAware, tenantAwareModel, tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IBscCover:
 *       type: object
 *       required:
 *         - bscType
 *         - fromSI
 *         - toSI
 *         - ratePerMile
 *         - productId
 *         - applicableFrom
 *         - applicableTo
 *       properties:
 *         bscType:
 *           type: string
 *         fromSI:
 *           type: number
 *         toSI:
 *           type: number
 *         ratePerMile:
 *           type: number
 *         productId:
 *           type: Schema.Types.ObjectId
 *         applicableFrom:
 *           type: date
 *         applicableTo:
 *           type: date
 */

// Base interface
export interface IBscCover {
  bscType: string;
  fromSI: Number;
  toSI: Number;
  ratePerMile: Number;
  productId: Schema.Types.ObjectId;
}

// IDocument
export interface IBscCoverDocument extends IBscCover, ITenantAware, Document, IApplicableDateRangeAware, ISumInsuredBetweenAware {}

// IModel
export interface IBscCoverModel extends Model<IBscCoverDocument> {
  applicableBetween: () => Query<IBscCoverDocument[], IBscCoverDocument>;
  sumInsuredBetween: () => Query<IBscCoverDocument[], IBscCoverDocument>;
}

// Schema
const BscCoverSchema = new Schema<IBscCoverDocument, IBscCoverModel>({
  bscType: {
    type: String,
    enum: {
      values: [
        "Fire Loss of Profit",
        "Burglary & Housebreaking",
        "Money In Safe/Till",
        "Money In Transit",
        "Electronic Equipments",
        "Portable Equipments",
        "Fixed Plate Glass",
        "Accompanied Baggage",
        "Fidelity guarantee",
        "Signage",
        "Liability section"
      ],
      message:
        "Business Suraksha Cover is either: Non Fire Loss of Profit or Burglary & Housebreaking or Money In Safe/Till or Money In Transit or Electronic Equipments or Portable Equipments or Fixed Plate Glass or Accompanied Baggage or Fidelity guarantee or Signage or Liability section."
    },
    required: [true, "Electronic Equipments location is required"]
  },
  ratePerMile: Number,
  productId: {
    type: Schema.Types.ObjectId,
    ref: "ProductMaster"
  }
});

// STATICS METHODS

BscCoverSchema.statics.applicableBetween = applicableBetween;
BscCoverSchema.statics.sumInsuredBetween = sumInsuredBetween;

// Model
export const BscCover = tenantAwareModel<IBscCoverDocument, IBscCoverModel>("BscCover", BscCoverSchema);

// Initialize the model once.
BscCover({ skipTenant: true });
