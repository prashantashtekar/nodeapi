import mongoose, { Document, Model, Schema } from "mongoose";
import { BscCover } from "./bscCoverModel";
import { ITenantAware, tenantAwareModel, tenantUnawareModel } from "./common";
import { QuoteSlip } from "./quoteSlipModel";

/**
 * @swagger
 * components:
 *   schemas:
 *     IBscAccompaniedBaggageCover:
 *       type: object
 *       required:
 *         - baggageType
 *         - baggageDescription
 *         - sumInsured
 *         - total
 *       properties:
 *         baggageType:
 *           type: string
 *         baggageDescription:
 *           type: string
 *         sumInsured:
 *           type: number
 *         total:
 *           type: number
 */

// Base interface
export interface IBscAccompaniedBaggageCover {
  baggageType: string;
  baggageDescription: string;
  sumInsured: Number;
  total: Number;
  clientLocationId: Schema.Types.ObjectId;
  quoteId:Schema.Types.ObjectId;
}

// IDocument
export interface IBscAccompaniedBaggageCoverDocument extends IBscAccompaniedBaggageCover, ITenantAware, Document {}

// IModel
export interface IBscAccompaniedBaggageCoverModel extends Model<IBscAccompaniedBaggageCoverDocument> {}

// Schema
const BscAccompaniedBaggageCoverSchema = new Schema<IBscAccompaniedBaggageCoverDocument, IBscAccompaniedBaggageCoverModel>({
  baggageType: {
    type: String,
    required: [true, "Baggage Type is required"]
  },
  baggageDescription: {
    type: String
  },
  sumInsured: {
    type: Number
  },
  total: {
    type: Number
  },
  clientLocationId: {
    type: Schema.Types.ObjectId,
    ref: "ClientLocation",
    required: [true, "Client location is required"]
  },
  quoteId: {
    type: Schema.Types.ObjectId,
    ref: "QuoteSlip",
    required: [true, "Quote is required"]
  },
});


// INSTANCE METHODS
BscAccompaniedBaggageCoverSchema.methods.computeBaggagetotalPremium = async function () {
  const quote = await QuoteSlip({ skipTenant: false, specifiedPartnerId: this.partnerId }).findOne({ _id: this.quoteId });
  const bscCover = await BscCover({ skipTenant: false, specifiedPartnerId: this.partnerId })
    .applicableBetween()
    .findOne({ bscType: "Accompanied Baggage",productId: quote.productId, fromSI: { $lte: this.sumInsured }, toSI: { $gte: this.sumInsured } }); 
  if (bscCover) {
    const baggageTotal = (Number(this.sumInsured) * Number(bscCover.ratePerMile)) / 1000;
    return baggageTotal;
  } else {
    return 0;
  }
};

// Model
export const BscAccompaniedBaggageCover = tenantAwareModel<IBscAccompaniedBaggageCoverDocument, IBscAccompaniedBaggageCoverModel>(
  "BscAccompaniedBaggageCover",
  BscAccompaniedBaggageCoverSchema
);

// Initialize the model once.
BscAccompaniedBaggageCover({ skipTenant: true });
