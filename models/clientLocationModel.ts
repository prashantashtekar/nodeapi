import mongoose, { Document, Model, Schema } from "mongoose";
import validator from "validator";
import diffHistory from "../utils/diffHistory";
import { ITenantAware, tenantAwareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IClientKycMaster:
 *       type: object
 *       required:
 *         - address
 *         - cityId
 *         - stateId
 *         - pincodeId
 *         - locationName
 *         - clientId
 *       properties:
 *         address:
 *           type: string
 *         cityId:
 *           type: Schema.Types.ObjectId
 *         stateId:
 *           type: Schema.Types.ObjectId
 *         pincodeId:
 *           type: Schema.Types.ObjectId
 *         locationName:
 *           type: string
 *         clientId:
 *           type: Schema.Types.ObjectId
 */

export interface IClientLocation {
  address: string;
  cityId: Schema.Types.ObjectId;
  stateId: Schema.Types.ObjectId;
  pincodeId: Schema.Types.ObjectId;
  locationName: string;
  clientId: Schema.Types.ObjectId;
}

export interface IClientLocationDocument extends IClientLocation, Document, ITenantAware {}

// IModel
export interface IClientLocationModel extends Model<IClientLocationDocument> {}

const ClientLocationSchema = new Schema<IClientLocationDocument, IClientLocationModel>({
  address: {
    type: String,
    required: [true, "Address required"],
    trim: true,
    maxLength: [250, "Address cannot be more than 250 characters"]
  },
  cityId: {
    type: Schema.Types.ObjectId,
    ref: "CityMaster",
    required: [true, "City is required"]
  },
  stateId: {
    type: Schema.Types.ObjectId,
    ref: "StateMaster",
    required: [true, "State is required"]
  },
  pincodeId: {
    type: Schema.Types.ObjectId,
    ref: "PincodeMaster",
    required: [true, "Pincode is required"]
  },
  locationName: {
    type: String,
    trim: true
  },
  clientId: {
    type: Schema.Types.ObjectId,
    ref: "Client",
    required: [true, "Please select client"]
  }
});
ClientLocationSchema.plugin(diffHistory.plugin);

// Model
export const ClientLocation = tenantAwareModel<IClientLocationDocument, IClientLocationModel>("ClientLocation", ClientLocationSchema);

// Initialize the model once.
ClientLocation({ skipTenant: true });
