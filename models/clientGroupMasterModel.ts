import mongoose, { Document, Model, Schema } from "mongoose";
import validator from "validator";
import diffHistory from "../utils/diffHistory";
import { ITenantAware, tenantAwareModel, tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IClientGroupMaster:
 *       type: object
 *       required:
 *         - vin
 *         - clientGroupName
 *         - clientGroupContacName
 *         - address
 *         - cityId
 *         - pincodeId
 *         - email
 *         - active
 *       properties:
 *         vin:
 *           type: string
 *         clientGroupName:
 *           type: string
 *         clientGroupContactName:
 *           type: string
 *         address:
 *           type: string
 *         cityId:
 *           type: string
 *         pincodeId:
 *           type: string
 *         email:
 *           type: string
 *         active:
 *           type: boolean
 */

// Base interface
export interface IClientGroupMaster {
  vin: string;
  clientGroupName: string;
  clientGroupContactName: string;
  address: string;
  cityId: Schema.Types.ObjectId;
  pincodeId: Schema.Types.ObjectId;
  email: string;
  active: Boolean;

  // selectApprover: (PENDING)
}

// IDocument
export interface IClientGroupMasterDocument extends IClientGroupMaster, ITenantAware, Document {}

// IModel
export interface IClientGroupMasterModel extends Model<IClientGroupMasterDocument> {
  resolveClientGroup: (data: any) => Promise<IClientGroupMasterDocument>;
  findByContainsSearch: (search: string) => Promise<IClientGroupMasterDocument[]>;
}

const ClientGroupMasterSchema = new Schema<IClientGroupMasterDocument, IClientGroupMasterModel>({
  vin: {
    type: String,
    required: [true, "VIN number is required"]
  },
  clientGroupName: {
    type: String,
    required: [true, "Client group name is required"]
  },
  clientGroupContactName: {
    type: String,
    required: [true, "Client Group Contact name required"]
  },
  address: {
    type: String,
    required: [true, "Address is required"]
  },
  cityId: {
    type: Schema.Types.ObjectId,
    ref: "CityMaster",
    required: [true, "City is required"]
  },
  pincodeId: {
    type: Schema.Types.ObjectId,
    ref: "PincodeMaster",
    required: [true, "Pincode is required"]
  },
  email: {
    type: String,
    required: [true, "Email is required"]
  }
  // selectApprover
});

ClientGroupMasterSchema.plugin(diffHistory.plugin);

// STATIC METHODS
ClientGroupMasterSchema.statics.findByContainsSearch = function (search: string) {
  return this.find({ clientGroupName: { $regex: ".*" + search + ".*" } }).limit(20);
};

ClientGroupMasterSchema.statics.resolveClientGroup = function (data: object) {
  return this.create(data);
};

export const ClientGroupMaster = tenantAwareModel<IClientGroupMasterDocument, IClientGroupMasterModel>("ClientGroupMaster", ClientGroupMasterSchema);

// Initialize the model once.
ClientGroupMaster({ skipTenant: true });
