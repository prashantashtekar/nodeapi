import { METHODS } from "http";
import mongoose, { Document, Model, Query, Schema } from "mongoose";
import validator from "validator";
import { applicableBetween, IApplicableDateRangeAware, ITenantAware, tenantAwareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IEarthquakeRateMaster:
 *       type: object
 *       required:
 *         - industryType
 *         - zone
 *         - rate
 *         - productId
 *         - applicableFrom
 *         - applicableTo
 *       properties:
 *         industryType:
 *           type: string
 *         zone:
 *           type: string
 *         rate:
 *           type: number
 *         productId:
 *           type: Schema.Types.ObjectId
 *         applicableFrom:
 *           type: date
 *         applicableTo:
 *           type: date
 */

// Base interface
export interface IEarthquakeRateMaster {
  industryTypeId: Schema.Types.ObjectId;
  zone: string;
  rate: number;
  productId: Schema.Types.ObjectId;
}

// IDocument
/** Contains the instance methods as and when we add them. */
export interface IEarthquakeRateMasterDocument extends IEarthquakeRateMaster, Document, ITenantAware, IApplicableDateRangeAware {}

// IModel
/** Contains the static methods as and when we add them. */
export interface IEarthquakeRateMasterModel extends Model<IEarthquakeRateMasterDocument> {
  applicableBetween: () => Query<IEarthquakeRateMasterDocument[],IEarthquakeRateMasterDocument>
}

// Schema
const EarthquakeRateMasterSchema = new Schema<IEarthquakeRateMasterDocument, IEarthquakeRateMasterModel>({
  industryTypeId: {
    type: Schema.Types.ObjectId,
    ref: "IndustryTypeMaster"
  },
  zone: String,
  rate: Number,
  productId: {
    type: Schema.Types.ObjectId,
    ref: "ProductMaster"
  },
});

// STATIC METHODS

EarthquakeRateMasterSchema.statics.applicableBetween = applicableBetween;

// Model
export const EarthquakeRateMaster = tenantAwareModel<IEarthquakeRateMasterDocument, IEarthquakeRateMasterModel>("EarthquakeRateMaster", EarthquakeRateMasterSchema);

// Initialize the model once.
EarthquakeRateMaster({ skipTenant: true });
