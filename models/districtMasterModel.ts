import mongoose, { Document, Model, Schema } from "mongoose";
import validator from "validator";
import { tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IDistrictMaster:
 *       type: object
 *       required:
 *         - name
 *         - stateId
 *       properties:
 *         name:
 *           type: string
 *         stateId:
 *           type: Schema.Types.ObjectId
 */

// Base interface
export interface IDistrictMaster {
  name: string;
  stateId: Schema.Types.ObjectId;
}

// IDocument
export interface IDistrictMasterDocument extends IDistrictMaster, Document {}

// IModel
export interface IDistrictMasterModel extends Model<IDistrictMasterDocument> {}

// Schema
const DistrictMasterSchema = new Schema<IDistrictMasterDocument, IDistrictMasterModel>({
  name: {
    type: String,
    required: [true, "A District must have a name"],
    unique: true,
    trim: true,
    maxLength: [128, "A District name must have less than or equal to 128 characters."]
  },
  stateId: {
    type: Schema.Types.ObjectId,
    ref: "StateMaster",
    required: [true, "City must have a state"]
  }
});

// Model
// export const DistrictMaster = mongoose.model<IDistrictMasterDocument, IDistrictMasterModel>('DistrictMaster', DistrictMasterSchema);
export const DistrictMaster = tenantUnawareModel<IDistrictMasterDocument, IDistrictMasterModel>("DistrictMaster", DistrictMasterSchema);

// Initialize the model once.
DistrictMaster();
