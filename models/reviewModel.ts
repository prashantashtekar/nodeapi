import { Aggregate, Model, Query, Schema, Document } from "mongoose";
import { ITenantAware, tenantAwareModel } from "./common";
import { ITourModel, Tour } from "./tourModel";

export interface IReview {
  review: string;
  rating: number;
  createdAt: Date;
  tour: Schema.Types.ObjectId;
  user: Schema.Types.ObjectId;
}

export interface ReviewMiddlewareContext {
  reviewModel?: IReviewModel;
  tourModel?: ITourModel;
}

export interface IReviewDocument extends IReview, Document, ITenantAware {
  // Instance methods go here for type checking.
  // Post hook context, used to pass things between pre & post middleware functions
  _phc: ReviewMiddlewareContext;
}

export interface IReviewModel extends Model<IReviewDocument> {
  // Static methods go here for type checking.
  calcAverageRatings: (tourModel: ITourModel, tourId: Schema.Types.ObjectId) => Promise<Aggregate<any[]>>;
  // calcAverageRatings: (tourId: Schema.Types.ObjectId) => Promise<Aggregate<any[]>>;
}

// This was overriden so that we can pass data on the query being executed.
interface IReviewQuery extends Query<IReviewDocument, IReviewDocument> {
  reviewBeingUpdated: IReview;
  // Post hook context, used to pass things between pre & post middleware functions
  _phc: ReviewMiddlewareContext;
}

// Create the tour schema.
const ReviewSchema = new Schema<IReviewDocument, IReviewModel>(
  {
    review: {
      type: String,
      required: [true, "Review cannot be empty"],
      trim: true
    },
    rating: {
      type: Number,
      required: [true, "A review must have a rating"],
      min: 1,
      max: 5
    },
    createdAt: {
      type: Date,
      default: Date.now(),
      select: false
    },
    // Parent referencing...
    tour: {
      type: Schema.Types.ObjectId,
      ref: "Tour",
      required: [true, "Review must belong to a tour"]
    },
    // Parent referencing...
    user: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: [true, "Review must belong to a user"]
    }
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
  }
);

// Create a compound Unique index.
ReviewSchema.index({ tour: 1, user: 1 }, { unique: true });

// VIRTUALS

// DOCUMENT MIDDLEWARE: runs before .save() and .create(), but not before insertMany()
ReviewSchema.pre<IReviewDocument>("save", async function (next) {
  // this points to the current review.
  this._phc = this._phc || {};
  this._phc.reviewModel = Review();
  this._phc.tourModel = Tour();

  next();
});
// ReviewSchema.pre<IReviewDocument>(
//   'save',
//   tenantAwarePreDocumentMiddleware<IReviewDocument, IReviewModel>(function (
//     next
//   ) {
//     next();
//   })
// );
ReviewSchema.post<IReviewDocument>("save", async function () {
  // this points to the current review.
  await this._phc.reviewModel.calcAverageRatings(this._phc.tourModel, this.tour);
  // await Review().calcAverageRatings(this.tour);
});

// QUERY MIDDLEWARE
ReviewSchema.pre<Query<IReviewDocument, IReviewDocument>>(/^find/, function (next) {
  this.populate({
    path: "user",
    select: "name photo"
  });

  next();
});

// This was done to take care of findByIdAndUpdate & findByIdAndDelete, these internally call the findOneAnd.... method.
// The below pre & post methods are working in tandem, to take care of the scenario of updating the ratings on update an delete.
ReviewSchema.pre<IReviewQuery>(/^findOneAnd/, async function (next) {
  this.reviewBeingUpdated = await this.findOne();
  this._phc.tourModel = Tour();

  next();
});
ReviewSchema.post<IReviewQuery>(/^findOneAnd/, async function () {
  if (this.reviewBeingUpdated) {
    await Review().calcAverageRatings(this._phc.tourModel, this.reviewBeingUpdated.tour);
  }
});

// AGGREGATION MIDDLEWARE

// INSTANCE METHODS

// STATIC METHODS
// We are using a static method, the "this" keyword here represents the actual model.
ReviewSchema.statics.calcAverageRatings = async function (tourModel: ITourModel, tourId: Schema.Types.ObjectId): Promise<Aggregate<any[]>> {
  // ReviewSchema.statics.calcAverageRatings = async function (tourId: Schema.Types.ObjectId): Promise<Aggregate<any[]>> {
  // generate stats
  const stats = await this.aggregate([
    {
      $match: { tour: tourId }
    },
    // Group all the reviews by tour.
    {
      $group: {
        _id: "$tour",
        numberOfRatings: { $sum: 1 },
        averageRating: { $avg: "$rating" }
      }
    }
  ]);

  let ratingsQuantity = 0;
  let ratingsAverage = 4.5;
  if (stats.length > 0) {
    ratingsQuantity = stats[0].numberOfRatings;
    ratingsAverage = stats[0].averageRating;
  }

  // save it against the respective tour.
  await Tour().findByIdAndUpdate(tourId, {
    ratingsQuantity: ratingsQuantity,
    ratingsAverage: ratingsAverage
  });

  return stats;
};

// Now create the model
// export const Review = mongoose.model<IReviewDocument, IReviewModel>('Review', ReviewSchema);
export const Review = tenantAwareModel<IReviewDocument, IReviewModel>("Review", ReviewSchema);

// Initialize the model once.
Review({ skipTenant: true });
