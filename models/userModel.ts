import { Query, Schema, Document, Model } from "mongoose";
import validator from "validator";
import bcrypt from "bcryptjs";
import crypto from "crypto";
import { ITenantAware, tenantAwareModel } from "./common";
// import diffHistory from 'mongoose-diff-history/diffHistory';
import diffHistory from "../utils/diffHistory";


/**
 * @swagger
 * components:
 *   schemas:
 *     IUser:
 *       type: object
 *       required:
 *         - name
 *         - email
 *         - mobileNumber
 *       properties:
 *         reportToId:
 *           type: string
 *           description: ObjectId of the IUser record this user reports to.
 *         name:
 *           type: string
 *         email:
 *           type: string
 *         mobileNumber:
 *           type: string
 *         password:
 *           type: string
 *         passwordConfirm:
 *           type: string
 *         partnerId:
 *           type: string
 *
 *
 */
export interface IUser {
  EntityObjId:Schema.Types.ObjectId;
  EntityName: string;
  Type: string;
  TypeName: string;
  ReportTo: Schema.Types.ObjectId;
  Name: string;
  Email: string;
  Contact: string;
  Status: boolean;
  Password: string;
  StrId: string;
  OTP: number;
  OTPDate: Date | string;
  RoleId: Schema.Types.ObjectId;
  RoleName: string;
  BranchId: string;
  BranchName: string;
  VerticleId: string;
  VerticleType: string;
  ProductTypeId: string;
  ProductType: string;
  BrokerPrivileges: BrokerPrivileges;
  InsurrerPrivileges: InsurrerPrivileges;
  PreferredInsuranceCompanyId: string;
  PasswordChangedAt: Date;
  PasswordResetToken: string;
  PasswordResetExpires: Date;
}
export interface BrokerPrivileges {

  BranchId: Schema.Types.ObjectId;
  BranchName: string;
  VerticleId: string;
  VerticleType: string;
  ProductTypeId: Schema.Types.ObjectId;
  ProductType: string;
}

export interface InsurrerPrivileges {
  ProductTypeId: Schema.Types.ObjectId;
  ProductType: string;
  GeographyId: string;
  Geography: string;
  ApproverTypeId: string;
  ApproverType: string;
}

export interface IUserDocument extends IUser, Document, ITenantAware {
  correctPassword: (enteredPassword: string, userPassword: string) => Promise<Boolean>;
  changedPasswordAfter: (jwtTimestamp: number) => Boolean;
  createPasswordResetToken: () => string;
}

export const _USER_ROLES = {
  SuperAdmin: "SuperAdmin",
  Creator: "Creator",
  Approver: "Approver",
  ICReviewer: "ICReviewer",
  ICApprover: "ICApprover",
  RMUser: "RM User",
  Agent: "Agent" 
};

export interface IUserModel extends Model<IUserDocument> {
  findByEmail: (email: string) => Promise<IUserDocument>;
}

const UserSchema = new Schema<IUserDocument, IUserModel>({
  // Parent referencing...
  EntityObjId: {
    type: String
  },
  EntityName: {
    type: String
  },
  Type: {
    type: String
  },
  TypeName: {
    type: String
  },
  ReportTo: {
    type: String
  },
  name: {
    type: String,
    required: [true, "Please tell us your name!"]
  },
  Email: {
    type: String,
    required: [true, "Email is mandatory!"],
    unique: true,
    lowercase: true,
    validate: [validator.isEmail, "Please provide a valid email!"]
  },
  Contact: {
    type: String,
    required: [true, "Mobile number is mandatory!"],
    unique: true,
    lowercase: true,
    validate: {
      validator: function (val): boolean {
        // this only points to current doc on NEW document creation.
        // return val < this.price;
        // TODO: Create the logic here to make sure that mobile numbers are
        // 1. 10 digits
        // 2. Start with one of 9,8,7,6...

        return true;
      },
      message: "Mobile number has to be 10 digits and start with 9,8,7,6 only."
    }
  },
  RoleId: {
    type: String,
    required: [true, "Every user must be assigned one role"],
    enum: {
      values: [
        "SuperAdmin",
        "Creator",
        "Approver",
        "ICReviewer",
        "ICApprover",
        "RM User",
        "Agent"       
      ],
      message:
        "Role must be either:."
    }
  },
  RoleName: {
    type: String,
    required: [true, "Please tell us your name!"]
  },
  // photo: {
  //   type: String,
  //   default: "default.jpg"
  // },
  Password: {
    type: String,
    required: [true, "Provide a password"],
    minlength: 8,
    select: false
  },
  // passwordConfirm: {
  //   type: String,
  //   required: [true, "Provide confirm your password"],
  //   validate: {
  //     // Cannot use arrow function as we need to use the "this" keyword.
  //     // Also please note that from the custom validator function we are supposed to return true or false.
  //     // When we return false we get a validation error, and not when we return true.
  //     // This only works on CREATE and SAVE.
  //     validator: function (el) {
  //       return el === this.password;
  //     },
  //     message: "Passwords do not match."
  //   }
  // },
  // passwordChangedAt: Date,
  // // This is the actual password reset token stored as an encrypted string.
  // passwordResetToken: String,
  // // This is till when the last password reset token is active.
  // passwordResetExpires: Date,
  // passwordResetOtp: String,
  // passwordResetOtpExpires: Date,
  // active: {
  //   type: Boolean,
  //   default: true
  //   // select: false,
  // },
  // configSidebarIsOpen: {
  //   type: Boolean,
  //   default: true
  // },
  // configMenuType: {
  //   type: String,
  //   enum: ["static", "overlay", "slim", "sidebar", "horizontal"],
  //   default: "sidebar"
  // },
  // configColorScheme: {
  //   type: String,
  //   enum: ["light", "dark"],
  //   default: "dark"
  // },
  // configRippleEffect: {
  //   type: Boolean,
  //   default: true
  // }
});
UserSchema.plugin(diffHistory.plugin);

// VIRTUALS

// DOCUMENT MIDDLEWARE
// Between getting data and saving it to the DB.
UserSchema.pre<IUserDocument>("save", async function (next) {
  // If password was not changed then we don't need to encrypt.
  if (!this.isModified("password")) {
    return next();
  }

  // The second argument here is called the cost.
  // The higher the number the more costly it is to encrypt this password and the more costly it will be to decrypt it.
  // The hash that we have used below is the asynchronous version, there is a synchronous version also.
  this.Password = await bcrypt.hash(this.Password, 12);

  // Get rid of the passwordConfirm field, all we need this field is for the validation.
  this.Password = undefined;

  next();
});

UserSchema.pre<IUserDocument>("save", function (next) {
  if (!this.isModified("password") || this.isNew) return next();

  // sometimes it can happen that saving the below changed at date is slower than issuing the JWT.
  // in this case after changing the password the user will still not be able to login with the newly generated JWT.
  // the subtraction below.
  //this.passwordChangedAt = new Date(Date.now() - 1000);
  next();
});

// QUERY MIDDLEWARE
// Make sure we are loading only active users.
UserSchema.pre<Query<IUserDocument[], IUserDocument>>(/^find/, function (next) {
  // TODO: HP to implement generic soft delete functionaliyt.
  // this.find({ active: { $ne: false } });

  next();
});

// AGGREGATION MIDDLEWARE

// INSTANCE METHODS
UserSchema.methods.correctPassword = async function (enteredPassword, userPassword) {
  // Ideally we can do this.password, which would then be the users password and compare it against the enteredPassword
  // However since we have marked the password field as select: false, we have to explicitly expect the user password as an argument.
  // This method is supposed to return true if the password match, false otherwise.
  return await bcrypt.compare(enteredPassword, userPassword);
};

UserSchema.methods.changedPasswordAfter = function (jwtTimestamp) {
  // The passwordChangedAt property will be present on a document only for users who have changed their password at-least once.

  // To test this we need to manually create a user on whom this property has been set.
  // We need to do this as we have not yet implemented the change password functionality.
  if (this.passwordChangedAt) {
    // convert the passwordChangedAt to the same format as we have jwtTimestamp variable in.
    const changedTimestamp: number = parseInt((this.passwordChangedAt.getTime() / 1000).toString(), 10);

    // console.log(changedTimestamp, JWTTimestamp);
    // console.log(this.passwordChangedAt, JWTTimestamp);

    return jwtTimestamp < changedTimestamp;
  }

  // Password has not changed.
  return false;
};

UserSchema.methods.createPasswordResetToken = function () {
  // This token is what we are going to send to the user.
  // This is like a reset password, this is accessible only to the user to whom we are sending this over email.
  // We will send the plain text over email, and store the encypted version in the database. Then we will use the 2 to compare.
  const resetToken = crypto.randomBytes(32).toString("hex");

  // We will also encrypt the reset token.
  this.passwordResetToken = crypto.createHash("sha256").update(resetToken).digest("hex");

  console.log({ resetToken }, this.passwordResetToken);

  // expires in 10 minutes from now.
  this.passwordResetExpires = new Date(Date.now() + 10 * 60 * 1000);

  return resetToken;
};

// STATIC METHODS
UserSchema.statics.findByEmail = function (email: string) {
  return this.findOne({ email });
};

export const User = tenantAwareModel<IUserDocument, IUserModel>("User", UserSchema);

// Initialize the model once.
User({ skipTenant: true });
