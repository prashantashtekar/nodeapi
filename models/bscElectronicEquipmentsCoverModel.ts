import mongoose, { Document, Model, Schema } from "mongoose";
import { BscBurglaryHousebreakingCover } from "./bscBurglaryHousebreakingCoverModel";
import { BscCover } from "./bscCoverModel";
import { ITenantAware, tenantAwareModel, tenantUnawareModel } from "./common";
import { QuoteSlip } from "./quoteSlipModel";

/**
 * @swagger
 * components:
 *   schemas:
 *     IBscElectronicEquipmentsCover:
 *       type: object
 *       required:
 *         - locationId
 *         - descriptionEquipments
 *         - sumInsured
 *         - total
 *       properties:
 *         locationId:
 *           type: Schema.Types.ObjectId
 *         descriptionEquipments:
 *           type: string
 *         sumInsured:
 *           type: number
 *         total:
 *           type: number
 */

// Base interface
export interface IBscElectronicEquipmentsCover {
  locationId: Schema.Types.ObjectId;
  descriptionEquipments: string;
  sumInsured: Number;
  total: Number;
  clientLocationId: Schema.Types.ObjectId;
  quoteId:Schema.Types.ObjectId;
}

// IDocument
export interface IBscElectronicEquipmentsCoverDocument extends IBscElectronicEquipmentsCover, ITenantAware, Document {}

// IModel
export interface IBscElectronicEquipmentsCoverModel extends Model<IBscElectronicEquipmentsCoverDocument> {}

// Schema
const BscElectronicEquipmentsCoverSchema = new Schema<IBscElectronicEquipmentsCoverDocument, IBscElectronicEquipmentsCoverModel>({
  LocationId: {
    type: Schema.Types.ObjectId,
    ref: "ClientLocation",
    required: [true, "Electronic Equipments location is required"]
  },
  descriptionEquipments: {
    type: String
  },
  sumInsured: {
    type: Number
  },
  total: {
    type: Number
  },
  clientLocationId: {
    type: Schema.Types.ObjectId,
    ref: "ClientLocation",
    required: [true, "Client location is required"]
  },
  quoteId: {
    type: Schema.Types.ObjectId,
    ref: "QuoteSlip",
    required: [true, "Quote is required"]
  },
});

// INSTANCE METHODS
BscElectronicEquipmentsCoverSchema.methods.computeElectronicEquipmentstotalPremium = async function () {
  const quote = await QuoteSlip({ skipTenant: false, specifiedPartnerId: this.partnerId }).findOne({ _id: this.quoteId });
  const bscCover = await BscCover({ skipTenant: false, specifiedPartnerId: this.partnerId })
    .applicableBetween()
    .findOne({ bscType: "Electronic Equipments",productId: quote.productId, fromSI: { $lte: this.sumInsured }, toSI: { $gte: this.sumInsured } }); 
  if (bscCover) {
    const electronicEquipmentsTotal = (Number(this.sumInsured) * Number(bscCover.ratePerMile)) / 1000;
    return electronicEquipmentsTotal;
  } else {
    return 0;
  }
};


// Model
export const BscElectronicEquipmentsCover = tenantAwareModel<IBscElectronicEquipmentsCoverDocument, IBscElectronicEquipmentsCoverModel>(
  "BscElectronicEquipmentsCover",
  BscElectronicEquipmentsCoverSchema
);

// Initialize the model once.
BscElectronicEquipmentsCover({ skipTenant: true });
