import mongoose, { Schema } from "mongoose";

export class HistoryTypes {
  static TypeOnCreate = "onCreate";
  static TypeOnUpdate = "onUpdate";
  static TypeOnDelete = "onDelete";
}

const HistorySchema = new Schema(
  {
    eventType: {
      type: String,
      enum: ["onDelete", "onCreate", "onUpdate"]
    },
    collectionName: String,
    collectionId: Schema.Types.ObjectId,
    diff: {},
    user: {},
    reason: String,
    version: { type: Number, min: 0 }
  },
  {
    timestamps: true
  }
);

export const History = mongoose.model("History", HistorySchema);
