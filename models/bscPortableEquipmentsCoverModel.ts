import mongoose, { Document, Model, Schema } from "mongoose";
import { BscCover } from "./bscCoverModel";
import { ITenantAware, tenantAwareModel, tenantUnawareModel } from "./common";
import { QuoteSlip } from "./quoteSlipModel";

/**
 * @swagger
 * components:
 *   schemas:
 *     IBscPortableEquipmentsCover:
 *       type: object
 *       required:
 *         - equipmentType
 *         - equipmentDescription
 *         - geography
 *         - sumInsured
 *         - total
 *       properties:
 *         equipmentType:
 *           type: string
 *         equipmentDescription:
 *           type: string
 *         geography:
 *           type: string
 *         sumInsured:
 *           type: number
 *         total:
 *           type: number
 */
// Base interface
export interface IBscPortableEquipmentsCover {
  equipmentType: string;
  equipmentDescription: string;
  geography: string;
  sumInsured: Number;
  total: Number;
  clientLocationId: Schema.Types.ObjectId;
  quoteId:Schema.Types.ObjectId;
}

// IDocument
export interface IBscPortableEquipmentsCoverDocument extends IBscPortableEquipmentsCover, ITenantAware, Document {}

// IModel
export interface IBscPortableEquipmentsCoverModel extends Model<IBscPortableEquipmentsCoverDocument> {}

// Schema
const BscPortableEquipmentsCoverSchema = new Schema<IBscPortableEquipmentsCoverDocument, IBscPortableEquipmentsCoverModel>({
  equipmentType: {
    type: String,
    required: [true, "Portable Equipments Type is required"]
  },
  equipmentDescription: {
    type: String
  },
  geography: {
    type: String
  },
  sumInsured: {
    type: Number
  },
  total: {
    type: Number
  },
  clientLocationId: {
    type: Schema.Types.ObjectId,
    ref: "ClientLocation",
    required: [true, "Client location is required"]
  },
  quoteId: {
    type: Schema.Types.ObjectId,
    ref: "QuoteSlip",
    required: [true, "Quote is required"]
  },
});

// INSTANCE METHODS
BscPortableEquipmentsCoverSchema.methods.computePortableEquipmentstotalPremium = async function () {
  const quote = await QuoteSlip({ skipTenant: false, specifiedPartnerId: this.partnerId }).findOne({ _id: this.quoteId });
  const bscCover = await BscCover({ skipTenant: false, specifiedPartnerId: this.partnerId })
    .applicableBetween()
    .findOne({ bscType: "Portable Equipments",productId: quote.productId, fromSI: { $lte: this.sumInsured }, toSI: { $gte: this.sumInsured } }); 
  if (bscCover) {
    const electronicEquipmentsTotal = (Number(this.sumInsured) * Number(bscCover.ratePerMile)) / 1000;
    return electronicEquipmentsTotal;
  } else {
    return 0;
  }
};

// Model
export const BscPortableEquipmentsCover = tenantAwareModel<IBscPortableEquipmentsCoverDocument, IBscPortableEquipmentsCoverModel>(
  "BscPortableEquipmentsCover",
  BscPortableEquipmentsCoverSchema
);

// Initialize the model once.
BscPortableEquipmentsCover({ skipTenant: true });
