import mongoose, { Document, Model, Schema } from "mongoose";
import validator from "validator";
import { tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IPincodeMaster:
 *       type: object
 *       required:
 *         - name
 *         - districtId
 *         - cityId
 *         - stateId
 *         - countryId
 *         - earthquakeZone
 *       properties:
 *         name:
 *           type: string
 *         districtId:
 *           type: Schema.Types.ObjectId
 *         cityId:
 *           type: Schema.Types.ObjectId
 *         stateId:
 *           type: Schema.Types.ObjectId
 *         countryId:
 *           type: Schema.Types.ObjectId
 *         earthquakeZone:
 *           type: string
 */

// Base interface
export interface IPincodeMaster {
  name: string;
  districtId: Schema.Types.ObjectId;
  cityId: Schema.Types.ObjectId;
  stateId: Schema.Types.ObjectId;
  countryId: Schema.Types.ObjectId;
  earthquakeZone: string;
}

// IDocument
export interface IPincodeMasterDocument extends IPincodeMaster, Document {}

// IModel
export interface IPincodeMasterModel extends Model<IPincodeMasterDocument> {}

// Schema
const PincodeMasterSchema = new Schema<IPincodeMasterDocument, IPincodeMasterModel>({
  name: {
    type: String,
    required: [true, "A Pincode must have a name"],
    unique: true,
    trim: true,
    maxLength: [10, "A Pincode name must have less than or equal to 10 characters."]
  },
  districtId: {
    type: Schema.Types.ObjectId,
    ref: "DistrictMaster",
    required: [true, "Pincode must have a district"]
  },
  cityId: {
    type: Schema.Types.ObjectId,
    ref: "CityMaster",
    required: [true, "Pincode must have a city"]
  },
  stateId: {
    type: Schema.Types.ObjectId,
    ref: "StateMaster",
    required: [true, "Pincode must have a state"]
  },
  countryId: {
    type: Schema.Types.ObjectId,
    ref: "CountryMaster",
    required: [true, "Pincode must have a country"]
  },
  earthquakeZone: {
    type: String,
    enum: {
      values: ["I", "II", "III", "IV"],
      message: "Earthquake Zone is either: I, II, III, IV"
    }
  }
});

// Model
export const PincodeMaster = tenantUnawareModel<IPincodeMasterDocument, IPincodeMasterModel>("PincodeMaster", PincodeMasterSchema);

// Initialize the model once.
PincodeMaster();
