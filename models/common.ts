import mongoose, { Model, Schema } from "mongoose";
import { AppError } from "../utils/appError";
import { AsyncLocalStorage } from "async_hooks";
import { IAsyncStoreCtxt } from "../interfaces";

export const requestAsyncLocalStorageCtxt = new AsyncLocalStorage<IAsyncStoreCtxt>();

export interface IApplicableDateRangeAware {
  applicableFrom: Date;
  applicableTo: Date;
}

export function applicableBetween() {
  const datenow = new Date();
  return this.find({ applicableFrom: { $lte: datenow } }, { applicableTo: { $gte: datenow } });
}

export interface ISumInsuredBetweenAware {
  fromSI: Number;
  toSI: Number;
}
export function sumInsuredBetween() {
  return this.find({ fromSI: { $lte: 0 } }, { toSI: { $gte: 1000 } });
};

export interface ITenantAware {
  tenantId: String;
  partnerId: String;
}

export interface ITenantModelProps {
  skipTenant: Boolean;
  specifiedPartnerId?: String;
}

// https://thecodebarbarian.com/2015/07/24/guide-to-mongoose-discriminators
// Mindset behind implementing the multi-tenancy using the mongoose discriminators.
export function tenantAwareModel<D extends ITenantAware, M extends Model<D>>(name: string, schema: Schema<D, M>) {
  return (props: ITenantModelProps = { skipTenant: false }): M => {
    schema.add({
      tenantId: String
    });
    schema.add({
      partnerId: {
        type: Schema.Types.ObjectId,
        ref: "Partner"
      }
    });

    const Model = mongoose.model<D, M>(name, schema);

    // If skip tenant is specified then we return the model AS-IS.
    const { skipTenant, specifiedPartnerId } = props;
    if (skipTenant) {
      return Model;
    }

    // Next we try to resolve the tenant.
    // TODO: For some reason if we are unable to get a valid tenantId at this stage then do we want to raise an exception?
    // Or do we stamp the ID of the common partner discriminator?

    // @ts-ignore
    const requestTenantId = requestAsyncLocalStorageCtxt.getStore()?.tenantId;
    // @ts-ignore
    // const mongooseTenantId = mongooseAsyncLocalStorageCtxt.getStore()?.tenantId;
    // const tenantId = requestTenantId || mongooseTenantId || specifiedPartnerId;
    const tenantId = requestTenantId || specifiedPartnerId;

    // For admin users we return immediately.
    if (tenantId === "-666") {
      return Model;
    }

    if (!tenantId) {
      // If tenantId was not found, and we are not the admin user then we throw an error.
      throw new AppError("Invoking tenantAwareModel, but unable to resolve tenantId.", 500);
    }

    // At this point we are sure that a tenant has been resolved.

    // Registered a document middleware that will stamp the partnerId based on the current tenant.
    schema.pre<D>("save", function (next) {
      // If the entity being saved does not have the partner already, then we set the current tenantId as the partnerId.
      if (!this.partnerId && tenantId) {
        this.partnerId = tenantId;
      }

      next();
    });

    Model.schema.set("discriminatorKey", "tenantId");

    const discriminatorName = `${Model.modelName}-${tenantId}`;
    const existingDiscriminator = (Model.discriminators || {})[discriminatorName];
    const r = existingDiscriminator || Model.discriminator(discriminatorName, new Schema<D, M>({}));

    return r as M;
  };
}

export function tenantUnawareModel<D, M extends Model<D>>(name: string, schema: Schema<D, M>) {
  return () => mongoose.model<D, M>(name, schema);
}
