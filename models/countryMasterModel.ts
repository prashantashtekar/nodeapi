import mongoose, { Document, Model, Schema } from "mongoose";
import validator from "validator";
import { tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     ICountryMaster:
 *       type: object
 *       required:
 *         - name
 *       properties:
 *         name:
 *           type: string
 */

// Base interface
export interface ICountryMaster {
  name: string;
}

// IDocument
export interface ICountryMasterDocument extends ICountryMaster, Document {}

// IModel
export interface ICountryMasterModel extends Model<ICountryMasterDocument> {}

// Schema
const CountryMasterSchema = new Schema<ICountryMasterDocument, ICountryMasterModel>({
  name: {
    type: String,
    required: [true, "A Country must have a name"],
    unique: true,
    trim: true,
    maxLength: [128, "A Country name must have less than or equal to 128 characters."]
  }
});

// Model
// export const CountryMaster = mongoose.model<ICountryMasterDocument, ICountryMasterModel>('CountryMaster', CountryMasterSchema);
export const CountryMaster = tenantUnawareModel<ICountryMasterDocument, ICountryMasterModel>("CountryMaster", CountryMasterSchema);

// Initialize the model once.
CountryMaster();
