import mongoose, { Document, Model, Schema } from "mongoose";
import { BscCover } from "./bscCoverModel";
import { ClientLocation } from "./clientLocationModel";
import { ITenantAware, tenantAwareModel, tenantUnawareModel } from "./common";
import { EarthquakeRateMaster } from "./earthquakeRateMasterModel";
import { Occupancy } from "./occupancyModel";
import { PincodeMaster } from "./pincodeMasterModel";
import { QuoteLocationOccupancy } from "./quoteLocationOccupancyModel";
import { QuoteSlip } from "./quoteSlipModel";
import { TerrorismRateMaster } from "./terrorismRateMasterModel";

/**
 * @swagger
 * components:
 *   schemas:
 *     IBscFireLossOfProfitCover:
 *       type: object
 *       required:
 *         - grossProfit
 *         - indmenityPeriod
 *         - terrorism
 *         - auditorsFees
 *       properties:
 *         grossProfit:
 *           type: string
 *         indmenityPeriod:
 *           type: string
 *         terrorism:
 *           type: boolean
 *         auditorsFees:
 *           type: number
 */

// Base interface
export interface IBscFireLossOfProfitCover {
  grossProfit: string;
  indmenityPeriod: string;
  terrorism: Boolean;
  auditorsFees: Number;
  clientLocationId: Schema.Types.ObjectId;
  quoteId: Schema.Types.ObjectId;
}

// IDocument
export interface IBscFireLossOfProfitCoverDocument extends IBscFireLossOfProfitCover, ITenantAware, Document {}

// IModel
export interface IBscFireLossOfProfitCoverModel extends Model<IBscFireLossOfProfitCoverDocument> {}

// Schema
const BscFireLossOfProfitCoverSchema = new Schema<IBscFireLossOfProfitCoverDocument, IBscFireLossOfProfitCoverModel>({
  grossProfit: {
    type: Number,
    required: [true, ""]
  },
  indmenityPeriod: {
    type: String,
    required: [true, "A Fire Loss  must have a Indmenity Period"],
    enum: {
      values: ["01 Month", "02 Months", "03 Months", "04 Months", "05 Months", "06 Months", "07 Months", "08 Months", "09 Months", "10 Months", "11 Months", "12 Months"],
      message: "Indmenity Period must is either: 01 Month-02 Months-03 Months-04 Months-05 Months-06 Months-07 Months-08 Months-09 Months-10 Months-11 Months-12 Months."
    }
  },
  terrorism: {
    type: Boolean
  },

  auditorsFees: {
    type: Number,
    required: [true, "A Fire Loss  must have a Auditors Fees"]
  },
  clientLocationId: {
    type: Schema.Types.ObjectId,
    ref: "ClientLocation",
    required: [true, "Client location is required"]
  },
  quoteId: {
    type: Schema.Types.ObjectId,
    ref: "QuoteSlip",
    required: [true, "Quote is required"]
  }
});

// INSTANCE METHODS
BscFireLossOfProfitCoverSchema.methods.computeFloptotalPremium = async function () {
  let fire = 0;
  let stfi = 0;
  const months = Number(this.indmenityPeriod.split(" ")[0]);
  const quote = await QuoteSlip({ skipTenant: false, specifiedPartnerId: this.partnerId }).findOne({ _id: this.quoteId });
  const clientLocation = await ClientLocation({ skipTenant: false, specifiedPartnerId: this.partnerId }).findOne({ _id: this.clientLocationId });
  const pincode = await PincodeMaster().findOne({ _id: clientLocation.pincodeId });
  const earthquakeZone = await EarthquakeRateMaster({ skipTenant: false, specifiedPartnerId: this.partnerId }).applicableBetween().findOne({
    zone: pincode.earthquakeZone,
    // industryTypeId: industryType._id,
    productId: quote.productId
  });
  const terrorismRate = await TerrorismRateMaster({ skipTenant: false, specifiedPartnerId: this.partnerId })
    .applicableBetween()
    .findOne({
      fromSI: { $lte: Number(this.grossProfit) },
      toSI: { $gte: Number(this.grossProfit) },
      productId: quote.productId
    });

  const quoteLocationOccupancy = await QuoteLocationOccupancy({ skipTenant: false, specifiedPartnerId: this.partnerId }).findOne({
    clientLocationId: this.clientLocationId,
    quoteId: this.quoteId
  });
  if (quoteLocationOccupancy){
    const occupancy = await Occupancy({ skipTenant: false, specifiedPartnerId: this.partnerId }).findOne({_id:quoteLocationOccupancy.occupancyId})
    let fire = occupancy.flexaiib;
    let stfi = occupancy.stfiRate
  }  
  const bscCover = await BscCover({ skipTenant: false, specifiedPartnerId: this.partnerId })
    .applicableBetween()
    //  BscCover({ skipTenant: false, specifiedPartnerId: this.partnerId }).sumInsuredBetween()
    .findOne({ bscType: "Fire Loss of Profit", productId: quote.productId, fromSI: { $lte: Number(this.grossProfit) }, toSI: { $gte: Number(this.grossProfit) } });
  if (bscCover) {
    const flopTotal = Number(this.grossProfit) * Number(fire + stfi + earthquakeZone.rate + terrorismRate.ratePerMile) * (months / 12);
    return flopTotal;
  } else {
    return 0;
  }
};

// Model
// export const CityMaster = mongoose.model<IBscFireLossOfProfitCoverDocument, IBscFireLossOfProfitCoverModel>('BscFireLossOfProfitCover', BscFireLossOfProfitCoverSchema);
export const BscFireLossOfProfitCover = tenantAwareModel<IBscFireLossOfProfitCoverDocument, IBscFireLossOfProfitCoverModel>(
  "BscFireLossOfProfitCover",
  BscFireLossOfProfitCoverSchema
);

// Initialize the model once.
BscFireLossOfProfitCover({ skipTenant: true });
