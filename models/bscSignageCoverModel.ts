import mongoose, { Document, Model, Schema } from "mongoose";
import { ITenantAware, tenantAwareModel, tenantUnawareModel } from "./common";

/**
 * @swagger
 * components:
 *   schemas:
 *     IBscSignageCover:
 *       type: object
 *       required:
 *         - signageType
 *         - signageDescription
 *         - sumInsured
 *         - total
 *       properties:
 *         signageType:
 *           type: string
 *         signageDescription:
 *           type: string
 *         sumInsured:
 *           type: number
 *         total:
 *           type: number
 */

// Base interface
export interface IBscSignageCover {
  signageType: string;
  signageDescription: string;
  sumInsured: Number;
  total: Number;
  clientLocationId: Schema.Types.ObjectId;
  quoteId:Schema.Types.ObjectId;
}

// IDocument
export interface IBscSignageCoverDocument extends IBscSignageCover, Document, ITenantAware {}

// IModel
export interface IBscSignageCoverModel extends Model<IBscSignageCoverDocument> {}

// Schema
const BscSignageCoverSchema = new Schema<IBscSignageCoverDocument, IBscSignageCoverModel>({
  signageType: {
    type: String,
    required: [true, "Baggage Type is required"]
  },
  signageDescription: {
    type: String
  },
  sumInsured: {
    type: Number
  },
  total: {
    type: Number
  },
  clientLocationId: {
    type: Schema.Types.ObjectId,
    ref: "ClientLocation",
    required: [true, "Client location is required"]
  },
  quoteId: {
    type: Schema.Types.ObjectId,
    ref: "QuoteSlip",
    required: [true, "Quote is required"]
  },
});

// Model
export const BscSignageCover = tenantAwareModel<IBscSignageCoverDocument, IBscSignageCoverModel>("BscSignageCover", BscSignageCoverSchema);

// Initialize the model once.
BscSignageCover({ skipTenant: true });
