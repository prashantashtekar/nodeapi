// Express specific overrides

import * as core from 'express-serve-static-core';
import express from 'express';
import { IUserDocument } from './models/userModel';
import { IQueryString } from './utils/filterHelper';

export interface Query extends core.Query {}

export interface Params extends core.ParamsDictionary {}

export interface Request<
  ReqBody = any,
  ReqQuery = Query | IQueryString,
  URLParams extends Params = core.ParamsDictionary
> extends express.Request<URLParams, any, ReqBody, ReqQuery> {
  user?: IUserDocument;
}

// This has been added here just so that we are able to import the request and response in one go.
// Otherwise we would have to import the Request from overrides.ts and the Response from express.
export interface Response extends express.Response {}

export interface NextFunction extends express.NextFunction {}

// NodeJS specific overrides.
declare global {
  namespace NodeJS {
    interface ProcessEnv {
      NODE_ENV: 'development' | 'production';
      PORT?: number;
      USER: string;
      DATABASE_LOCAL: string;
      DATABASE_PASSWORD: string;
      JWT_SECRET: string;
      JWT_EXPIRES_IN: string;
      JWT_COOKIE_EXPIRES_IN_DAYS: number;
      EMAIL_USERNAME: string;
      EMAIL_PASSWORD: string;
      EMAIL_HOST: string;
      EMAIL_PORT: string;
      EMAIL_FROM: string;
    }
  }
}
