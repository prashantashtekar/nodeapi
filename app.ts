import express, { Express, Request, Response } from "express";
import rateLimit from "express-rate-limit";
import helmet from "helmet";
import mongoSanitize from "express-mongo-sanitize";
import xss from "xss-clean";
import hpp from "hpp";

import morgan from "morgan";
import cors from "cors";
import { router as tourRouter } from "./routes/tourRoutes";
import { router as userRouter } from "./routes/userRoutes";
import { router as reviewRouter } from "./routes/reviewRoutes";
import { router as partnerRouter } from "./routes/partnerRoutes";
import { router as countryMasterRouter } from "./routes/countryMasterRoutes";
import { router as stateMasterRouter } from "./routes/stateMasterRoutes";
import { router as cityMasterRouter } from "./routes/cityMasterRoutes";
import { router as districtMasterRouter } from "./routes/districtMasterRoutes";
import { router as pincodeMasterRouter } from "./routes/pincodeMasterRoutes";
import { router as industryTypeMasterRouter } from "./routes/industryTypeMasterRoutes";
import { router as clientGroupMasterRouter } from "./routes/clientGroupMasterRoutes";
import { router as clientRouter } from "./routes/clientRoutes";
import { router as quoteLocationOccupancyRouter } from "./routes/quoteLocationOccupancyRoutes";
import { router as productMasterRouter } from "./routes/productMasterRoutes";
import { router as clientKycRouter } from "./routes/clientKycRoutes";
import { router as clientLocationRouter } from "./routes/clientLocationRoutes";
import { router as quoteSlipRouter } from "./routes/quoteSlipRoutes";
import { router as hazardCategoryMasterRouter } from "./routes/hazardCategoryMasterRoutes";
import { router as addOnCoversRouter } from "./routes/addonCoversRoutes";
import { router as sectorMasterRouter } from "./routes/sectorMasterRoutes";
import { router as addOnCoverSectorRouter } from "./routes/addonCoverSectorRoutes";
import { router as occupancyRouter } from "./routes/occupancyRoutes";
import { router as bscAccompaniedBaggageCoverRouter } from "./routes/bscAccompaniedBaggageCoverRoutes";
import { router as bscBurglaryHousebreakingCoverRouter } from "./routes/bscBurglaryHousebreakingCoverRoutes";
import { router as bscElectronicEquipmentsCoverRouter } from "./routes/bscElectronicEquipmentsCoverRoutes";
import { router as bscFidelityGuaranteeCoverRouter } from "./routes/bscFidelityGuaranteeCoverRoutes";
import { router as bscFireLossOfProfitCoverRouter } from "./routes/bscFireLossOfProfitCoverRoutes";
import { router as bscFixedPlateGlassCoverRouter } from "./routes/bscFixedPlateGlassCoverRoutes";
import { router as bscLiabilitySectionCoverRouter } from "./routes/bscLiabilitySectionCoverRoutes";
import { router as bscMoneySafeTillCoverRouter } from "./routes/bscMoneySafeTillCoverRoutes";
import { router as bscMoneyTransitCoverRouter } from "./routes/bscMoneyTransitCoverRoutes";
import { router as bscPortableEquipmentsCoverRouter } from "./routes/bscPortableEquipmentsCoverRoutes";
import { router as bscSignageCoverRouter } from "./routes/bscSignageCoverRoutes";
import { router as quoteLocationAddonCoversRouter } from "./routes/quoteLocationAddonCoversRoutes";
import { router as bscCoverRouter } from "./routes/bscCoverRoutes";
import { router as bscDiscountCoverRouter } from "./routes/bscDiscountCoverRoutes";
import { router as terrorismRateMasterRouter } from "./routes/terrorismRateMasterRoutes";
import { router as earthquakeRateMasterRouter } from "./routes/earthquakeRateMasterRoutes";
import { router as buggyRouter } from "./routes/buggyRoutes";

import { AppError } from "./utils/appError";
import { globalErrorHandler } from "./controllers/errorController";
// import { bindCurrentNamespace } from './utils/storage';
import swaggerJSDoc from "swagger-jsdoc";
import swaggerUi from "swagger-ui-express";

export const app: Express = express();

// ############################
// Middleware chain...
// ############################

// 1. set security HTTP headers
app.use(helmet({ crossOriginResourcePolicy: false }));

// 2. development logging
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
  app.use(cors());
  console.log("Development-PA")
} else {
  const whitelist = ["http://uat.inexchange.co.in", "https://uat.inexchange.co.in", "http://uatapi.inexchange.co.in", "https://uatapi.inexchange.co.in"];

  const corsOptions = {
    origin: function (origin, callback) {
      console.log(`Cors whitelist: ${whitelist}`);
      console.log(`Cors origin: ${origin}`);

      if (!origin || whitelist.indexOf(origin) !== -1) {
        callback(null, true);
      } else {
        callback(new Error("Not allowed by CORS"));
      }
    }
  };

  app.use(cors(corsOptions));
}

// 3. Limit requests from same IP
// This will allow 100 requests from a given IP in one hour.
const limiter = rateLimit({
  max: 10000,
  windowMs: 60 * 60 * 1000,
  message: "Too many requests from this IP, please try again in an hour!"
});
// This limiter will be applied to only routes beginning with /api.
app.use("/api", limiter);

// 4. Body parser, reading data from body in req.body.
app.use(express.json({ limit: "10kb" }));

// 5. Data sanitization against NoSQL query injection.
// What this middleware does is to look at the request body, query string and params and remove all the dollars, dots etc.
app.use(mongoSanitize());

// 6. Data sanitization against XSS
app.use(xss());

// 7. Prevent parameter pollution.
app.use(
  hpp({
    whitelist: ["duration", "ratingsQuantity", "ratingsAverage", "maxGroupSize", "difficulty", "price"]
  })
);

// 8. Serving static files.
console.log(__dirname);
app.use(express.static(`${__dirname}/public`));

// 9. Test middleware.
app.use((req: Request, res: Response, next) => {
  // req.requestTime = new Date().toISOString();
  // console.log(req.headers);

  next();
});

// 9.1 Continuation local storage middleware.
// app.use(bindCurrentNamespace);

// 9.2 Swagger : https://dev.to/kabartolo/how-to-document-an-express-api-with-swagger-ui-and-jsdoc-50do
const swaggerDefinition = {
  openapi: "3.0.0",
  info: {
    title: "Express API for QMS",
    version: "1.0.0",
    description: "This is the REST API specification for the Qms application.",
    // license: {
    //     name: 'Licensed Under MIT',
    //     url: 'https://spdx.org/licenses/MIT.html',
    // },
    contact: {
      name: "Himanshu",
      url: "https://bitsteps.in"
    }
  },
  servers: [
    {
      url: "http://localhost:8000",
      description: "Development server"
    }
  ]
};
const options = {
  swaggerDefinition,
  // Paths to files containing OpenAPI definitions
  apis: ["./routes/*Routes.ts", "./models/*Model.ts"]
};

const swaggerSpec = swaggerJSDoc(options);
app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec));

// 10. Routes
app.use("/api/v1/tours", tourRouter);
app.use("/api/v1/users", userRouter);
app.use("/api/v1/reviews", reviewRouter);
app.use("/api/v1/partners", partnerRouter);
app.use("/api/v1/countries", countryMasterRouter);
app.use("/api/v1/states", stateMasterRouter);
app.use("/api/v1/cities", cityMasterRouter);
app.use("/api/v1/districts", districtMasterRouter);
app.use("/api/v1/pincodes", pincodeMasterRouter);
app.use("/api/v1/industryTypes", industryTypeMasterRouter);
app.use("/api/v1/client-groups", clientGroupMasterRouter);
app.use("/api/v1/clients", clientRouter);
app.use("/api/v1/quotes", quoteSlipRouter);
app.use("/api/v1/quote-locations", quoteLocationOccupancyRouter);
app.use("/api/v1/products", productMasterRouter);
app.use("/api/v1/client-kyc-masters", clientKycRouter);
app.use("/api/v1/client-locations", clientLocationRouter);
app.use("/api/v1/hazard-categories", hazardCategoryMasterRouter);
app.use("/api/v1/addOn-covers", addOnCoversRouter);
app.use("/api/v1/sector-masters", sectorMasterRouter);
app.use("/api/v1/addOn-cover-sectors", addOnCoverSectorRouter);
app.use("/api/v1/occupancy", occupancyRouter);
app.use("/api/v1/quote-location-addon-covers", quoteLocationAddonCoversRouter);
app.use("/api/v1/bsc-accompanied-baggage-cover", bscAccompaniedBaggageCoverRouter);
app.use("/api/v1/bsc-burglary-housebreaking-cover", bscBurglaryHousebreakingCoverRouter);
app.use("/api/v1/bsc-electronic-equipments-cover", bscElectronicEquipmentsCoverRouter);
app.use("/api/v1/bsc-fidelity-guarantee-cover", bscFidelityGuaranteeCoverRouter);
app.use("/api/v1/bsc-fireLossOfProfit-cover", bscFireLossOfProfitCoverRouter);
app.use("/api/v1/bsc-fixed-plate-glass-cover", bscFixedPlateGlassCoverRouter);
app.use("/api/v1/bsc-liability-section-cover", bscLiabilitySectionCoverRouter);
app.use("/api/v1/bsc-money-safe-till-cover", bscMoneySafeTillCoverRouter);
app.use("/api/v1/bsc-money-transit-cover", bscMoneyTransitCoverRouter);
app.use("/api/v1/bsc-portable-equipments-cover", bscPortableEquipmentsCoverRouter);
app.use("/api/v1/bsc-signage-cover", bscSignageCoverRouter);
app.use("/api/v1/bsc-cover", bscCoverRouter);
app.use("/api/v1/bsc-discount-cover", bscDiscountCoverRouter);
app.use("/api/v1/terrorism-rate-masters", terrorismRateMasterRouter);
app.use("/api/v1/earthquake-rate-masters", earthquakeRateMasterRouter);
app.use("/api/v1/buggy", buggyRouter);

// 11. Catch all middleware.
app.all("*", (req: Request, res: Response, next) => {
  // const error = new Error(`Can't find ${req.originalUrl}`);
  // error.status = 'fail';
  // error.statusCode = 404;

  next(new AppError(`Can't find ${req.originalUrl}`, 404));
});

// 12. Error middleware.
app.use(globalErrorHandler);
