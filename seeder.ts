import dotenv from "dotenv";
dotenv.config({ path: "./config.env" });

import fs from "fs";
import mongoose from "mongoose";
import { CountryMaster } from "./models/countryMasterModel";
import { StateMaster } from "./models/stateMasterModel";
import { Partner } from "./models/partnerModel";
import { CityMaster } from "./models/cityMasterModel";
import { DistrictMaster } from "./models/districtMasterModel";
import { PincodeMaster } from "./models/pincodeMasterModel";
import { Occupancy } from "./models/occupancyModel";
import csv from "csvtojson";
import { HazardCategoryMaster } from "./models/hazardCategoryMasterModel";
import { ClientGroupMaster } from "./models/clientGroupMasterModel";
import { Client } from "./models/clientModel";
import { ClientLocation } from "./models/clientLocationModel";
import { SectorMaster } from "./models/sectorMasterModel";
import { IndustryTypeMaster } from "./models/industryTypeModel";
import { User, _USER_ROLES } from "./models/userModel";
import { AddOnCover } from "./models/addonCoversModel";
import { AddOnCoverSector } from "./models/addonCoverSectorModel";
import { EarthquakeRateMaster } from "./models/earthquakeRateMasterModel";
import { ProductMaster } from "./models/productMasterModel";
import { TerrorismRateMaster } from "./models/terrorismRateMasterModel";
import { BscCover } from "./models/bscCoverModel";
import { BscDiscountCover } from "./models/bscDiscountCoverModel";

const DATABASE_URL = process.env.DATABASE_URL.replace("<DATABASE_USER>", process.env.DATABASE_USER).replace("<DATABASE_PASSWORD>", process.env.DATABASE_PASSWORD);
console.log(`Using Database URL: ${DATABASE_URL}`);

mongoose
  .connect(DATABASE_URL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  })
  .then(() => console.log("DB connection successful!"));

const _SELF_PARTNER = "In Exchange";
const _BROKER_PARTNER = "LogicLoop";
const _INSURER_PARTNER = "Apple";
const _LAGHU_PRODUCT = "Bharat Laghu Udyam Suraksha";

// READ JSON FILE_BROKER_PARTNER
const baseDir = `${__dirname}/seed_data`;
const countries = JSON.parse(fs.readFileSync(`${baseDir}/countries.json`, "utf-8"));
const states = JSON.parse(fs.readFileSync(`${baseDir}/states.json`, "utf-8"));
const cities = JSON.parse(fs.readFileSync(`${baseDir}/cities.json`, "utf-8"));
const districts = JSON.parse(fs.readFileSync(`${baseDir}/districts.json`, "utf-8"));
const pincodes = JSON.parse(fs.readFileSync(`${baseDir}/pincodes.json`, "utf-8"));
const partners = JSON.parse(fs.readFileSync(`${baseDir}/partners.json`, "utf-8"));
const hazardCategory = JSON.parse(fs.readFileSync(`${baseDir}/hazardCategories.json`, "utf-8"));
const clientsGroups = JSON.parse(fs.readFileSync(`${baseDir}/clientsGroup.json`, "utf-8"));
const clientsdata = JSON.parse(fs.readFileSync(`${baseDir}/clients.json`, "utf-8"));
const clientsLocationdata = JSON.parse(fs.readFileSync(`${baseDir}/clientLocation.json`, "utf-8"));
const productdata = JSON.parse(fs.readFileSync(`${baseDir}/laghuProduct.json`, "utf-8"));
const earthquakedata = JSON.parse(fs.readFileSync(`${baseDir}/earthquakeRate.json`, "utf-8"));
const bscCovers = JSON.parse(fs.readFileSync(`${baseDir}/bscCover.json`, "utf-8"));
const bscDiscountCovers = JSON.parse(fs.readFileSync(`${baseDir}/bscDiscountCover.json`, "utf-8"));

// IMPORT DATA INTO DB
const importData = async () => {
  try {
    // Import the countries.
    await CountryMaster().create(countries);

    // Import the states.
    await importStates();

    // Import the cities
    await importCities();

    // Import the districts
    await importDistricts();

    // Import the pincode
    await importPincodes();

    // Import the partners.
    await importPartners();

    // Import the hazard master
    await HazardCategoryMaster().create(hazardCategory);

    // Import the users.
    await importUsers();

    // Import the Products.
    await ProductMaster().create(productdata);

    // Create the industry types.
    await IndustryTypeMaster().create([
      { industryTypeName: "Industrial" },
      { industryTypeName: "Non Industrial" },
      { industryTypeName: "Residential" },
      { industryTypeName: "Residential Society" }
    ]);

    // Import the occupancy master
    await importLaghuOccupancies();

    // Import the Cleint Groups Master
    await importClientsGroup();

    // Import the Cleints
    await importClients();

    // Import the Cleints Location
    await importClientsLocation();

    // Import earthquakerates
    await importLaghuEarthquakeRates();

    // Import BSC Covers
    await importBscCovers();

    // Import BSC Discount Cover
    await importBscDiscountCover();

    // Create the sectors.
    await SectorMaster().create([
      { name: "Metal/Steel" },
      { name: "Cement" },
      { name: "Fertilizer & Petrochemical" },
      { name: "Plastic Mfg" },
      { name: "Chemicals" },
      { name: "Transmission & Distribution Lines" },
      { name: "Textiles" },
      { name: "Non-Industrial" },
      { name: "Banking" },
      { name: "Engineering Workshop" },
      { name: "Pharma" },
      { name: "Food" },
      { name: "Power-Conventional" },
      { name: "Power-Renewable" },
      { name: "IT/ITES-Software Development" },
      { name: "Storage" },
      { name: "Hazardous Goods Manufacturing" },
      { name: "Rubber Goods Manufacturing" },
      { name: "Infrastructure" },
      { name: "Paper" },
      { name: "Silent" }
    ]);

    // Import the addons.
    await importLaghuAddons();

    // Import terrorism rates
    await importLaghuTerrorismRates();

    console.log("Data successfully loaded!");
  } catch (err) {
    console.log(err);
  }
  process.exit();
};

async function importStates() {
  for (let i = 0; i < states.length; i++) {
    const state = states[i];
    const countryName = state.countryName;

    // 1. Find the country given the countryName
    const country = await CountryMaster().findOne({ name: countryName });

    // 2. Insert the state.
    if (country) {
      delete state.countryName;
      await StateMaster().create({ ...state, countryId: country._id });
    } else {
      console.log(`Attempting to create state with country: ${countryName}, but unable to resolve a document with the given country name.`);
      process.exit(1);
    }
  }
}

async function importCities() {
  for (let i = 0; i < cities.length; i++) {
    const city = cities[i];
    const stateName = city.stateName;

    // 1. Find the country given the stateName
    const state = await StateMaster().findOne({ name: stateName });

    // 2. Insert the city.
    if (state) {
      delete city.stateName;
      await CityMaster().create({ ...city, stateId: state._id });
    } else {
      console.log(`Attempting to create city with state: ${stateName}, but unable to resolve a document with the given state name.`);
      process.exit(1);
    }
  }
}

async function importDistricts() {
  for (let i = 0; i < districts.length; i++) {
    const district = districts[i];
    const stateName = district.stateName;

    // 1. Find the country given the stateName
    const state = await StateMaster().findOne({ name: stateName });

    // 2. Insert the district.
    if (state) {
      delete district.stateName;
      await DistrictMaster().create({ ...district, stateId: state._id });
    } else {
      console.log(`Attempting to create district with state: ${stateName}, but unable to resolve a document with the given state name.`);
      process.exit(1);
    }
  }
}

async function importPincodes() {
  for (let i = 0; i < pincodes.length; i++) {
    const pincode = pincodes[i];
    const countryName = pincode.countryName;
    const stateName = pincode.stateName;
    const districtName = pincode.districtName;
    const cityName = pincode.cityName;

    // 1. Find the country given the stateName
    const country = await CountryMaster().findOne({ name: countryName });
    const state = await StateMaster().findOne({ name: stateName });
    const district = await DistrictMaster().findOne({ name: districtName });
    const city = await CityMaster().findOne({ name: cityName });

    // 2. Insert the district.
    if (country && state && city && district) {
      delete pincode.countryName;
      delete pincode.stateName;
      delete pincode.districtName;
      delete pincode.cityName;
      await PincodeMaster().create({
        ...pincode,
        countryId: country._id,
        stateId: state._id,
        districtId: district._id,
        cityId: city._id
      });
    } else {
      console.log(`Attempting to create pincode, but unable to resolve a document with the given parent references: ${JSON.stringify(pincode)}`);
      process.exit(1);
    }
  }
}

async function importPartners() {
  for (let i = 0; i < partners.length; i++) {
    const partner = partners[i];

    const countryName = partner.countryName;
    const stateName = partner.stateName;
    const districtName = partner.districtName;
    const cityName = partner.cityName;
    const pincodeName = partner.pincode;

    const country = await CountryMaster().findOne({ name: countryName });
    const state = await StateMaster().findOne({ name: stateName });
    const district = await DistrictMaster().findOne({ name: districtName });
    const city = await CityMaster().findOne({ name: cityName });
    const pincode = await PincodeMaster().findOne({ name: pincodeName });

    if (city && district && state && pincode && country) {
      delete partner.countryName;
      delete partner.stateName;
      delete partner.pincodeName;
      delete partner.cityName;
      delete partner.districtName;

      await Partner().create({
        ...partner,
        countryId: country._id,
        stateId: state._id,
        districtId: district._id,
        cityId: city._id,
        pincodeId: pincode._id
      });
    } else {
      console.log(`Attempting to create partner, but unable to resolve a document with the given parent references: ${JSON.stringify(partner)}`);
      process.exit(1);
    }
  }
}

async function importUsers() {
  // First get hold of the broker partner.
  const brokerPartner = await Partner().findOne({ name: _BROKER_PARTNER });

  // Create the broker admin
  await User({ skipTenant: true }).create({
    name: "Super Admin (Logicloop)",
    mobileNumber: "9988998801",
    configMenuType:'horizontal',
    email: "hasan@gmail.com",
    password: "test1234",
    passwordConfirm: "test1234",
    role: _USER_ROLES.Creator,
    partnerId: brokerPartner._id,
    tenantId: `User-${brokerPartner._id}`
  });

  // First get hold of the insurer partner.
  const insurerPartner = await Partner().findOne({ name: _INSURER_PARTNER });

  // Create the broker admin
  await User({ skipTenant: true }).create({
    name: "Jay Insurer (Apple)",
    configMenuType:'horizontal',
    mobileNumber: "9988998800",
    email: "creatorb1@gmail.com",
    password: "pass@123",
    passwordConfirm: "test1234",
    role: _USER_ROLES.Approver,
    partnerId: insurerPartner._id,
    tenantId: `User-${insurerPartner._id}`
  });

  const selfPartner = await Partner().findOne({ name: _SELF_PARTNER });

  // Create the admin
  await User({ skipTenant: true }).create({
    name: "Jeff Bezos (Amazon)",
    mobileNumber: "911911911911",
    configMenuType:'horizontal',
    email: "trump@test.com",
    password: "test1234",
    passwordConfirm: "test1234",
    role: _USER_ROLES.SuperAdmin,
    partnerId: selfPartner._id,
    tenantId: `User-${selfPartner._id}`
  });
}

async function importLaghuEarthquakeRates() {
  const selfPartner = await Partner().findOne({ name: _SELF_PARTNER });
  const laghuProduct = await ProductMaster().findOne({ type: _LAGHU_PRODUCT });

  for (let i = 0; i < earthquakedata.length; i++) {
    const earthquakeRate = earthquakedata[i];
    const industryTypeName = earthquakeRate.industryType;

    const industryType = await IndustryTypeMaster().findOne({
      industryTypeName: industryTypeName
    });

    await EarthquakeRateMaster({
      skipTenant: true
    }).create({
      ...earthquakeRate,
      industryTypeId: industryType._id,
      partnerId: selfPartner._id,
      productId: laghuProduct._id
    });
  }
}

async function importLaghuOccupancies() {
  const selfPartner = await Partner().findOne({ name: _SELF_PARTNER });
  const laghuProduct = await ProductMaster().findOne({ type: _LAGHU_PRODUCT });

  const occupanciesCsv = `${baseDir}/occupancy-master.csv`;
  const occupancies = await csv().fromFile(occupanciesCsv);
  for (let i = 0; i < occupancies.length; i++) {
    const occupancy = occupancies[i];
    const hazardCategoryName = occupancy.hazardCategoryName;
    const industryTypeName = occupancy.industryTypeName;

    const hazardCategory = await HazardCategoryMaster().findOne({
      category: hazardCategoryName
    });
    const industryType = await IndustryTypeMaster().findOne({
      industryTypeName: industryTypeName
    });

    if (hazardCategory) {
      delete occupancy.hazardCategoryName;
      occupancy["gradedRetention"] = occupancy.gradedRetention === "Yes";

      await Occupancy({ skipTenant: true }).create({
        ...occupancy,
        hazardCategoryId: hazardCategory._id,
        industryTypeId: industryType._id,
        partnerId: selfPartner._id,
        productId: laghuProduct._id,
        tenantId: `Occupancy-${selfPartner._id}`
      });
    } else {
      console.log(`Attempting to create occupancy, but unable to resolve a document with the given occupancy references: ${JSON.stringify(occupancy)}`);
      process.exit(1);
    }
  }
}

async function importBscCovers() {
  for (let i = 0; i < bscCovers.length; i++) {
    const bscCover = bscCovers[i];
    const productName = bscCover.productName;

    const product = await ProductMaster().findOne({ type: productName });

    if (product) {
      delete bscCover.productName;
      await BscCover({ skipTenant: true }).create({
        ...bscCover,
        productId: product._id
      });
    } else {
      console.log(`Attempting to create BSC cover with product: ${productName}, but unable to resolve a document with the given product name.`);
      process.exit(1);
    }
  }
}

async function importBscDiscountCover() {
  for (let i = 0; i < bscDiscountCovers.length; i++) {
    const bscDiscountCover = bscDiscountCovers[i];
    const productName = bscDiscountCover.productName;

    const product = await ProductMaster().findOne({ type: productName });

    if (product) {
      delete bscDiscountCover.productName;
      await BscDiscountCover({ skipTenant: true }).create({
        ...bscDiscountCover,
        productId: product._id
      });
    } else {
      console.log(`Attempting to create BSC Discount cover with product: ${productName}, but unable to resolve a document with the given product name.`);
      process.exit(1);
    }
  }
}

const _ADDON_HEADER_MAPPING = {
  sr_no: "Sr. No.",
  name_of_cover: "Name of Cover/Add-on",
  category_of_importance: "Category of Importance",
  wordings: "Wordings",
  paid_free: "Paid/Free",
  rate: "Rate",
  metal_steel: "Metal/Steel",
  cement: "Cement",
  fetilizer_and_petrochemical: "Fertilizer & Petrochemical",
  plastic_mfg: "Plastic Mfg",
  chemicals: "Chemicals",
  transmission_and_distribution_lines: "Transmission & Distribution Lines",
  textiles: "Textiles",
  non_industrial: "Non-Industrial",
  banking: "Banking",
  engineering_workshop: "Engineering Workshop",
  pharma: "Pharma",
  food: "Food",
  power_coventional: "Power-Conventional",
  power_renewable: "Power-Renewable",
  it_ites_software_development: "IT/ITES-Software Development",
  storage: "Storage",
  hazardous_goods_manufacturing: "Hazardous Goods Manufacturing",
  rubber_goods_manufacturing: "Rubber Goods Manufacturing",
  infrastructure: "Infrastructure",
  paper: "Paper",
  silent: "Silent",
  others: "Others"
};

const _TERRORISM_HEADER_MAPPING = {
  industry_type: "Industry type",
  from_si: "From SI",
  to_si: "To SI",
  rate_per_mile: "Rate (Per Mile)",
  add_value: "to be add for earlier layers",
  remark: "Remark"
};

async function importLaghuTerrorismRates() {
  const selfPartner = await Partner().findOne({ name: _SELF_PARTNER });
  const laghuProduct = await ProductMaster().findOne({ type: _LAGHU_PRODUCT });

  const terrorismCsv = `${baseDir}/terrorism-rates.csv`;
  const terrorismRates = await csv().fromFile(terrorismCsv);
  for (let i = 0; i < terrorismRates.length; i++) {
    const terrorism = terrorismRates[i];

    const terrorismModel = TerrorismRateMaster({
      skipTenant: true
    });
    // Havent added the check  if it is already existing or not

    const terrorismRate = await terrorismModel.create({
      industryType: terrorism[_TERRORISM_HEADER_MAPPING.industry_type],
      partnerId: selfPartner._id,
      productId: laghuProduct._id,
      fromSI: terrorism[_TERRORISM_HEADER_MAPPING.from_si],
      toSI: terrorism[_TERRORISM_HEADER_MAPPING.to_si],
      ratePerMile: terrorism[_TERRORISM_HEADER_MAPPING.rate_per_mile],
      addValue: terrorism[_TERRORISM_HEADER_MAPPING.add_value],
      remark: terrorism[_TERRORISM_HEADER_MAPPING.remark]
    });
  }
}

/**
 * The below import will only work assuming that the Sector related columns in the imported CSV
 * are verbatim matching the sectors which have been imported in the SectorMaster table.
 */
async function importLaghuAddons() {
  const selfPartner = await Partner().findOne({ name: _SELF_PARTNER });
  const laghuProduct = await ProductMaster().findOne({ type: _LAGHU_PRODUCT });

  const addonsCsv = `${baseDir}/addon-cover-sector.csv`;
  const addons = await csv().fromFile(addonsCsv);
  for (let i = 0; i < addons.length; i++) {
    const addon = addons[i];

    // 1. Check the name_of_cover and see if already existing in the AddOnCover model.
    const addOnCoverModel = AddOnCover({
      skipTenant: false,
      specifiedPartnerId: selfPartner._id
    });
    let resolvedAddOnCover = await addOnCoverModel.findOne({
      name: addon[_ADDON_HEADER_MAPPING.name_of_cover]
    });

    // 2. If not there then we fire an insert in the Addons table.
    if (!resolvedAddOnCover) {
      resolvedAddOnCover = await addOnCoverModel.create({
        name: addon[_ADDON_HEADER_MAPPING.name_of_cover],
        partnerId: selfPartner._id,
        productId: laghuProduct._id,
        tenantId: `AddOnCover-${selfPartner._id}`,
        isFree: addon[_ADDON_HEADER_MAPPING.paid_free] === "Free" ? true : false,
        freeUpToFlag: addon[_ADDON_HEADER_MAPPING.paid_free],
        claimClause: addon[_ADDON_HEADER_MAPPING.wordings],
        siLimit: true,
        categoryOfImportance: addon[_ADDON_HEADER_MAPPING.category_of_importance]
      });
    }

    // 3. For each sector we try to see if this addon is applicable or not, if applicable make an insert in the AddOnCoverSector model.
    const addonCoverSectorModel = AddOnCoverSector({
      skipTenant: false,
      specifiedPartnerId: selfPartner._id
    });
    const sectors = await SectorMaster().find();
    for (let i = 0; i < sectors.length; i++) {
      const sector = sectors[i];

      const applicable = addon[sector.name];

      if (applicable && applicable === "Applicable") {
        const addonCoverSector = await addonCoverSectorModel.findOne({
          sectorId: sector._id,
          addOnCoverId: resolvedAddOnCover._id
        });

        if (!addonCoverSector) {
          await addonCoverSectorModel.create({
            addOnCoverId: resolvedAddOnCover._id,
            sectorId: sector._id
          });
        }
      }
    }
  }
}

async function importClientsGroup() {
  const brokerPartner = await Partner().findOne({ name: _BROKER_PARTNER });

  for (let i = 0; i < clientsGroups.length; i++) {
    const clientsGroup = clientsGroups[i];

    const cityName = clientsGroup.cityName;
    const pincodeName = clientsGroup.pincodeName;
    const city = await CityMaster().findOne({ name: cityName });
    const pincode = await PincodeMaster().findOne({ name: pincodeName });

    if (city && pincode) {
      delete clientsGroup.pincodeName;
      delete clientsGroup.cityName;

      await ClientGroupMaster({ skipTenant: true }).create({
        ...clientsGroup,
        cityId: city._id,
        pincodeId: pincode._id,
        partnerId: brokerPartner._id,
        tenantId: `ClientGroupMaster-${brokerPartner._id}`
      });
    } else {
      console.log(`Attempting to create clientsGroup, but unable to resolve a document with the given clientsGroup references: ${JSON.stringify(clientsGroup)}`);
      process.exit(1);
    }
  }
}

async function importClients() {
  const brokerPartner = await Partner().findOne({ name: _BROKER_PARTNER });

  for (let i = 0; i < clientsdata.length; i++) {
    const clients = clientsdata[i];

    const clientGroupName = clients.clientGroupName;
    const cityName = clients.cityName;
    const pincodeName = clients.pincodeName;
    const stateName = clients.stateName;

    const clientGroup = await ClientGroupMaster({ skipTenant: true }).findOne({
      clientGroupName: clientGroupName
    });
    const state = await StateMaster().findOne({ name: stateName });
    const city = await CityMaster().findOne({ name: cityName });
    const pincode = await PincodeMaster().findOne({ name: pincodeName });

    if (city && pincode) {
      delete clients.clientGroupName;
      delete clients.stateName;
      delete clients.cityName;
      delete clients.pincodeName;

      await Client({ skipTenant: true }).create({
        ...clients,
        clientGroupId: clientGroup._id,
        stateId: state._id,
        cityId: city._id,
        pincodeId: pincode._id,
        partnerId: brokerPartner._id,
        tenantId: `Client-${brokerPartner._id}`
      });
    } else {
      console.log(`Attempting to create Client, but unable to resolve a document with the given Client references: ${JSON.stringify(clients)}`);
      process.exit(1);
    }
  }
}

async function importClientsLocation() {
  const brokerPartner = await Partner().findOne({ name: _BROKER_PARTNER });

  for (let i = 0; i < clientsLocationdata.length; i++) {
    const clientLocation = clientsLocationdata[i];

    const clientShortName = clientLocation.clientShortName;
    const cityName = clientLocation.cityName;
    const pincodeName = clientLocation.pincodeName;
    const stateName = clientLocation.stateName;

    const client = await Client({ skipTenant: true }).findOne({
      shortName: clientShortName
    });
    const state = await StateMaster().findOne({ name: stateName });
    const city = await CityMaster().findOne({ name: cityName });
    const pincode = await PincodeMaster().findOne({ name: pincodeName });

    if (city && pincode) {
      delete clientLocation.clientShortName;
      delete clientLocation.stateName;
      delete clientLocation.cityName;
      delete clientLocation.pincodeName;

      await ClientLocation({ skipTenant: true }).create({
        ...clientLocation,
        clientId: client._id,
        stateId: state._id,
        cityId: city._id,
        pincodeId: pincode._id,
        partnerId: brokerPartner._id,
        tenantId: `ClientLocation-${brokerPartner._id}`
      });
    } else {
      console.log(`Attempting to create Client Location, but unable to resolve a document with the given Client references: ${JSON.stringify(clientLocation)}`);
      process.exit(1);
    }
  }
}

// DELETE ALL DATA FROM DB
const deleteData = async () => {
  try {
    // Get rid of the countries.
    await CountryMaster().deleteMany();

    // Get rid of the states.
    await StateMaster().deleteMany();

    // Get rid of the cities.
    await CityMaster().deleteMany();

    // Get rid of the districts.
    await DistrictMaster().deleteMany();

    // Get rid of the pincodes.
    await PincodeMaster().deleteMany();

    // Get rid of partners
    await Partner().deleteMany();

    // Get rid of Products.
    await ProductMaster().deleteMany();

    // Get rid of the industry types
    await IndustryTypeMaster().deleteMany();

    // Get rid of occupancy.
    await Occupancy({ skipTenant: true }).deleteMany();

    // Get rid of occupancy.
    await HazardCategoryMaster().deleteMany();

    await User({ skipTenant: true }).deleteMany();

    // Get rid of Clientgroups.
    await ClientGroupMaster({ skipTenant: true }).deleteMany();

    // Get rid of Clients
    await Client({ skipTenant: true }).deleteMany();

    // Get rid of Clients Location
    await ClientLocation({ skipTenant: true }).deleteMany();

    // Get rid of the sectors
    await SectorMaster().deleteMany();

    // Get rid of the earthquake rates
    await EarthquakeRateMaster({ skipTenant: true }).deleteMany();

    // Get rid of addons.
    await AddOnCover({ skipTenant: true }).deleteMany();

    // Get rid of terrorism rates
    await TerrorismRateMaster({ skipTenant: true }).deleteMany();

    // Get rid of BSC Covers
    await BscCover({ skipTenant: true }).deleteMany();

    // Get rid of BSC Discount Cover
    await BscDiscountCover({ skipTenant: true }).deleteMany();

    console.log("Data successfully deleted!");
  } catch (err) {
    console.log(err);
  }
  process.exit();
};

// console.log(process.argv);

if (process.argv[2] === "--import") {
  importData();
} else if (process.argv[2] === "--delete") {
  deleteData();
}
