import { Request, Response, NextFunction } from "../overrides";
import { IProductMasterDocument, IProductMasterModel, ProductMaster } from "../models/productMasterModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllProducts = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IProductMasterDocument, IProductMasterModel>(ProductMaster());
  fn(req, res, next);
};

const getProduct = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IProductMasterDocument, IProductMasterModel>(ProductMaster());
  fn(req, res, next);
};

const createProduct = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IProductMasterDocument, IProductMasterModel>(ProductMaster());
  fn(req, res, next);
};

const updateProduct = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IProductMasterDocument, IProductMasterModel>(ProductMaster());
  fn(req, res, next);
};

const deleteProduct = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IProductMasterDocument, IProductMasterModel>(ProductMaster());
  fn(req, res, next);
};

const getAllProductsPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IProductMasterDocument, IProductMasterModel>(ProductMaster(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { type: { $regex: gs.value, $options: "i" } },
        { category: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteProducts = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IProductMasterDocument, IProductMasterModel>(ProductMaster());
  fn(req, res, next);
};
export default {
  getAllProducts,
  getProduct,
  createProduct,
  updateProduct,
  deleteProduct,
  getAllProductsPrime,
  batchDeleteProducts
};
