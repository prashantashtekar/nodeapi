import { NextFunction, Request, Response } from "express";
import { ISectorMasterDocument, ISectorMasterModel, SectorMaster } from "../models/sectorMasterModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllSectorMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<ISectorMasterDocument, ISectorMasterModel>(SectorMaster());
  fn(req, res, next);
};
const getSectorMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<ISectorMasterDocument, ISectorMasterModel>(SectorMaster(), {
    path: "stateId"
  });
  fn(req, res, next);
};
const createSectorMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<ISectorMasterDocument, ISectorMasterModel>(SectorMaster());
  fn(req, res, next);
};
const updateSectorMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<ISectorMasterDocument, ISectorMasterModel>(SectorMaster());
  fn(req, res, next);
};
const deleteSectorMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<ISectorMasterDocument, ISectorMasterModel>(SectorMaster());
  fn(req, res, next);
};
const getAllSectorMastersPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<ISectorMasterDocument, ISectorMasterModel>(SectorMaster(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { name: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};
const batchDeleteSectorMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<ISectorMasterDocument, ISectorMasterModel>(SectorMaster());
  fn(req, res, next);
};
export default {
  getAllSectorMasters,
  getSectorMaster,
  createSectorMaster,
  updateSectorMaster,
  deleteSectorMaster,
  getAllSectorMastersPrime,
  batchDeleteSectorMasters
};
