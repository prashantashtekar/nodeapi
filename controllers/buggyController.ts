import { IResponseDto } from "../interfaces";
import { Tour } from "../models/tourModel";
import { Request, Response, NextFunction } from "../overrides";
import { AppError } from "../utils/appError";
import { catchAsync } from "../utils/catchAsync";

const notFound = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const document = await Tour().findById("62d7fb7a04aaefa52289c755");

  if (!document) {
    return next(new AppError(`No document found with that ID`, 404));
  }

  const respDto: IResponseDto = {
    status: "success",
    data: {}
  };
  res.status(200).json(respDto);
});

const serverError = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const document = await Tour().findById("62d7fb7a04aaefa52289c755");

  const respDto: IResponseDto = {
    status: "success",
    data: {
      tourName: document.name
    }
  };
  res.status(200).json(respDto);
});

const badRequest = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const document = await Tour().create({});

  const respDto: IResponseDto = {
    status: "success",
    data: {}
  };
  res.status(200).json(respDto);
});

export default {
  notFound,
  serverError,
  badRequest
};
