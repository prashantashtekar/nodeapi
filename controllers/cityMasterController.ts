import { Request, Response, NextFunction } from "../overrides";
import { ICityMasterDocument, ICityMasterModel, CityMaster } from "../models/cityMasterModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllCityMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<ICityMasterDocument, ICityMasterModel>(CityMaster());
  fn(req, res, next);
};

const getCityMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<ICityMasterDocument, ICityMasterModel>(CityMaster(), { path: "stateId" });
  fn(req, res, next);
};

const createCityMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<ICityMasterDocument, ICityMasterModel>(CityMaster());
  fn(req, res, next);
};

const updateCityMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<ICityMasterDocument, ICityMasterModel>(CityMaster());
  fn(req, res, next);
};

const deleteCityMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<ICityMasterDocument, ICityMasterModel>(CityMaster());
  fn(req, res, next);
};

const getAllCityMastersPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<ICityMasterDocument, ICityMasterModel>(CityMaster(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { name: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteCityMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<ICityMasterDocument, ICityMasterModel>(CityMaster());
  fn(req, res, next);
};
export default {
  getAllCityMasters,
  getCityMaster,
  createCityMaster,
  updateCityMaster,
  deleteCityMaster,
  getAllCityMastersPrime,
  batchDeleteCityMasters
};
