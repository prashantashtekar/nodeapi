import { Request, Response, NextFunction } from "../overrides";
import { IClientLocationDocument, IClientLocationModel, ClientLocation } from "../models/clientLocationModel";
import handlerFactory from "./handlerFactory";
import factory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllClientLocations = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IClientLocationDocument, IClientLocationModel>(ClientLocation());
  fn(req, res, next);
};

const getClientLocation = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IClientLocationDocument, IClientLocationModel>(ClientLocation(), { path: "stateId" });
  fn(req, res, next);
};

const createClientLocation = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IClientLocationDocument, IClientLocationModel>(ClientLocation());
  fn(req, res, next);
};

const updateClientLocation = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IClientLocationDocument, IClientLocationModel>(ClientLocation());
  fn(req, res, next);
};

const deleteClientLocation = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IClientLocationDocument, IClientLocationModel>(ClientLocation());
  fn(req, res, next);
};

const getAllClientLocationsPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IClientLocationDocument, IClientLocationModel>(ClientLocation(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { address: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteClientLocations = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IClientLocationDocument, IClientLocationModel>(ClientLocation());
  fn(req, res, next);
};

const getUserDiffHistory = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.getDiffHistory<IClientLocationDocument, IClientLocationModel>("ClientLocation");
  fn(req, res, next);
};

export default {
  getAllClientLocations,
  getClientLocation,
  createClientLocation,
  updateClientLocation,
  deleteClientLocation,
  getAllClientLocationsPrime,
  batchDeleteClientLocations,
  getUserDiffHistory
};
