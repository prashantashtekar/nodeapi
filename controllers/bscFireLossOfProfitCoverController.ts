import { NextFunction, Request, Response } from "express";
import { IBscFireLossOfProfitCoverDocument, IBscFireLossOfProfitCoverModel, BscFireLossOfProfitCover } from "../models/bscFireLossOfProfitCoverModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllBscFireLossOfProfitCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IBscFireLossOfProfitCoverDocument, IBscFireLossOfProfitCoverModel>(BscFireLossOfProfitCover());
  fn(req, res, next);
};

const getBscFireLossOfProfitCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IBscFireLossOfProfitCoverDocument, IBscFireLossOfProfitCoverModel>(BscFireLossOfProfitCover());
  fn(req, res, next);
};

const createBscFireLossOfProfitCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IBscFireLossOfProfitCoverDocument, IBscFireLossOfProfitCoverModel>(BscFireLossOfProfitCover());
  fn(req, res, next);
};

const updateBscFireLossOfProfitCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IBscFireLossOfProfitCoverDocument, IBscFireLossOfProfitCoverModel>(BscFireLossOfProfitCover());
  fn(req, res, next);
};

const deleteBscFireLossOfProfitCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IBscFireLossOfProfitCoverDocument, IBscFireLossOfProfitCoverModel>(BscFireLossOfProfitCover());
  fn(req, res, next);
};

const getAllBscFireLossOfProfitCoverPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IBscFireLossOfProfitCoverDocument, IBscFireLossOfProfitCoverModel>(BscFireLossOfProfitCover(),(gs: IGlobalSearch) => {
    return {
      $or: [
        { grossProfit: { $regex: gs.value, $options: "i" } },
        { indmenityPeriod: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteBscFireLossOfProfitCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IBscFireLossOfProfitCoverDocument, IBscFireLossOfProfitCoverModel>(BscFireLossOfProfitCover());
  fn(req, res, next);
};

export default {
  getAllBscFireLossOfProfitCovers,
  getBscFireLossOfProfitCover,
  createBscFireLossOfProfitCover,
  updateBscFireLossOfProfitCover,
  deleteBscFireLossOfProfitCover,
  getAllBscFireLossOfProfitCoverPrime,
  batchDeleteBscFireLossOfProfitCover
};
