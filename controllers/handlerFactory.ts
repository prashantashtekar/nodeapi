import { Request, Response, NextFunction } from "express";
import { Document, Model } from "mongoose";
import { IResponseDto } from "../interfaces";
import diffHistory from "../utils/diffHistory";
import { AppError } from "../utils/appError";
import { catchAsync } from "../utils/catchAsync";
import { DefaultFilterHelper, GlobalSearchFilterHandler, IPrimeLazyLoadEvent, PrimeFilterHelper } from "../utils/filterHelper";

/**
 * D here represents the type of the document.
 *
 * @param Model model of type M.
 * @returns
 */
export const deleteOne = <D extends Document, M extends Model<D>>(Model: M) =>
  catchAsync(async (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id;

    const document = await Model.findByIdAndDelete(id);

    if (!document) {
      return next(new AppError(`No document found with that ID`, 404));
    }

    const respDto: IResponseDto = {
      status: "success",
      data: null
    };
    res.status(204).json(respDto);
  });

const updateOne = <D extends Document, M extends Model<D>>(Model: M) =>
  catchAsync(async (req: Request, res: Response, next: NextFunction) => {
    const id = req.params.id;

    const updatedDoc = await Model.findByIdAndUpdate(id, req.body, {
      new: true,
      runValidators: true
    });

    if (!updatedDoc) {
      return next(new AppError(`No document found with that ID`, 404));
    }

    const respDto: IResponseDto = {
      status: "success",
      data: {
        entity: updatedDoc
      }
    };

    res.status(201).json(respDto);
  });

const createOne = <D extends Document, M extends Model<D>>(Model: M) =>
  catchAsync(async (req: Request, res: Response, next: NextFunction) => {
    const newDoc = await Model.create(req.body);

    const respDto: IResponseDto = {
      status: "success",
      data: {
        entity: newDoc
      }
    };

    res.status(201).json(respDto);
  });

const getOne = <D extends Document, M extends Model<D>>(Model: M, populateOptions = null) =>
  catchAsync(async (req: Request, res: Response, next: NextFunction) => {
    let query = Model.findById(req.params.id);

    if (populateOptions) {
      query = query.populate(populateOptions);
    }

    const document = await query;

    if (!document) {
      return next(new AppError(`No document found with that ID`, 404));
    }

    const respDto: IResponseDto = {
      status: "success",
      data: {
        entity: document
      }
    };

    res.status(200).json(respDto);
  });

const getAll = <D extends Document, M extends Model<D>>(Model: M) =>
  catchAsync(async (req: Request, res: Response, next: NextFunction) => {
    // Build the query.
    const features = new DefaultFilterHelper<D, M>(req.query, Model).filter().populate().sort().limitFields();
    await features.paginate();

    // Execute the query
    // const documents = await features.query.explain();
    const documents = await features.query;

    const respDto: IResponseDto = {
      status: "success",
      results: documents.length,
      data: {
        entities: documents
      }
    };

    // Send the response
    res.status(200).json(respDto);
  });

const getAllPrime = <D extends Document, M extends Model<D>>(Model: M, gsFn: GlobalSearchFilterHandler = null) =>
  catchAsync(async (req: Request, res: Response, next: NextFunction) => {
    const primePayload: IPrimeLazyLoadEvent = req.body as IPrimeLazyLoadEvent;

    // console.log(JSON.stringify(primePayload));
    // const userModel = User();

    const features = new PrimeFilterHelper<D, M>(primePayload, Model).filter(gsFn).populate().sort();
    await features.paginate();

    const documents = await features.query;

    // return the response.
    const respDto: IResponseDto = {
      status: "success",
      results: features.totalRecords,
      data: {
        entities: documents
      }
    };

    res.status(200).json(respDto);
  });

const batchDelete = <D extends Document, M extends Model<D>>(Model: M) =>
  catchAsync(async (req: Request, res: Response, next: NextFunction) => {
    const idsb64 = req.query.ids as string;
    const ids = JSON.parse(Buffer.from(idsb64, "base64").toString("utf-8"));
    await Model.deleteMany({ _id: { $in: ids } });

    const respDto: IResponseDto = {
      status: "success"
    };

    res.status(204).json(respDto);
  });

const getDiffHistory = <D extends Document, M extends Model<D>>(modelName: string) =>
  catchAsync(async (req: Request, res: Response, next: NextFunction) => {
    const primePayload: IPrimeLazyLoadEvent = req.body as IPrimeLazyLoadEvent;
    // console.log(JSON.stringify(primePayload));

    const limit = primePayload.rows || 20;
    const skip = primePayload.first;

    // @ts-ignore
    const diffHistories = await diffHistory.getDiffs(modelName, req.params.id, { limit, skip, sort: '-createdAt' });
    const diffHistoriesCount = await diffHistory.getDiffsCount(modelName, req.params.id);

    const respDto: IResponseDto = {
      status: "success",
      results: diffHistoriesCount,
      data: {
        entities: diffHistories
      }
    };

    res.status(200).json(respDto);
  });

export default {
  getOne,
  getAll,
  getAllPrime,
  getDiffHistory,
  deleteOne,
  updateOne,
  createOne,
  batchDelete
};
