import { NextFunction, Request, Response } from "express";
import { IBscLiabilitySectionCoverDocument, IBscLiabilitySectionCoverModel, BscLiabilitySectionCover } from "../models/bscLiabilitySectionCoverModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllBscLiabilitySectionCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IBscLiabilitySectionCoverDocument, IBscLiabilitySectionCoverModel>(BscLiabilitySectionCover());
  fn(req, res, next);
};

const getBscLiabilitySectionCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IBscLiabilitySectionCoverDocument, IBscLiabilitySectionCoverModel>(BscLiabilitySectionCover());
  fn(req, res, next);
};

const createBscLiabilitySectionCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IBscLiabilitySectionCoverDocument, IBscLiabilitySectionCoverModel>(BscLiabilitySectionCover());
  fn(req, res, next);
};

const updateBscLiabilitySectionCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IBscLiabilitySectionCoverDocument, IBscLiabilitySectionCoverModel>(BscLiabilitySectionCover());
  fn(req, res, next);
};

const deleteBscLiabilitySectionCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IBscLiabilitySectionCoverDocument, IBscLiabilitySectionCoverModel>(BscLiabilitySectionCover());
  fn(req, res, next);
};

const getAllBscLiabilitySectionCoversPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IBscLiabilitySectionCoverDocument, IBscLiabilitySectionCoverModel>(BscLiabilitySectionCover(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { riskType: { $regex: gs.value, $options: "i" } },
        { riskDescription: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteBscLiabilitySectionCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IBscLiabilitySectionCoverDocument, IBscLiabilitySectionCoverModel>(BscLiabilitySectionCover());
  fn(req, res, next);
};
export default {
  getAllBscLiabilitySectionCovers,
  getBscLiabilitySectionCover,
  createBscLiabilitySectionCover,
  updateBscLiabilitySectionCover,
  deleteBscLiabilitySectionCover,
  getAllBscLiabilitySectionCoversPrime,
  batchDeleteBscLiabilitySectionCovers
};
