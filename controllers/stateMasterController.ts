import { NextFunction, Request, Response } from "express";
import { IStateMasterDocument, IStateMasterModel, StateMaster } from "../models/stateMasterModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllStateMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IStateMasterDocument, IStateMasterModel>(StateMaster());
  fn(req, res, next);
};

const getStateMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IStateMasterDocument, IStateMasterModel>(StateMaster(), { path: "countryId" });
  fn(req, res, next);
};

const createStateMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IStateMasterDocument, IStateMasterModel>(StateMaster());
  fn(req, res, next);
};

const updateStateMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IStateMasterDocument, IStateMasterModel>(StateMaster());
  fn(req, res, next);
};

const deleteStateMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IStateMasterDocument, IStateMasterModel>(StateMaster());
  fn(req, res, next);
};

const getAllStateMastersPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IStateMasterDocument, IStateMasterModel>(StateMaster(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { name: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteStateMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IStateMasterDocument, IStateMasterModel>(StateMaster());
  fn(req, res, next);
};

export default {
  getAllStateMasters,
  getStateMaster,
  createStateMaster,
  updateStateMaster,
  deleteStateMaster,
  getAllStateMastersPrime,
  batchDeleteStateMasters
};
