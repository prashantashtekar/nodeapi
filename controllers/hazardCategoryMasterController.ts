import { Request, Response, NextFunction } from "../overrides";
import { IHazardCategoryMasterDocument, IHazardCategoryMasterModel, HazardCategoryMaster } from "../models/hazardCategoryMasterModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllHazardCategoryMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IHazardCategoryMasterDocument, IHazardCategoryMasterModel>(HazardCategoryMaster());
  fn(req, res, next);
};

const getHazardCategoryMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IHazardCategoryMasterDocument, IHazardCategoryMasterModel>(HazardCategoryMaster());
  fn(req, res, next);
};

const createHazardCategoryMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IHazardCategoryMasterDocument, IHazardCategoryMasterModel>(HazardCategoryMaster());
  fn(req, res, next);
};

const updateHazardCategoryMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IHazardCategoryMasterDocument, IHazardCategoryMasterModel>(HazardCategoryMaster());
  fn(req, res, next);
};

const deleteHazardCategoryMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IHazardCategoryMasterDocument, IHazardCategoryMasterModel>(HazardCategoryMaster());
  fn(req, res, next);
};

const getAllHazardCategoryMastersPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IHazardCategoryMasterDocument, IHazardCategoryMasterModel>(HazardCategoryMaster(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { category: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteHazardCategoryMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IHazardCategoryMasterDocument, IHazardCategoryMasterModel>(HazardCategoryMaster());
  fn(req, res, next);
};
export default {
  getAllHazardCategoryMasters,
  getHazardCategoryMaster,
  createHazardCategoryMaster,
  updateHazardCategoryMaster,
  deleteHazardCategoryMaster,
  getAllHazardCategoryMastersPrime,
  batchDeleteHazardCategoryMasters
};
