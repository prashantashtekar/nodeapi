import { NextFunction, Request, Response } from "express";
import { IBscPortableEquipmentsCoverDocument, IBscPortableEquipmentsCoverModel, BscPortableEquipmentsCover } from "../models/bscPortableEquipmentsCoverModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllBscPortableEquipmentsCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IBscPortableEquipmentsCoverDocument, IBscPortableEquipmentsCoverModel>(BscPortableEquipmentsCover());
  fn(req, res, next);
};

const getBscPortableEquipmentsCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IBscPortableEquipmentsCoverDocument, IBscPortableEquipmentsCoverModel>(BscPortableEquipmentsCover());
  fn(req, res, next);
};

const createBscPortableEquipmentsCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IBscPortableEquipmentsCoverDocument, IBscPortableEquipmentsCoverModel>(BscPortableEquipmentsCover());
  fn(req, res, next);
};

const updateBscPortableEquipmentsCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IBscPortableEquipmentsCoverDocument, IBscPortableEquipmentsCoverModel>(BscPortableEquipmentsCover());
  fn(req, res, next);
};

const deleteBscPortableEquipmentsCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IBscPortableEquipmentsCoverDocument, IBscPortableEquipmentsCoverModel>(BscPortableEquipmentsCover());
  fn(req, res, next);
};

const getAllBscPortableEquipmentsCoversPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IBscPortableEquipmentsCoverDocument, IBscPortableEquipmentsCoverModel>(BscPortableEquipmentsCover(),(gs: IGlobalSearch) => {
    return {
      $or: [
        // { name: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteBscPortableEquipmentsCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IBscPortableEquipmentsCoverDocument, IBscPortableEquipmentsCoverModel>(BscPortableEquipmentsCover());
  fn(req, res, next);
};

export default {
  getAllBscPortableEquipmentsCovers,
  getBscPortableEquipmentsCover,
  createBscPortableEquipmentsCover,
  updateBscPortableEquipmentsCover,
  deleteBscPortableEquipmentsCover,
  getAllBscPortableEquipmentsCoversPrime,
  batchDeleteBscPortableEquipmentsCovers
};
