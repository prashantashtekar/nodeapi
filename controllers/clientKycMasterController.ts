import { Request, Response, NextFunction } from "../overrides";
import { IClientKycMasterDocument, IClientKycMasterModel, ClientKycMaster } from "../models/clientKycMasterModel";
import handlerFactory from "./handlerFactory";
import factory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllClientKycMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IClientKycMasterDocument, IClientKycMasterModel>(ClientKycMaster());
  fn(req, res, next);
};

const getClientKycMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IClientKycMasterDocument, IClientKycMasterModel>(ClientKycMaster());
  fn(req, res, next);
};

const createClientKycMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IClientKycMasterDocument, IClientKycMasterModel>(ClientKycMaster());
  fn(req, res, next);
};

const updateClientKycMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IClientKycMasterDocument, IClientKycMasterModel>(ClientKycMaster());
  fn(req, res, next);
};

const deleteClientKycMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IClientKycMasterDocument, IClientKycMasterModel>(ClientKycMaster());
  fn(req, res, next);
};

const getAllClientKycMastersPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IClientKycMasterDocument, IClientKycMasterModel>(ClientKycMaster(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { clientGroupName: { $regex: gs.value, $options: "i" } },
        { clientName: { $regex: gs.value, $options: "i" } },
        { pan: { $regex: gs.value, $options: "i" } },
        { gst: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteClientKycMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IClientKycMasterDocument, IClientKycMasterModel>(ClientKycMaster());
  fn(req, res, next);
};

const getUserDiffHistory = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.getDiffHistory<IClientKycMasterDocument, IClientKycMasterModel>("ClientKycMaster");
  fn(req, res, next);
};

export default {
  getAllClientKycMasters,
  getClientKycMaster,
  createClientKycMaster,
  updateClientKycMaster,
  deleteClientKycMaster,
  getAllClientKycMastersPrime,
  batchDeleteClientKycMasters,
  getUserDiffHistory
};
