import { IUserDocument, IUserModel, User } from "../models/userModel";
import { catchAsync } from "../utils/catchAsync";
import { Request, Response, NextFunction } from "../overrides";
import { AppError } from "../utils/appError";
import { IResponseDto } from "../interfaces";
import factory from "./handlerFactory";
import multer from "multer";
// import diffHistory from "mongoose-diff-history/diffHistory";
import diffHistory from "../utils/diffHistory";

import authController from "./authController";
import { IGlobalSearch } from "../utils/filterHelper";

const multerStorage = multer.diskStorage({
  destination: (req: Request, file, cb) => {
    cb(null, "public/uploads/img/users");
  },
  filename: (req: Request, file, cb) => {
    // user-87s9dfds8798-201232180.jpeg
    const extension = file.mimetype.split("/")[1];
    cb(null, `user-${req.user.id}-${Date.now()}.${extension}`);
  }
});

// The goal here is to check if the uploaded file is an image, if it is then we pass true, else we pass an error and false.
const multerFilter = (req: Request, file, cb) => {
  if (file.mimetype.startsWith("image")) {
    cb(null, true);
  } else {
    cb(new AppError("Not an image! Please upload only images.", 400), false);
  }
};

const upload = multer({
  storage: multerStorage,
  fileFilter: multerFilter
});

const uploadUserPhoto = upload.single("photo");

const cleanBodyForUpdate = (body, ...allowedFields): any => {
  const newBody = {};

  Object.keys(body).forEach(el => {
    if (allowedFields.includes(el)) {
      newBody[el] = body[el];
    }
  });

  return newBody;
};

const getAllUsers = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.getAll<IUserDocument, IUserModel>(User());
  fn(req, res, next);
};
const getUser = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.getOne<IUserDocument, IUserModel>(User(), {
    path: "partnerId"
  });
  fn(req, res, next);
};

const getAllUsersPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.getAllPrime<IUserDocument, IUserModel>(User(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { name: { $regex: gs.value, $options: "i" } },
        { email: { $regex: gs.value, $options: "i" } },
        { mobileNumber: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const getMe = (req: Request, res: Response, next: NextFunction) => {
  authController.createAndSendToken(req.user, 200, res);
};

const createUser = (req: Request, res: Response) => {
  res.status(400).json({
    status: "error",
    message: "This route is not yet defined! Please use signup instead."
  });
};

const updateMe = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  // 1. create an error if the user tries to update the password.
  if (req.body.password || req.body.passwordConfirm) {
    return next(new AppError("This route is not for password updates. Please use /update-my-password.", 400));
  }

  // 2. Updating the user document.

  // Demonstration of an error that come with the below approach.
  // if we do the below, we end up getting the validation error linked to
  // "message": "User validation failed: passwordConfirm: Provide confirm your password",
  // const user = await User.findById(req.user.id);
  // user.name  = 'Harish';
  // await user.save();

  // Here we have filtered out fields which are not allowed to be updated.
  const cleanBody = cleanBodyForUpdate(req.body, "name", "email", "configSidebarIsOpen", "configMenuType", "configColorScheme", "configRippleEffect");
  if (req.file) {
    cleanBody.photo = req.file.filename;
  }

  // Instead we can use findByIdAndUpdate
  const updatedUser = await User().findByIdAndUpdate(req.user.id, cleanBody, {
    new: true,
    runValidators: true
  });

  const respDto: IResponseDto = {
    status: "success",
    data: {
      entity: updatedUser
    }
  };

  res.status(200).json(respDto);
});

const updatePhoto = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  // Here we have filtered out fields which are not allowed to be updated.

  let updatedUser = null;
  if (req.file) {
    const cleanBody = {
      photo: ""
    };
    cleanBody.photo = req.file.filename;

    // Instead we can use findByIdAndUpdate
    updatedUser = await User().findByIdAndUpdate(req.params.id, cleanBody, {
      new: true,
      runValidators: true
    });
  }

  const respDto: IResponseDto = {
    status: "success",
    data: {
      entity: updatedUser
    }
  };

  res.status(200).json(respDto);
});

const updateUser = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.updateOne<IUserDocument, IUserModel>(User());
  fn(req, res, next);
};

const deleteMe = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  await User().findByIdAndUpdate(req.user.id, { active: false });

  const respDto: IResponseDto = {
    status: "success"
  };

  res.status(204).json(respDto);
});

const deleteUser = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.deleteOne<IUserDocument, IUserModel>(User());
  fn(req, res, next);
};

const batchDeleteUsers = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.batchDelete<IUserDocument, IUserModel>(User());
  fn(req, res, next);
};

const getUserDiffHistory = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.getDiffHistory<IUserDocument, IUserModel>("User");
  fn(req, res, next);
};

export default {
  createUser,
  getAllUsers,
  getUser,
  getAllUsersPrime,
  getMe,
  updateMe,
  updateUser,
  updatePhoto,
  deleteMe,
  deleteUser,
  uploadUserPhoto,
  batchDeleteUsers,
  getUserDiffHistory,
};
