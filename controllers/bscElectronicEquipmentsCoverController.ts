import { NextFunction, Request, Response } from "express";
import { IBscElectronicEquipmentsCoverDocument, IBscElectronicEquipmentsCoverModel, BscElectronicEquipmentsCover } from "../models/bscElectronicEquipmentsCoverModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllBscElectronicEquipmentsCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IBscElectronicEquipmentsCoverDocument, IBscElectronicEquipmentsCoverModel>(BscElectronicEquipmentsCover());
  fn(req, res, next);
};

const getBscElectronicEquipmentsCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IBscElectronicEquipmentsCoverDocument, IBscElectronicEquipmentsCoverModel>(BscElectronicEquipmentsCover());
  fn(req, res, next);
};

const createBscElectronicEquipmentsCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IBscElectronicEquipmentsCoverDocument, IBscElectronicEquipmentsCoverModel>(BscElectronicEquipmentsCover());
  fn(req, res, next);
};

const updateBscElectronicEquipmentsCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IBscElectronicEquipmentsCoverDocument, IBscElectronicEquipmentsCoverModel>(BscElectronicEquipmentsCover());
  fn(req, res, next);
};
const deleteBscElectronicEquipmentsCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IBscElectronicEquipmentsCoverDocument, IBscElectronicEquipmentsCoverModel>(BscElectronicEquipmentsCover());
  fn(req, res, next);
};

const getAllBscElectronicEquipmentsCoverPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IBscElectronicEquipmentsCoverDocument, IBscElectronicEquipmentsCoverModel>(BscElectronicEquipmentsCover(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { descriptionEquipments: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteBscElectronicEquipmentsCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IBscElectronicEquipmentsCoverDocument, IBscElectronicEquipmentsCoverModel>(BscElectronicEquipmentsCover());
  fn(req, res, next);
};
export default {
  getAllBscElectronicEquipmentsCovers,
  getBscElectronicEquipmentsCover,
  createBscElectronicEquipmentsCover,
  updateBscElectronicEquipmentsCover,
  deleteBscElectronicEquipmentsCover,
  getAllBscElectronicEquipmentsCoverPrime,
  batchDeleteBscElectronicEquipmentsCover
};
