import { NextFunction, Request, Response } from "express";
import { IQuoteLocationOccupancyDocument, IQuoteLocationOccupancyModel, QuoteLocationOccupancy } from "../models/quoteLocationOccupancyModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllQuoteLocationOccupancy = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IQuoteLocationOccupancyDocument, IQuoteLocationOccupancyModel>(QuoteLocationOccupancy());
  fn(req, res, next);
};

const getQuoteLocationOccupancy = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IQuoteLocationOccupancyDocument, IQuoteLocationOccupancyModel>(QuoteLocationOccupancy(), { path: "countryId stateId cityId districtId" });
  fn(req, res, next);
};

const createQuoteLocationOccupancy = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IQuoteLocationOccupancyDocument, IQuoteLocationOccupancyModel>(QuoteLocationOccupancy());
  fn(req, res, next);
};

const updateQuoteLocationOccupancy = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IQuoteLocationOccupancyDocument, IQuoteLocationOccupancyModel>(QuoteLocationOccupancy());
  fn(req, res, next);
};

const deleteQuoteLocationOccupancy = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IQuoteLocationOccupancyDocument, IQuoteLocationOccupancyModel>(QuoteLocationOccupancy());
  fn(req, res, next);
};

const getAllQuoteLocationOccupanciesPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IQuoteLocationOccupancyDocument, IQuoteLocationOccupancyModel>(QuoteLocationOccupancy(), (gs: IGlobalSearch) => {
    return {
      $or: [
        // { name: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteQuoteLocationOccupancies = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IQuoteLocationOccupancyDocument, IQuoteLocationOccupancyModel>(QuoteLocationOccupancy());
  fn(req, res, next);
};

export default {
  getAllQuoteLocationOccupancy,
  getQuoteLocationOccupancy,
  createQuoteLocationOccupancy,
  updateQuoteLocationOccupancy,
  deleteQuoteLocationOccupancy,
  getAllQuoteLocationOccupanciesPrime,
  batchDeleteQuoteLocationOccupancies
};
