import crypto from "crypto";
import { IUserDocument, User } from "./../models/userModel";
import { catchAsync } from "../utils/catchAsync";
import jwt from "jsonwebtoken";
import { AppError } from "../utils/appError";
import { Email } from "../utils/email";
import { promisify } from "util";
import { Request, Response, NextFunction } from "../overrides";
import { IAsyncStoreCtxt, IResponseDto, ITokenResponseDto } from "../interfaces";
import { CookieOptions } from "express";
import { requestAsyncLocalStorageCtxt } from "../models/common";

const signToken = id => {
  return jwt.sign({ id: id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN
  });
};

const createAndSendToken = (user: IUserDocument, statusCode: number, res: Response) => {
  const token = signToken(user._id);

  const expires = new Date(Date.now() + process.env.JWT_COOKIE_EXPIRES_IN_DAYS * 24 * 60 * 60 * 1000);

  // A cookie is a small piece of test that a server can send to the client.
  // once a client receives a cookie, it stores it and then sends it back in all future requests to the server.
  // All of the above is done by a browser client.
  const cookieOptions: CookieOptions = {
    expires: expires,
    // the cookie cannot be accessed or modified by the browser, preventing cross site scripting attack.
    httpOnly: true
  };
  // this option makes sure that cookie is only be sent on an encrypted connection.
  if (process.env.NODE_ENV === "production") {
    cookieOptions.secure = true;
  }
  res.cookie("jwt", token, cookieOptions);

  // make sure we are not sending the password back to the UI, this comes when we are
  // sending a JWT token on create of a new user.
  user.Password = undefined;

  const respDto: ITokenResponseDto = {
    status: "success",
    token: token,
    expires: expires.getTime(),
    data: {
      entity: user
    }
  };

  res.status(statusCode).json(respDto);
};

const signup = catchAsync(async (req: Request, res: Response, next) => {
  // This is a security fix.
  // const newUser = await User().create(req.body);

  // This only puts the data that we need.
  const newUserData = {
    name: req.body.name,
    mobileNumber: req.body.mobileNumber,
    email: req.body.email,
    password: req.body.password,
    passwordConfirm: req.body.passwordConfirm,
    passwordChangedAt: req.body.passwordChangedAt,
    role: req.body.role
    // tenantId: `User-${req.body.partnerId}`,
    // partnerId: `${req.body.partnerId}`,
  };

  if (req.body.partnerId) {
    newUserData["tenantId"] = `User-${req.body.partnerId}`;
    newUserData["partnerId"] = `${req.body.partnerId}`;
  }
  const newUser = await User({ skipTenant: true }).create(newUserData);

  // const url = `${req.protocol}://${req.get("host")}/me`;
  // console.log("Current url:", url);
  // await new Email(newUser, url).sendWelcome();

  createAndSendToken(newUser, 201, res);
});

const login = catchAsync(async (req: Request, res: Response, next) => {
  // const email = req.body.email;
  const { email, password } = req.body;
  console.log(req.body);
  // 1. Check if email and password exist.
  if (!email || !password) {
    return next(new AppError("Please provide email and password", 400));
  }

  // 2. Check if user exists and password is correct.
  // Please note how below we have had to explicitly ask to select the password field, as otherwise it is marked as "select: false" in the Mongo schema.
  const user = await User({ skipTenant: true }).findOne({ email: email }).select("+password");
  // console.log(user);

  if (!user || !(await user.correctPassword(password, user.Password))) {
    return next(new AppError("Incorrect email or password", 401));
  }

  // To compare the password, we need to encrypt the password received from the client and then compare that against the stored encrypted password.

  // 3. If everything ok, send token back to the client.
  createAndSendToken(user, 200, res);
});

const protect = catchAsync(async (req: Request, res: Response, next) => {
  // 1) Getting the token and check if its there.
    // let token, _;
    // if (req.headers.authorization && req.headers.authorization.startsWith("Bearer")) {
    //   [_, token] = req.headers.authorization.split(" ");
    // }

    // if (!token) {
    //   return next(new AppError("You are not logged in, please login to get access.", 401));
    // }

  // 2) Verifications of the token.
  // This function takes a callback, and we don't want to break the pattern which we have been using all this while
  // which is to use promises and then async/await. The promisify function from the util package allows us to convert a function into a function which returns a promise, which we can then use with the normal async/await functionality.
  // At this point we will have the decoded token, we will be able to get the id of the user from the decoded output.
  // To illustrate the decoding process is working fine, we will use jwt.io and change the id a bit,
  // which will in turn change the JWT, we will use this modified JWT and try to access the getTours method,
  // this time it should give an error something on the lines of "JsonWebTokenError"
   //const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);

  // 3) Check if user still exists
  // This takes care of the scenario of when a long term token was issued, however the associated user has been
  // deleted in the interim.
  // const freshUser = await User({ skipTenant: true }).findById(decoded.id);
  // if (!freshUser) {
  //   return next(new AppError("The user belonging to the token no longer exists", 401));
  // }

  // 4) Check if user changed password after the JWT was issued.
  // if (freshUser.changedPasswordAfter(decoded.iat)) {
  //   return next(new AppError("User recently changed password! Please login again..", 401));
  // }

  // Finally grant access to the protected route.
  //req.user = freshUser;

  // Add the current tenant to the continuous location storage.
  // setCurrentUser(req.user);
  // if (req.user.partnerId && req.user.role !== 'admin') {
  //   setCurrentTenantId(req.user.partnerId.toString());
  // }

  let tenantId = "-666";
  if (req.user.partnerId && req.user.roleName !== "SuperAdmin") {
    tenantId = req.user.partnerId.toString();
  }

  const storeData: IAsyncStoreCtxt = { tenantId: tenantId, currentUser: req.user };
  requestAsyncLocalStorageCtxt.run(storeData, () => {
    storeData.tenantId = tenantId;
    storeData.currentUser = req.user;

    // Imagine any chain of async operations here
    // They all should have access to the storeData now.
    next();
  });

  // next();
});

const restrictTo = (...roles: Array<string>) => {
  return (req: Request, res: Response, next: NextFunction) => {
    // roles is an array and available because of the closure.
    // for eg. ['admin', 'lead-guide'].

    // Here we need to get hold of the current users role.
    // This is available because the protect middleware always runs before the restrict middleware.
    if (!roles.includes(req.user?.roleName)) {
      return next(new AppError("You do not have permission to perform this action", 403));
    }

    next();
  };
};

const forgotPassword = catchAsync(async (req: Request, res: Response, next) => {
  // 1. Get user based on email specified.
  const user = await User().findOne({ Email: req.body.Email });
  if (!user) {
    return next(new AppError("There is no user with email address", 404));
  }

  // 2. generate the random reset token.
  const resetToken = user.createPasswordResetToken();

  // we have done this because we don't want to trigger the validations which are there on the schema.
  await user.save({ validateBeforeSave: false });

  // 3. send it back as an email.
  const resetUrl = `${req.protocol}://${req.get("host")}/api/v1/users/reset-password/${resetToken}`;

  try {
    await new Email(user, resetUrl).sendPasswordReset();

    const respDto: IResponseDto = {
      status: "success",
      message: "Token sent to email!"
    };

    res.status(200).json(respDto);
  } catch (err) {
    user.passwordResetToken = undefined;
    user.passwordResetExpires = undefined;
    await user.save({ validateBeforeSave: false });

    return next(new AppError("There was an error sending the email. Try again later.", 500));
  }
});

const resetPassword = catchAsync(async (req: Request, res: Response, next) => {
  // 1. Get user based on the token.
  // We encrypt the original token again so we can compare it.
  const hashedToken = crypto.createHash("sha256").update(req.params.token).digest("hex");
  const user = await User({ skipTenant: true }).findOne({
    passwordResetToken: hashedToken,
    passwordResetExpires: {
      $gt: new Date()
    }
  });

  // 2. If token has not expired and there is a user, then set the password.
  if (!user) {
    return next(new AppError("Token is invalid or has expired", 400));
  }

  // if the token is a valid one then we go ahead and update the user with the new password and get rid of the reset token, reset expires.
  user.Password = req.body.password;
  user.Password = req.body.passwordConfirm;
  user.passwordResetToken = undefined;
  user.passwordResetExpires = undefined;
  await user.save();

  // 3. Update the changedPasswordAt property for the current user.

  // 4. Log the user in, send JWT to the client.
  createAndSendToken(user, 200, res);
});

const updatePassword = catchAsync(async (req: Request, res: Response, next) => {
  // 1. Get the user from the collection.
  // We need to explicitly as the password as by default in the schema we have mentioned that it will not be projected.
  const user = await User().findById(req.user.id).select("+password");

  // 2. Check if the posted password is correct.
  // For this we will be using the instance method.
  if (!(await user.correctPassword(req.body.passwordCurrent, user.Password))) {
    return next(new AppError("Your current password is wrong", 401));
  }

  // 3. If the password is correct, then update the password.
  // Note: Below to fire the update we have not used User().findByIdAndUpdate() because the validation linked
  // to checking if the password and confirm password are same will not run if we use this method.
  user.Password = req.body.password;
  user.Password = req.body.passwordConfirm;
  await user.save();

  // 4. Log user in, send JWT
  createAndSendToken(user, 200, res);
});

export default {
  signup,
  login,
  protect,
  restrictTo,
  forgotPassword,
  resetPassword,
  updatePassword,
  createAndSendToken
};
