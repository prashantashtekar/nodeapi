import { ITourDocument, ITourModel, Tour } from "../models/tourModel";
import _ from "lodash";
import { catchAsync } from "../utils/catchAsync";
import { Request, Response, NextFunction } from "../overrides";
import { IResponseDto } from "../interfaces";
import factory from "./handlerFactory";

const aliasTopTours = (req: Request, res: Response, next: NextFunction) => {
  req.query.limit = 5;
  req.query.sort = "-ratingsAverage,price";
  req.query.fields = "name,price,ratingsAverage,summary,difficulty";
  next();
};

const getAllTours = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.getAll<ITourDocument, ITourModel>(Tour());
  fn(req, res, next);
};

const getTour = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.getOne<ITourDocument, ITourModel>(Tour(), {
    path: "reviews"
  });
  fn(req, res, next);
};

const createTour = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.createOne<ITourDocument, ITourModel>(Tour());
  fn(req, res, next);
};

const updateTour = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.updateOne<ITourDocument, ITourModel>(Tour());
  fn(req, res, next);
};

const deleteTour = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.deleteOne<ITourDocument, ITourModel>(Tour());
  fn(req, res, next);
};

const getTourStats = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const stats = await Tour().aggregate([
    {
      $match: { ratingsAverage: { $gte: 4.5 } }
    },
    {
      $group: {
        _id: { $toUpper: "$difficulty" },
        num: { $sum: 1 },
        numRatings: { $sum: "$ratingsQuantity" },
        avgRating: { $avg: "$ratingsAverage" },
        avgPrice: { $avg: "$price" },
        minPrice: { $min: "$price" },
        maxPrice: { $max: "$price" }
      }
    },
    {
      $sort: {
        // 1 for ascending, -1 for descending.
        avgPrice: 1
      }
    }
    // {
    //     $match: {
    //         _id: { $ne: 'EASY' }
    //     }
    // }
  ]);

  const respDto: IResponseDto = {
    status: "success",
    data: {
      stats
    }
  };
  res.status(201).json(respDto);
});

const getMonthlyPlan = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const year = Number(req.params.year);

  const plan = await Tour().aggregate([
    {
      $unwind: "$startDates"
    },
    {
      $match: {
        startDates: {
          $gte: new Date(`${year}-01-01`),
          $lte: new Date(`${year}-12-31`)
        }
      }
    },
    {
      $group: {
        _id: { $month: "$startDates" },
        numTourStarts: { $sum: 1 },
        tours: { $push: "$name" }
      }
    },
    {
      $addFields: {
        month: "$_id"
      }
    },
    {
      $project: {
        _id: 0
      }
    },
    {
      $sort: {
        numTourStarts: -1
      }
    },
    {
      $limit: 12
    }
  ]);

  const respDto: IResponseDto = {
    status: "success",
    data: {
      plan
    }
  };

  res.status(201).json(respDto);
});

export default {
  aliasTopTours,
  getAllTours,
  getTour,
  createTour,
  updateTour,
  deleteTour,
  getTourStats,
  getMonthlyPlan
};
