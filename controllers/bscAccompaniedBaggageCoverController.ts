import { NextFunction, Request, Response } from "express";
import { IBscAccompaniedBaggageCoverDocument, IBscAccompaniedBaggageCoverModel, BscAccompaniedBaggageCover } from "../models/bscAccompaniedBaggageCoverModel";
import { IGlobalSearch } from "../utils/filterHelper";
import handlerFactory from "./handlerFactory";

const getAllBscAccompaniedBaggageCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IBscAccompaniedBaggageCoverDocument, IBscAccompaniedBaggageCoverModel>(BscAccompaniedBaggageCover());
  fn(req, res, next);
};

const getBscAccompaniedBaggageCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IBscAccompaniedBaggageCoverDocument, IBscAccompaniedBaggageCoverModel>(BscAccompaniedBaggageCover());
  fn(req, res, next);
};

const createBscAccompaniedBaggageCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IBscAccompaniedBaggageCoverDocument, IBscAccompaniedBaggageCoverModel>(BscAccompaniedBaggageCover());
  fn(req, res, next);
};

const updateBscAccompaniedBaggageCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IBscAccompaniedBaggageCoverDocument, IBscAccompaniedBaggageCoverModel>(BscAccompaniedBaggageCover());
  fn(req, res, next);
};

const deleteBscAccompaniedBaggageCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IBscAccompaniedBaggageCoverDocument, IBscAccompaniedBaggageCoverModel>(BscAccompaniedBaggageCover());
  fn(req, res, next);
};

const getAllbscAccompaniedBaggageCoverPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IBscAccompaniedBaggageCoverDocument, IBscAccompaniedBaggageCoverModel>(BscAccompaniedBaggageCover(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { baggageType: { $regex: gs.value, $options: "i" } },
        { baggageDescription: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteBscAccompaniedBaggageCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IBscAccompaniedBaggageCoverDocument, IBscAccompaniedBaggageCoverModel>(BscAccompaniedBaggageCover());
  fn(req, res, next);
};
export default {
  getAllBscAccompaniedBaggageCovers,
  getBscAccompaniedBaggageCover,
  createBscAccompaniedBaggageCover,
  updateBscAccompaniedBaggageCover,
  deleteBscAccompaniedBaggageCover,
  getAllbscAccompaniedBaggageCoverPrime,
  batchDeleteBscAccompaniedBaggageCover
};
