import { NextFunction, Request, Response } from "express";
import { IBscFixedPlateGlassCoverDocument, IBscFixedPlateGlassCoverModel, BscFixedPlateGlassCover } from "../models/bscFixedPlateGlassCoverModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllBscFixedPlateGlassCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IBscFixedPlateGlassCoverDocument, IBscFixedPlateGlassCoverModel>(BscFixedPlateGlassCover());
  fn(req, res, next);
};

const getBscFixedPlateGlassCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IBscFixedPlateGlassCoverDocument, IBscFixedPlateGlassCoverModel>(BscFixedPlateGlassCover());
  fn(req, res, next);
};

const createBscFixedPlateGlassCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IBscFixedPlateGlassCoverDocument, IBscFixedPlateGlassCoverModel>(BscFixedPlateGlassCover());
  fn(req, res, next);
};

const updateBscFixedPlateGlassCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IBscFixedPlateGlassCoverDocument, IBscFixedPlateGlassCoverModel>(BscFixedPlateGlassCover());
  fn(req, res, next);
};

const deleteBscFixedPlateGlassCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IBscFixedPlateGlassCoverDocument, IBscFixedPlateGlassCoverModel>(BscFixedPlateGlassCover());
  fn(req, res, next);
};

const getAllBscFixedPlateGlassCoverPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IBscFixedPlateGlassCoverDocument, IBscFixedPlateGlassCoverModel>(BscFixedPlateGlassCover(),(gs: IGlobalSearch) => {
    return {
      $or: [
        { plateGlassType: { $regex: gs.value, $options: "i" } },
        { description: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteBscFixedPlateGlassCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IBscFixedPlateGlassCoverDocument, IBscFixedPlateGlassCoverModel>(BscFixedPlateGlassCover());
  fn(req, res, next);
};

export default {
  getAllBscFixedPlateGlassCovers,
  getBscFixedPlateGlassCover,
  createBscFixedPlateGlassCover,
  updateBscFixedPlateGlassCover,
  deleteBscFixedPlateGlassCover,
  getAllBscFixedPlateGlassCoverPrime,
  batchDeleteBscFixedPlateGlassCover
};
