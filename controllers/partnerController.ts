import { Request, Response, NextFunction } from "../overrides";
import { IPartnerDocument, IPartnerModel, Partner } from "../models/partnerModel";
import handlerFactory from "./handlerFactory";
import { IResponseDto } from "../interfaces";
import { catchAsync } from "../utils/catchAsync";
import { IGlobalSearch } from "../utils/filterHelper";
import factory from "./handlerFactory";


const getAllPartners = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IPartnerDocument, IPartnerModel>(Partner());
  fn(req, res, next);
};

const getAllPartnersPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IPartnerDocument, IPartnerModel>(Partner(), (globalSearch: IGlobalSearch) => {
    return {
      $or: [
        { name: { $regex: globalSearch.value, $options: "i" } },
        { shortName: { $regex: globalSearch.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const getPartner = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IPartnerDocument, IPartnerModel>(Partner(), {
    path: "cityId districtId stateId pincodeId countryId"
  });
  fn(req, res, next);
};

const createPartner = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IPartnerDocument, IPartnerModel>(Partner());
  fn(req, res, next);
};

const updatePartner = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IPartnerDocument, IPartnerModel>(Partner());
  fn(req, res, next);
};

const deletePartner = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IPartnerDocument, IPartnerModel>(Partner());
  fn(req, res, next);
};

const batchDeletePartners = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IPartnerDocument, IPartnerModel>(Partner());
  fn(req, res, next);
};

const deleteManyPartners = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const idsb64 = req.query.ids;
  const ids = JSON.parse(Buffer.from(idsb64, "base64").toString("utf-8"));
  await Partner().deleteMany({ _id: { $in: ids } });

  const respDto: IResponseDto = {
    status: "success"
  };

  res.status(204).json(respDto);
});

const getUserDiffHistory = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.getDiffHistory<IPartnerDocument, IPartnerModel>("Partner");
  fn(req, res, next);
};

export default {
  getAllPartners,
  getAllPartnersPrime,
  getPartner,
  createPartner,
  updatePartner,
  deletePartner,
  deleteManyPartners,
  batchDeletePartners,
  getUserDiffHistory
};
