import { NextFunction, Request, Response } from "express";
import { IBscCoverDocument, IBscCoverModel, BscCover } from "../models/bscCoverModel";
import { IGlobalSearch } from "../utils/filterHelper";
import handlerFactory from "./handlerFactory";

const getAllBscCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IBscCoverDocument, IBscCoverModel>(BscCover());
  fn(req, res, next);
};

const getBscCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IBscCoverDocument, IBscCoverModel>(BscCover());
  fn(req, res, next);
};

const createBscCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IBscCoverDocument, IBscCoverModel>(BscCover());
  fn(req, res, next);
};

const updateBscCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IBscCoverDocument, IBscCoverModel>(BscCover());
  fn(req, res, next);
};
const deleteBscCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IBscCoverDocument, IBscCoverModel>(BscCover());
  fn(req, res, next);
};

const getALlBscCoverPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IBscCoverDocument, IBscCoverModel>(BscCover(),(gs: IGlobalSearch) => {
    return {
      $or: [
        { bscType: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteBscCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IBscCoverDocument, IBscCoverModel>(BscCover());
  fn(req, res, next);
};
export default {
  getAllBscCovers,
  getBscCover,
  createBscCover,
  updateBscCover,
  deleteBscCover,
  getALlBscCoverPrime,
  batchDeleteBscCover
};
