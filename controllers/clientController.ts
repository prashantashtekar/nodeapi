import { IClientDocument, IClientModel, Client } from "../models/clientModel";
import handlerFactory from "./handlerFactory";
import { catchAsync } from "../utils/catchAsync";
import { Request, Response, NextFunction } from "../overrides";
import { IResponseDto } from "../interfaces";
import factory from "./handlerFactory";

import { IGlobalSearch } from "../utils/filterHelper";

const getAllClients = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IClientDocument, IClientModel>(Client());
  fn(req, res, next);
};
const getClient = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IClientDocument, IClientModel>(Client(), {
    path: "clientGroupId cityId stateId pincodeId countryId"
  });
  fn(req, res, next);
};
const createClient = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IClientDocument, IClientModel>(Client());
  fn(req, res, next);
};
const updateClient = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IClientDocument, IClientModel>(Client());
  fn(req, res, next);
};
const deleteClient = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IClientDocument, IClientModel>(Client());
  fn(req, res, next);
};

const getAllClientsPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IClientDocument, IClientModel>(Client(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { name: { $regex: gs.value, $options: "i" } },
        { shortName: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteClients = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IClientDocument, IClientModel>(Client());
  fn(req, res, next);
};

const getMatchingClients = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const search = req.query.search;

  const clients = await Client().findByContainsSearch(search);

  const respClientMatch: IResponseDto = {
    status: "success",
    data: {
      entities: clients
    }
  };

  res.status(201).json(respClientMatch);
});

const resolveClient = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const data = req.body;

  const resolveclient = await Client().resolveClient(data);

  const respClientMatch: IResponseDto = {
    status: "success",
    data: {
      resolveclient
    }
  };

  res.status(201).json(resolveClient);
});

const getUserDiffHistory = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.getDiffHistory<IClientDocument, IClientModel>("Client");
  fn(req, res, next);
};

export default {
  getAllClients,
  getClient,
  createClient,
  updateClient,
  deleteClient,
  getMatchingClients,
  resolveClient,
  getAllClientsPrime,
  batchDeleteClients,
  getUserDiffHistory
};
