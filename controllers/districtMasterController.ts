import { Request, Response, NextFunction } from "../overrides";
import { IDistrictMasterDocument, IDistrictMasterModel, DistrictMaster } from "../models/districtMasterModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllDistrictMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IDistrictMasterDocument, IDistrictMasterModel>(DistrictMaster());
  fn(req, res, next);
};

const getDistrictMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IDistrictMasterDocument, IDistrictMasterModel>(DistrictMaster(), { path: "stateId" });
  fn(req, res, next);
};

const createDistrictMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IDistrictMasterDocument, IDistrictMasterModel>(DistrictMaster());
  fn(req, res, next);
};

const updateDistrictMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IDistrictMasterDocument, IDistrictMasterModel>(DistrictMaster());
  fn(req, res, next);
};

const deleteDistrictMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IDistrictMasterDocument, IDistrictMasterModel>(DistrictMaster());
  fn(req, res, next);
};

const getAllDistrictMastersPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IDistrictMasterDocument, IDistrictMasterModel>(DistrictMaster(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { name: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteDistrictMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IDistrictMasterDocument, IDistrictMasterModel>(DistrictMaster());
  fn(req, res, next);
};

export default {
  getAllDistrictMasters,
  getDistrictMaster,
  createDistrictMaster,
  updateDistrictMaster,
  deleteDistrictMaster,
  getAllDistrictMastersPrime,
  batchDeleteDistrictMasters
};
