import { IReviewDocument, IReviewModel, Review } from "../models/reviewModel";
import _ from "lodash";
import { Request, Response, NextFunction } from "../overrides";
import factory from "./handlerFactory";

const setTourInQuery = (req: Request, res: Response, next: NextFunction) => {
  // if we get a param in the tourId we simply need to update the req.query.
  if (req.params.tourId) {
    req.query.tour = req.params.tourId;
  }

  next();
};

const getAllReviews = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.getAll<IReviewDocument, IReviewModel>(Review());
  fn(req, res, next);
};
const getReview = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.getOne<IReviewDocument, IReviewModel>(Review());
  fn(req, res, next);
};

const setTourUserIds = (req: Request, res: Response, next: NextFunction) => {
  // Allow nested routes.
  if (!req.body.tour) {
    req.body.tour = req.params.tourId;
  }
  if (!req.body.user) {
    req.body.user = req.user.id;
  }
  next();
};

const createReview = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.createOne<IReviewDocument, IReviewModel>(Review());
  fn(req, res, next);
};
const updateReview = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.updateOne<IReviewDocument, IReviewModel>(Review());
  fn(req, res, next);
};
const deleteReview = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.deleteOne<IReviewDocument, IReviewModel>(Review());
  fn(req, res, next);
};

export default {
  setTourInQuery,
  getAllReviews,
  getReview,
  setTourUserIds,
  createReview,
  updateReview,
  deleteReview
};
