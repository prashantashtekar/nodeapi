import { NextFunction, Request, Response } from "express";
import { ClientLocation } from "../models/clientLocationModel";
import { Occupancy } from "../models/occupancyModel";
import { IQuoteSlipDocument, IQuoteSlipModel, QuoteSlip } from "../models/quoteSlipModel";
import { AppError } from "../utils/appError";
import { catchAsync } from "../utils/catchAsync";
import handlerFactory from "./handlerFactory";
import xl from "excel4node";
import { Partner } from "../models/partnerModel";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllQuoteSlip = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IQuoteSlipDocument, IQuoteSlipModel>(QuoteSlip());
  fn(req, res, next);
};

const getQuoteSlip = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IQuoteSlipDocument, IQuoteSlipModel>(QuoteSlip());
  fn(req, res, next);
};

const createQuoteSlip = (req: Request, res: Response, next: NextFunction) => {
  const createQuoteSlip = handlerFactory.createOne<IQuoteSlipDocument, IQuoteSlipModel>(QuoteSlip());
  createQuoteSlip(req, res, next);
};

const updateQuoteSlip = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IQuoteSlipDocument, IQuoteSlipModel>(QuoteSlip());
  fn(req, res, next);
};

const deleteQuoteSlip = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IQuoteSlipDocument, IQuoteSlipModel>(QuoteSlip());
  fn(req, res, next);
};

const getAllQuoteSlipsPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IQuoteSlipDocument, IQuoteSlipModel>(QuoteSlip(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { quoteNo: { $regex: gs.value, $options: "i" } },
        { quoteType: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteQuoteSlips = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IQuoteSlipDocument, IQuoteSlipModel>(QuoteSlip());
  fn(req, res, next);
};

const downloadClientLocationBulkImportExcel = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  // 1. Given the quoteId, we do the following.
  const quoteId = req.params.quoteId;
  const quote = await QuoteSlip().findById(quoteId);
  if (!quote) {
    return next(new AppError(`Invalid quoteId ${quoteId}`, 400));
  }

  // 2. Use the quoteId to find the client & corresponding locations.
  const clientLocations = await ClientLocation()
    .find({
      clientId: quote.clientId
    })
    .populate({
      path: "pincodeId"
    });

  // 3. Load the occupancy master.
  const selfPartner = await Partner().fetchSelfPartner();
  const occupancies = await Occupancy({
    skipTenant: false,
    specifiedPartnerId: selfPartner._id
  }).find();

  // 4. Generate an excel with 2 columns. The first column will contain all the locations of this client, 1 row each.
  // the second column will be empty, but will contain a data validation allowing one to only choose from the list of occupancies.

  // Create a new instance of a Workbook class
  var wb = new xl.Workbook();

  // Add Worksheets to the workbook
  var wsData = wb.addWorksheet("Data");
  var wsOccupancies = wb.addWorksheet("Occupancies");

  // Populate the occupancies in the relevant sheet.
  for (let i = 0; i < occupancies.length; i++) {
    wsOccupancies.cell(i + 1, 1).string(occupancies[i].occupancyType);
  }

  // Populate the data in the relevant sheet.
  // Write the header.
  wsData.cell(1, 1).string("Locations");
  wsData.cell(1, 2).string("Occupancies");

  // Setup data validation
  wsData.addDataValidation({
    type: "list",
    allowBlank: false,
    prompt: "Choose from dropdown",
    error: "Invalid choice was chosen",
    showDropDown: true,
    sqref: `B2:B${clientLocations.length + 1}`,
    formulas: [`=Occupancies!$A$1:$A$${occupancies.length}`]
  });

  // Write the actual data of locations.
  for (let i = 0; i < clientLocations.length; i++) {
    const clientLocation = clientLocations[i];
    // const pincode = await PincodeMaster().findById(clientLocation.pincodeId);
    wsData
      .cell(i + 2, 1)
      // .string(`${clientLocation.locationName}, ${pincode.name}`);
      .string(`${clientLocation.locationName}, ${clientLocation.pincodeId["name"]}`);
  }

  // Save the excel to the file system.
  wb.write(`QuoteLocationOccupancyBulkUploadFormat_${quoteId}.xlsx`, res);

  // const respDto: IResponseDto = {
  //   status: 'success',
  //   data: {
  //     stats: 'Excel ready',
  //   },
  // };
  // res.status(201).json(respDto);
});

export default {
  getAllQuoteSlip,
  getQuoteSlip,
  createQuoteSlip,
  updateQuoteSlip,
  deleteQuoteSlip,
  downloadClientLocationBulkImportExcel,
  getAllQuoteSlipsPrime,
  batchDeleteQuoteSlips
};
