import { NextFunction, Request, Response } from "express";
import { IBscDiscountCoverDocument, IBscDiscountCoverModel, BscDiscountCover } from "../models/bscDiscountCoverModel";
import { IGlobalSearch } from "../utils/filterHelper";
import handlerFactory from "./handlerFactory";

const getAllBscDiscountCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IBscDiscountCoverDocument, IBscDiscountCoverModel>(BscDiscountCover());
  fn(req, res, next);
};

const getBscDiscountCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IBscDiscountCoverDocument, IBscDiscountCoverModel>(BscDiscountCover(), { path: "productId" });
  fn(req, res, next);
};

const createBscDiscountCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IBscDiscountCoverDocument, IBscDiscountCoverModel>(BscDiscountCover());
  fn(req, res, next);
};

const updateBscDiscountCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IBscDiscountCoverDocument, IBscDiscountCoverModel>(BscDiscountCover());
  fn(req, res, next);
};
const deleteBscDiscountCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IBscDiscountCoverDocument, IBscDiscountCoverModel>(BscDiscountCover());
  fn(req, res, next);
};

const getAllBscDiscountCoverPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IBscDiscountCoverDocument, IBscDiscountCoverModel>(BscDiscountCover(),(gs: IGlobalSearch) => {
    return {
      $or: [
        { bscType: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteBscDiscountCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IBscDiscountCoverDocument, IBscDiscountCoverModel>(BscDiscountCover());
  fn(req, res, next);
};

export default {
  getAllBscDiscountCovers,
  getBscDiscountCover,
  createBscDiscountCover,
  updateBscDiscountCover,
  deleteBscDiscountCover,
  getAllBscDiscountCoverPrime,
  batchDeleteBscDiscountCover
};
