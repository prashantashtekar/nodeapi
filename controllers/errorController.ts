import { isUndefined } from "lodash";
import { AppError } from "../utils/appError";
import { Request, Response, NextFunction } from "../overrides";
import { IResponseDto } from "../interfaces";

interface IMongooseError {
  errors?: Map<string, IMongooseError>;
  kind?: string;
  path?: string;
  value?: string;
  message?: string;
  stack?: string;
}

const handleCastErrorDB = (err): AppError => {
  const message = `Invalid ${err.path}: ${err.value}`;
  return new AppError(message, 400);
};

const handleDuplicateFieldDB = (err): AppError => {
  const value = err.errmsg.match(/(["'])(?:\\.|[^\\])*?\1/)[0];
  const message = `Duplicate field value: ${value}. Please use another value.`;
  return new AppError(message, 400);
};

const handleValidationErrorDB = (err: IMongooseError): AppError => {
  const errors = Object.values(err.errors).map((el: IMongooseError) => el.message);
  // const message = `Invalid input data. ${errors.join('. ')}`;
  const message = `Invalid input data.`;
  const appError = new AppError(message, 400);
  appError.errors = errors;
  return appError;
};

const handleJWTError = (err): AppError => new AppError("Invalid token. Please login again.", 401);

const handleJWTExpiredError = (err): AppError => new AppError("Your token has expired. Please login again.", 401);

const sendErrorDev = (err, res: Response) => {
  const respDto: IResponseDto = {
    status: err.status,
    message: err.message,
    stack: err.stack,
    error: err
  };
  res.status(err.statusCode).json(respDto);
};

const sendErrorProd = (err, res: Response) => {
  // Operational error, trusted.
  // if (err.isOperational) {
  //   const respDto: IResponseDto = {
  //     status: err.status,
  //     message: err.message,
  //     stack: err.stack,
  //     error: err
  //   };
  //   res.status(err.statusCode).json(respDto);
  // }
  // // Programming or unknown error, don't leak too much details.
  // else {
  //   // 1. Log the error.
  //   console.error(err);

  //   const respDto: IResponseDto = {
  //     status: "error",
  //     message: "Something went very wrong!"
  //   };

  //   // 2. Send generic message.
  //   res.status(500).json(respDto);
  // }

  const respDto: IResponseDto = {
    status: err.status,
    message: err.message,
    stack: err.stack,
    error: err
  };
  res.status(err.statusCode).json(respDto);

};

export const globalErrorHandler = (err, req: Request, res: Response, next: NextFunction) => {
  // This will print the stack when the error happened.
  console.log(err.stack);
  console.log(err.name);

  err.statusCode = err.statusCode || 500;
  err.status = err.status || "error";

  let error = isUndefined;

  if (err.name == "CastError") {
    error = handleCastErrorDB(err);
  } else if (err.code === 11000) {
    error = handleDuplicateFieldDB(err);
  } else if (err.name === "ValidationError") {
    error = handleValidationErrorDB(err);
  } else if (err.name === "JsonWebTokenError") {
    error = handleJWTError(err);
  } else if (err.name === "TokenExpiredError") {
    error = handleJWTExpiredError(err);
  } else {
    error = err;
  }

  if (process.env.NODE_ENV === "development") {
    sendErrorDev(error, res);
  } else if (process.env.NODE_ENV === "production") {
    sendErrorProd(error, res);
  }
};
