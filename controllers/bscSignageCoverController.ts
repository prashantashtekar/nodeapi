import { NextFunction, Request, Response } from "express";
import { IBscSignageCoverDocument, IBscSignageCoverModel, BscSignageCover } from "../models/bscSignageCoverModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllBscSignageCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IBscSignageCoverDocument, IBscSignageCoverModel>(BscSignageCover());
  fn(req, res, next);
};

const getBscSignageCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IBscSignageCoverDocument, IBscSignageCoverModel>(BscSignageCover());
  fn(req, res, next);
};

const createBscSignageCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IBscSignageCoverDocument, IBscSignageCoverModel>(BscSignageCover());
  fn(req, res, next);
};

const updateBscSignageCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IBscSignageCoverDocument, IBscSignageCoverModel>(BscSignageCover());
  fn(req, res, next);
};

const deleteBscSignageCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IBscSignageCoverDocument, IBscSignageCoverModel>(BscSignageCover());
  fn(req, res, next);
};

const getAllBscSignageCoversPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IBscSignageCoverDocument, IBscSignageCoverModel>(BscSignageCover(), (gs: IGlobalSearch) => {
    return {
      $or: [
        // { name: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteBscSignageCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IBscSignageCoverDocument, IBscSignageCoverModel>(BscSignageCover());
  fn(req, res, next);
};

export default {
  getAllBscSignageCovers,
  getBscSignageCover,
  createBscSignageCover,
  updateBscSignageCover,
  deleteBscSignageCover,
  getAllBscSignageCoversPrime,
  batchDeleteBscSignageCovers
};
