import { Request, Response, NextFunction } from "../overrides";
import { IAddOnCoverSectorDocument, IAddOnCoverSectorModel, AddOnCoverSector } from "../models/addonCoverSectorModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllAddOnCoverSectors = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IAddOnCoverSectorDocument, IAddOnCoverSectorModel>(AddOnCoverSector());
  fn(req, res, next);
};

const getAddOnCoverSector = (req: Request, res: Response, next: NextFunction) => {
  // const fn = handlerFactory.getOne<IAddOnCoverSectorDocument, IAddOnCoverSectorModel>(AddOnCoverSector(), { path: "stateId" }); // TODO: Populate data require specific options
  const fn = handlerFactory.getOne<IAddOnCoverSectorDocument, IAddOnCoverSectorModel>(AddOnCoverSector(), { path: "addOnCoverId sectorId" });
  fn(req, res, next);
};

const createAddOnCoverSector = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IAddOnCoverSectorDocument, IAddOnCoverSectorModel>(AddOnCoverSector());
  fn(req, res, next);
};

const updateAddOnCoverSector = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IAddOnCoverSectorDocument, IAddOnCoverSectorModel>(AddOnCoverSector());
  fn(req, res, next);
};

const deleteAddOnCoverSector = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IAddOnCoverSectorDocument, IAddOnCoverSectorModel>(AddOnCoverSector());
  fn(req, res, next);
};

const getAllAddOnCoverSectorsPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IAddOnCoverSectorDocument, IAddOnCoverSectorModel>(AddOnCoverSector(), (gs: IGlobalSearch) => {
    return {
      $or: [
        // { name: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteAddOnCoverSectors = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IAddOnCoverSectorDocument, IAddOnCoverSectorModel>(AddOnCoverSector());
  fn(req, res, next);
};
export default {
  getAllAddOnCoverSectors,
  getAddOnCoverSector,
  createAddOnCoverSector,
  updateAddOnCoverSector,
  deleteAddOnCoverSector,
  getAllAddOnCoverSectorsPrime,
  batchDeleteAddOnCoverSectors
};
