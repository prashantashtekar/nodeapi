import { NextFunction, Request, Response } from "express";
import { IBscFidelityGuaranteeCoverDocument, IBscFidelityGuaranteeCoverModel, BscFidelityGuaranteeCover } from "../models/bscFidelityGuaranteeCoverModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllBscFidelityGuaranteeCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IBscFidelityGuaranteeCoverDocument, IBscFidelityGuaranteeCoverModel>(BscFidelityGuaranteeCover());
  fn(req, res, next);
};

const getBscFidelityGuaranteeCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IBscFidelityGuaranteeCoverDocument, IBscFidelityGuaranteeCoverModel>(BscFidelityGuaranteeCover());
  fn(req, res, next);
};

const createBscFidelityGuaranteeCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IBscFidelityGuaranteeCoverDocument, IBscFidelityGuaranteeCoverModel>(BscFidelityGuaranteeCover());
  fn(req, res, next);
};

const updateBscFidelityGuaranteeCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IBscFidelityGuaranteeCoverDocument, IBscFidelityGuaranteeCoverModel>(BscFidelityGuaranteeCover());
  fn(req, res, next);
};

const deleteBscFidelityGuaranteeCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IBscFidelityGuaranteeCoverDocument, IBscFidelityGuaranteeCoverModel>(BscFidelityGuaranteeCover());
  fn(req, res, next);
};

const getAllBscFidelityGuaranteeCoverPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IBscFidelityGuaranteeCoverDocument, IBscFidelityGuaranteeCoverModel>(BscFidelityGuaranteeCover(),(gs: IGlobalSearch) => {
    return {
      $or: [
        { riskType: { $regex: gs.value, $options: "i" } },
        { riskDescription: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteBscFidelityGuaranteeCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IBscFidelityGuaranteeCoverDocument, IBscFidelityGuaranteeCoverModel>(BscFidelityGuaranteeCover());
  fn(req, res, next);
};

export default {
  getAllBscFidelityGuaranteeCovers,
  getBscFidelityGuaranteeCover,
  createBscFidelityGuaranteeCover,
  updateBscFidelityGuaranteeCover,
  deleteBscFidelityGuaranteeCover,
  getAllBscFidelityGuaranteeCoverPrime,
  batchDeleteBscFidelityGuaranteeCover
};
