import { Request, Response, NextFunction } from "../overrides";
import { ICountryMasterDocument, ICountryMasterModel, CountryMaster } from "../models/countryMasterModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllCountryMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<ICountryMasterDocument, ICountryMasterModel>(CountryMaster());
  fn(req, res, next);
};

const getCountryMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<ICountryMasterDocument, ICountryMasterModel>(CountryMaster());
  fn(req, res, next);
};

const createCountryMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<ICountryMasterDocument, ICountryMasterModel>(CountryMaster());
  fn(req, res, next);
};

const updateCountryMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<ICountryMasterDocument, ICountryMasterModel>(CountryMaster());
  fn(req, res, next);
};

const deleteCountryMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<ICountryMasterDocument, ICountryMasterModel>(CountryMaster());
  fn(req, res, next);
};

const getAllCountryMasterPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<ICountryMasterDocument, ICountryMasterModel>(CountryMaster(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { name: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteCountryMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<ICountryMasterDocument, ICountryMasterModel>(CountryMaster());
  fn(req, res, next);
};

export default {
  getAllCountryMasters,
  getCountryMaster,
  createCountryMaster,
  updateCountryMaster,
  deleteCountryMaster,
  getAllCountryMasterPrime,
  batchDeleteCountryMasters
};
