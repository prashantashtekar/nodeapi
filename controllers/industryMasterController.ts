import { Request, Response, NextFunction } from "../overrides";
import { IIndustryTypeMasterDocument, IIndustryTypeMasterModel, IndustryTypeMaster } from "../models/industryTypeModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllIndustryTypeMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IIndustryTypeMasterDocument, IIndustryTypeMasterModel>(IndustryTypeMaster());
  fn(req, res, next);
};

const getIndustryTypeMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IIndustryTypeMasterDocument, IIndustryTypeMasterModel>(IndustryTypeMaster());
  fn(req, res, next);
};

const createIndustryTypeMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IIndustryTypeMasterDocument, IIndustryTypeMasterModel>(IndustryTypeMaster());
  fn(req, res, next);
};

const updateIndustryTypeMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IIndustryTypeMasterDocument, IIndustryTypeMasterModel>(IndustryTypeMaster());
  fn(req, res, next);
};

const deleteIndustryTypeMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IIndustryTypeMasterDocument, IIndustryTypeMasterModel>(IndustryTypeMaster());
  fn(req, res, next);
};

const getAllIndustryTypeMastersPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IIndustryTypeMasterDocument, IIndustryTypeMasterModel>(IndustryTypeMaster(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { industryTypeName: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteIndustryTypeMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IIndustryTypeMasterDocument, IIndustryTypeMasterModel>(IndustryTypeMaster());
  fn(req, res, next);
};

export default {
  getAllIndustryTypeMasters,
  getIndustryTypeMaster,
  createIndustryTypeMaster,
  updateIndustryTypeMaster,
  deleteIndustryTypeMaster,
  getAllIndustryTypeMastersPrime,
  batchDeleteIndustryTypeMasters
};
