import { NextFunction, Request, Response } from "express";
import { ITerrorismRateMasterDocument, ITerrorismRateMasterModel, TerrorismRateMaster } from "../models/terrorismRateMasterModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllTerrorismRateMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<ITerrorismRateMasterDocument, ITerrorismRateMasterModel>(TerrorismRateMaster());
  fn(req, res, next);
};

const getTerrorismRateMaster = (req: Request, res: Response, next: NextFunction) => {
  // const fn = handlerFactory.getOne<ITerrorismRateMasterDocument, ITerrorismRateMasterModel>(TerrorismRateMaster()); TODO: Model reference
  const fn = handlerFactory.getOne<ITerrorismRateMasterDocument, ITerrorismRateMasterModel>(TerrorismRateMaster(),{ path: "productId industryTypeId" });
  fn(req, res, next);
};

const createTerrorismRateMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<ITerrorismRateMasterDocument, ITerrorismRateMasterModel>(TerrorismRateMaster());
  fn(req, res, next);
};

const updateTerrorismRateMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<ITerrorismRateMasterDocument, ITerrorismRateMasterModel>(TerrorismRateMaster());
  fn(req, res, next);
};

const deleteTerrorismRateMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<ITerrorismRateMasterDocument, ITerrorismRateMasterModel>(TerrorismRateMaster());
  fn(req, res, next);
};

const getAllTerrorismRateMastersPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<ITerrorismRateMasterDocument, ITerrorismRateMasterModel>(TerrorismRateMaster(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { industryType: { $regex: gs.value, $options: "i" } },
        { fromSI: { $regex: gs.value, $options: "i" } },
        { toSI: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteTerrorismRateMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<ITerrorismRateMasterDocument, ITerrorismRateMasterModel>(TerrorismRateMaster());
  fn(req, res, next);
};

export default {
  getAllTerrorismRateMaster,
  getTerrorismRateMaster,
  createTerrorismRateMaster,
  updateTerrorismRateMaster,
  deleteTerrorismRateMaster,
  getAllTerrorismRateMastersPrime,
  batchDeleteTerrorismRateMasters
};
