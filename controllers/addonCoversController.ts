import { Request, Response, NextFunction } from "../overrides";
import { IAddOnCoverDocument, IAddOnCoverModel, AddOnCover } from "../models/addonCoversModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllAddOnCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IAddOnCoverDocument, IAddOnCoverModel>(AddOnCover());
  fn(req, res, next);
};

const getAddOnCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IAddOnCoverDocument, IAddOnCoverModel>(AddOnCover(), { path: "productId" });
  fn(req, res, next);
};

const createAddOnCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IAddOnCoverDocument, IAddOnCoverModel>(AddOnCover());
  fn(req, res, next);
};

const updateAddOnCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IAddOnCoverDocument, IAddOnCoverModel>(AddOnCover());
  fn(req, res, next);
};

const deleteAddOnCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IAddOnCoverDocument, IAddOnCoverModel>(AddOnCover());
  fn(req, res, next);
};

const getAllAddOnCoversPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IAddOnCoverDocument, IAddOnCoverModel>(AddOnCover(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { name: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteAddOnCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IAddOnCoverDocument, IAddOnCoverModel>(AddOnCover());
  fn(req, res, next);
};
export default {
  getAllAddOnCovers,
  getAddOnCover,
  createAddOnCover,
  updateAddOnCover,
  deleteAddOnCover,
  getAllAddOnCoversPrime,
  batchDeleteAddOnCovers
};
