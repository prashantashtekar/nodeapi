import { NextFunction, Request, Response } from "express";
import { IBscMoneySafeTillCoverDocument, IBscMoneySafeTillCoverModel, BscMoneySafeTillCover } from "../models/bscMoneySafeTillCoverModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllBscMoneySafeTillCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IBscMoneySafeTillCoverDocument, IBscMoneySafeTillCoverModel>(BscMoneySafeTillCover());
  fn(req, res, next);
};

const getBscMoneySafeTillCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IBscMoneySafeTillCoverDocument, IBscMoneySafeTillCoverModel>(BscMoneySafeTillCover());
  fn(req, res, next);
};

const createBscMoneySafeTillCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IBscMoneySafeTillCoverDocument, IBscMoneySafeTillCoverModel>(BscMoneySafeTillCover());
  fn(req, res, next);
};

const updateBscMoneySafeTillCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IBscMoneySafeTillCoverDocument, IBscMoneySafeTillCoverModel>(BscMoneySafeTillCover());
  fn(req, res, next);
};

const deleteBscMoneySafeTillCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IBscMoneySafeTillCoverDocument, IBscMoneySafeTillCoverModel>(BscMoneySafeTillCover());
  fn(req, res, next);
};

const getAllBscMoneySafeTillCoversPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IBscMoneySafeTillCoverDocument, IBscMoneySafeTillCoverModel>(BscMoneySafeTillCover(),(gs: IGlobalSearch) => {
    return {
      $or: [
        // { name: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteBscMoneySafeTillCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IBscMoneySafeTillCoverDocument, IBscMoneySafeTillCoverModel>(BscMoneySafeTillCover());
  fn(req, res, next);
};
export default {
  getAllBscMoneySafeTillCovers,
  getBscMoneySafeTillCover,
  createBscMoneySafeTillCover,
  updateBscMoneySafeTillCover,
  deleteBscMoneySafeTillCover,
  getAllBscMoneySafeTillCoversPrime,
  batchDeleteBscMoneySafeTillCovers
};
