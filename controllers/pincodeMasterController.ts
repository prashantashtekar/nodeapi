import { Request, Response, NextFunction } from "../overrides";
import { IPincodeMasterDocument, IPincodeMasterModel, PincodeMaster } from "../models/pincodeMasterModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllPincodeMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IPincodeMasterDocument, IPincodeMasterModel>(PincodeMaster());
  fn(req, res, next);
};

const getPincodeMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IPincodeMasterDocument, IPincodeMasterModel>(PincodeMaster(), { path: "countryId stateId cityId districtId" });
  fn(req, res, next);
};

const createPincodeMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IPincodeMasterDocument, IPincodeMasterModel>(PincodeMaster());
  fn(req, res, next);
};

const updatePincodeMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IPincodeMasterDocument, IPincodeMasterModel>(PincodeMaster());
  fn(req, res, next);
};

const deletePincodeMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IPincodeMasterDocument, IPincodeMasterModel>(PincodeMaster());
  fn(req, res, next);
};

const getAllPincodeMastersPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IPincodeMasterDocument, IPincodeMasterModel>(PincodeMaster(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { name: { $regex: gs.value, $options: "i" } },
        { earthquakeZone: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeletePincodeMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IPincodeMasterDocument, IPincodeMasterModel>(PincodeMaster());
  fn(req, res, next);
};

export default {
  getAllPincodeMasters,
  getPincodeMaster,
  createPincodeMaster,
  updatePincodeMaster,
  deletePincodeMaster,
  getAllPincodeMastersPrime,
  batchDeletePincodeMasters
};
