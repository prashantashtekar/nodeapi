import { Request, Response, NextFunction } from "../overrides";
import { IClientGroupMasterDocument, IClientGroupMasterModel, ClientGroupMaster } from "../models/clientGroupMasterModel";
import handlerFactory from "./handlerFactory";
import { catchAsync } from "../utils/catchAsync";
import { IResponseDto } from "../interfaces";
import factory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllGroupMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IClientGroupMasterDocument, IClientGroupMasterModel>(ClientGroupMaster());
  fn(req, res, next);
};

const getGroupMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IClientGroupMasterDocument, IClientGroupMasterModel>(ClientGroupMaster(), { path: "cityId pincodeId" });
  fn(req, res, next);
};

const createGroupMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IClientGroupMasterDocument, IClientGroupMasterModel>(ClientGroupMaster());
  fn(req, res, next);
};

const updateGroupMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IClientGroupMasterDocument, IClientGroupMasterModel>(ClientGroupMaster());
  fn(req, res, next);
};

const deleteGroupMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IClientGroupMasterDocument, IClientGroupMasterModel>(ClientGroupMaster());
  fn(req, res, next);
};

const getAllGroupMastersPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IClientGroupMasterDocument, IClientGroupMasterModel>(ClientGroupMaster(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { clientGroupName: { $regex: gs.value, $options: "i" } },
        { clientGroupContactName: { $regex: gs.value, $options: "i" } },
        { email: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteGroupMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IClientGroupMasterDocument, IClientGroupMasterModel>(ClientGroupMaster());
  fn(req, res, next);
};

const getMatchingClientGroups = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const search = req.query.search;

  // const clientsgroup = await ClientGroupMaster().find({ clientGroupName: { $regex: '.*' + search + '.*' } }).limit(20);

  const clientsgroup = await ClientGroupMaster().findByContainsSearch(search);

  const respclientGroupMatch: IResponseDto = {
    status: "success",
    data: {
      entities: clientsgroup
    }
  };

  res.status(201).json(respclientGroupMatch);
});

const resolveClientGroup = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const data = req.body;

  const resolveclientsgroup = await ClientGroupMaster().resolveClientGroup(data);

  const respclientGroup: IResponseDto = {
    status: "success",
    data: {
      resolveclientsgroup
    }
  };

  res.status(201).json(respclientGroup);
});

const getUserDiffHistory = (req: Request, res: Response, next: NextFunction) => {
  const fn = factory.getDiffHistory<IClientGroupMasterDocument, IClientGroupMasterModel>("ClientGroupMaster");
  fn(req, res, next);
};

export default {
  getAllGroupMaster,
  getGroupMaster,
  createGroupMaster,
  updateGroupMaster,
  deleteGroupMaster,
  getMatchingClientGroups,
  resolveClientGroup,
  getAllGroupMastersPrime,
  batchDeleteGroupMasters,
  getUserDiffHistory
};
