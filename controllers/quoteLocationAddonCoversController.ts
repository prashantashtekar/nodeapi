import { NextFunction, Request, Response } from "express";

import { IQuoteLocationAddonCoversDocument, IQuoteLocationAddonCoversModel, QuoteLocationAddonCovers } from "../models/quoteLocationAddonCoversModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllQuoteLocationAddonCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IQuoteLocationAddonCoversDocument, IQuoteLocationAddonCoversModel>(QuoteLocationAddonCovers());
  fn(req, res, next);
};

const getQuoteLocationAddonCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IQuoteLocationAddonCoversDocument, IQuoteLocationAddonCoversModel>(QuoteLocationAddonCovers(), { path: "stateId" });
  fn(req, res, next);
};

const createQuoteLocationAddonCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IQuoteLocationAddonCoversDocument, IQuoteLocationAddonCoversModel>(QuoteLocationAddonCovers());
  fn(req, res, next);
};

const updateQuoteLocationAddonCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IQuoteLocationAddonCoversDocument, IQuoteLocationAddonCoversModel>(QuoteLocationAddonCovers());
  fn(req, res, next);
};

const deleteQuoteLocationAddonCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IQuoteLocationAddonCoversDocument, IQuoteLocationAddonCoversModel>(QuoteLocationAddonCovers());
  fn(req, res, next);
};

const getAllQuoteLocationAddonCoversPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IQuoteLocationAddonCoversDocument, IQuoteLocationAddonCoversModel>(QuoteLocationAddonCovers(), (gs: IGlobalSearch) => {
    return {
      $or: [
        // { name: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteQuoteLocationAddonCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IQuoteLocationAddonCoversDocument, IQuoteLocationAddonCoversModel>(QuoteLocationAddonCovers());
  fn(req, res, next);
};

export default {
  getAllQuoteLocationAddonCovers,
  getQuoteLocationAddonCover,
  createQuoteLocationAddonCover,
  updateQuoteLocationAddonCover,
  deleteQuoteLocationAddonCover,
  getAllQuoteLocationAddonCoversPrime,
  batchDeleteQuoteLocationAddonCovers
};
