import { NextFunction, Request, Response } from "express";
import { IBscBurglaryHousebreakingCoverDocument, IBscBurglaryHousebreakingCoverModel, BscBurglaryHousebreakingCover } from "../models/bscBurglaryHousebreakingCoverModel";
import { IGlobalSearch } from "../utils/filterHelper";
import handlerFactory from "./handlerFactory";

const getAllBscBurglaryHousebreakingCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IBscBurglaryHousebreakingCoverDocument, IBscBurglaryHousebreakingCoverModel>(BscBurglaryHousebreakingCover());
  fn(req, res, next);
};

const getBscBurglaryHousebreakingCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IBscBurglaryHousebreakingCoverDocument, IBscBurglaryHousebreakingCoverModel>(BscBurglaryHousebreakingCover());
  fn(req, res, next);
};

const createBscBurglaryHousebreakingCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IBscBurglaryHousebreakingCoverDocument, IBscBurglaryHousebreakingCoverModel>(BscBurglaryHousebreakingCover());
  fn(req, res, next);
};

const updateBscBurglaryHousebreakingCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IBscBurglaryHousebreakingCoverDocument, IBscBurglaryHousebreakingCoverModel>(BscBurglaryHousebreakingCover());
  fn(req, res, next);
};

const deleteBscBurglaryHousebreakingCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IBscBurglaryHousebreakingCoverDocument, IBscBurglaryHousebreakingCoverModel>(BscBurglaryHousebreakingCover());
  fn(req, res, next);
};

const getAllBscBurglaryHousebreakingCoverPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IBscBurglaryHousebreakingCoverDocument, IBscBurglaryHousebreakingCoverModel>(BscBurglaryHousebreakingCover(), (gs: IGlobalSearch) => {
    return {
      $or: [
        // { name: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteBscBurglaryHousebreakingCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IBscBurglaryHousebreakingCoverDocument, IBscBurglaryHousebreakingCoverModel>(BscBurglaryHousebreakingCover());
  fn(req, res, next);
};
export default {
  getAllBscBurglaryHousebreakingCovers,
  getBscBurglaryHousebreakingCover,
  createBscBurglaryHousebreakingCover,
  updateBscBurglaryHousebreakingCover,
  deleteBscBurglaryHousebreakingCover,
  getAllBscBurglaryHousebreakingCoverPrime,
  batchDeleteBscBurglaryHousebreakingCover
};
