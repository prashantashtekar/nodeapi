import { NextFunction, Request, Response } from "express";
import { IBscMoneyTransitCoverDocument, IBscMoneyTransitCoverModel, BscMoneyTransitCover } from "../models/bscMoneyTransitCoverModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllBscMoneyTransitCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IBscMoneyTransitCoverDocument, IBscMoneyTransitCoverModel>(BscMoneyTransitCover());
  fn(req, res, next);
};

const getBscMoneyTransitCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getOne<IBscMoneyTransitCoverDocument, IBscMoneyTransitCoverModel>(BscMoneyTransitCover());
  fn(req, res, next);
};

const createBscMoneyTransitCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IBscMoneyTransitCoverDocument, IBscMoneyTransitCoverModel>(BscMoneyTransitCover());
  fn(req, res, next);
};

const updateBscMoneyTransitCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IBscMoneyTransitCoverDocument, IBscMoneyTransitCoverModel>(BscMoneyTransitCover());
  fn(req, res, next);
};

const deleteBscMoneyTransitCover = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IBscMoneyTransitCoverDocument, IBscMoneyTransitCoverModel>(BscMoneyTransitCover());
  fn(req, res, next);
};

const getAllBscMoneyTransitCoversPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IBscMoneyTransitCoverDocument, IBscMoneyTransitCoverModel>(BscMoneyTransitCover(),(gs: IGlobalSearch) => {
    return {
      $or: [
        // { name: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteBscMoneyTransitCovers = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IBscMoneyTransitCoverDocument, IBscMoneyTransitCoverModel>(BscMoneyTransitCover());
  fn(req, res, next);
};
export default {
  getAllBscMoneyTransitCovers,
  getBscMoneyTransitCover,
  createBscMoneyTransitCover,
  updateBscMoneyTransitCover,
  deleteBscMoneyTransitCover,
  getAllBscMoneyTransitCoversPrime,
  batchDeleteBscMoneyTransitCovers
};
