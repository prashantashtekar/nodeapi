import { Request, Response, NextFunction } from "../overrides";

import { IResponseDto } from "../interfaces";
import { IOccupancyDocument, IOccupancyModel, Occupancy } from "../models/occupancyModel";
import { Partner } from "../models/partnerModel";
import { catchAsync } from "../utils/catchAsync";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllOccupencies = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IOccupancyDocument, IOccupancyModel>(Occupancy());
  fn(req, res, next);
};

const getOccupancy = (req: Request, res: Response, next: NextFunction) => {
  // const fn = handlerFactory.getOne<IOccupancyDocument, IOccupancyModel>(Occupancy(), { path: "stateId" }); // TODO: Model populate changed
  const fn = handlerFactory.getOne<IOccupancyDocument, IOccupancyModel>(Occupancy(), { path: "hazardCategoryId productId" });
  fn(req, res, next);
};

const createOccupancy = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IOccupancyDocument, IOccupancyModel>(Occupancy());
  fn(req, res, next);
};

const updateOccupancy = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IOccupancyDocument, IOccupancyModel>(Occupancy());
  fn(req, res, next);
};

const deleteOccupancy = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IOccupancyDocument, IOccupancyModel>(Occupancy());
  fn(req, res, next);
};

const getAllOccupanciesPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IOccupancyDocument, IOccupancyModel>(Occupancy(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { occupancyType: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteOccupancies = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IOccupancyDocument, IOccupancyModel>(Occupancy());
  fn(req, res, next);
};
const getMatchingOccupancies = catchAsync(async (req: Request, res: Response, next: NextFunction) => {
  const search = req.query.search as string;

  type IsMasterType = "yes" | "no";
  const isMaster: IsMasterType = (req.query.isMaster ?? "yes") as IsMasterType;

  let occupancies: IOccupancyDocument[];

  // From the UI if we get isMaster=yes, then we return the occupancies tagged to the partner=self.
  if (isMaster === "yes") {
    const selfPartner = await Partner().findOne({ name: "self" });
    occupancies = await Occupancy({ skipTenant: true })
      .find({
        partnerId: selfPartner._id,
        occupancyType: { $regex: ".*" + search + ".*" }
      })
      .limit(5);
  }
  // Else we return the occupancies tagged to the currently logged in user based on their role.
  else {
    occupancies = await Occupancy().findByContainsSearch(search, 5);
  }

  const respClientMatch: IResponseDto = {
    status: "success",
    data: {
      entities: occupancies
    }
  };

  res.status(201).json(respClientMatch);
});

export default {
  getAllOccupencies,
  getOccupancy,
  createOccupancy,
  updateOccupancy,
  deleteOccupancy,
  getMatchingOccupancies,
  getAllOccupanciesPrime,
  batchDeleteOccupancies
};
