import { Request, Response, NextFunction } from "../overrides";
import { IEarthquakeRateMasterDocument, IEarthquakeRateMasterModel, EarthquakeRateMaster } from "../models/earthquakeRateMasterModel";
import handlerFactory from "./handlerFactory";
import { IGlobalSearch } from "../utils/filterHelper";

const getAllEarthquakeRateMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAll<IEarthquakeRateMasterDocument, IEarthquakeRateMasterModel>(EarthquakeRateMaster());
  fn(req, res, next);
};

const getEarthquakeRateMaster = (req: Request, res: Response, next: NextFunction) => {
  // const fn = handlerFactory.getOne<IEarthquakeRateMasterDocument, IEarthquakeRateMasterModel>(EarthquakeRateMaster(), { path: "countryId" }); // TODO: Model relation populate 
  const fn = handlerFactory.getOne<IEarthquakeRateMasterDocument, IEarthquakeRateMasterModel>(EarthquakeRateMaster(), { path: "industryTypeId productId" });
  fn(req, res, next);
};

const createEarthquakeRateMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.createOne<IEarthquakeRateMasterDocument, IEarthquakeRateMasterModel>(EarthquakeRateMaster());
  fn(req, res, next);
};

const updateEarthquakeRateMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.updateOne<IEarthquakeRateMasterDocument, IEarthquakeRateMasterModel>(EarthquakeRateMaster());
  fn(req, res, next);
};

const deleteEarthquakeRateMaster = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.deleteOne<IEarthquakeRateMasterDocument, IEarthquakeRateMasterModel>(EarthquakeRateMaster());
  fn(req, res, next);
};

const getAllEarthquakeRateMastersPrime = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.getAllPrime<IEarthquakeRateMasterDocument, IEarthquakeRateMasterModel>(EarthquakeRateMaster(), (gs: IGlobalSearch) => {
    return {
      $or: [
        { zone: { $regex: gs.value, $options: "i" } },
        { rate: { $regex: gs.value, $options: "i" } },
      ]
    };
  });
  fn(req, res, next);
};

const batchDeleteEarthquakeRateMasters = (req: Request, res: Response, next: NextFunction) => {
  const fn = handlerFactory.batchDelete<IEarthquakeRateMasterDocument, IEarthquakeRateMasterModel>(EarthquakeRateMaster());
  fn(req, res, next);
};

export default {
  getAllEarthquakeRateMaster,
  getEarthquakeRateMaster,
  createEarthquakeRateMaster,
  updateEarthquakeRateMaster,
  deleteEarthquakeRateMaster,
  getAllEarthquakeRateMastersPrime,
  batchDeleteEarthquakeRateMasters
};
