import { Schema } from "mongoose";
import { IClient } from "../models/clientModel";

export interface IClientResolutionDto {
  clientGroupMasterId: Schema.Types.ObjectId;
  client: IClient;
}
