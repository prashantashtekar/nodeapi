import nodemailer from "nodemailer";
import pug from "pug";
import { htmlToText } from "html-to-text";

// The user object will contain details of the user to whom we wish to send this email.
// For example the below will send an email on new user creation.
// new Email(user, url).sendWelcome();

export interface IEmailUser {
  Email: String;
  name: String;
}

export class Email {
  to: String;
  firstName: String;
  url: String;
  from: String;

  constructor(user: IEmailUser, url: String) {
    this.to = user.Email;
    this.firstName = user.name;
    this.url = url;
    this.from = `Harish Patel <${process.env.EMAIL_FROM}>`;
  }

  newTransport() {
    // 1. Create a transport.
    // This is basically actual service which we will use.
    // In the below there are a few services which nodemailer already knows how to deal with, so we don't have to configure these manually. Gmail is one of them.

    // To use Gmail as the transport we need to enable the less secure option in the respective gmail account which is being used to send.
    // Generally for a production grade application no one uses gmail. so we will also be using sendgrid.
    // const transport = nodemailer.createTransport({
    //     service: 'Gmail',
    //     auth: {
    //         user: process.env.EMAIL_USERNAME,
    //         pass: process.env.EMAIL_PASSWORD,
    //     }
    // });

    if (process.env.NODE_ENV === "production") {
      // Sendgrid.
      return nodemailer.createTransport({
        service: "SendGrid",
        auth: {
          user: process.env.EMAIL_USERNAME,
          pass: process.env.EMAIL_PASSWORD
        }
      });
    }

    return nodemailer.createTransport({
      host: process.env.EMAIL_HOST,
      port: process.env.EMAIL_PORT,
      auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD
      }
    });
  }

  /**
   * Send the actual email.
   *
   * @param template
   * @param subject
   */
  async send(template: String, subject: String) {
    // 1. Render the HTML based on a pug template.
    const emailBodyHtml = pug.renderFile(`${__dirname}/../views/emails/${template}.pug`, {
      firstName: this.firstName,
      url: this.url,
      subject: subject
    });

    // 2. Define the email options.
    const mailOptions = {
      from: this.from,
      to: this.to,
      subject: subject,
      html: emailBodyHtml,
      text: htmlToText(emailBodyHtml)
    };

    // 3. Create a transport and send the email.
    await this.newTransport().sendMail(mailOptions);
  }

  /**
   * Sends a welcome email. All it really does is call the internal send function
   * with the relevant template and subject.
   */
  async sendWelcome() {
    await this.send("welcome", "Welcome to InExchg!");
  }

  async sendPasswordReset() {
    await this.send("passwordReset", "Your password reset token (valid for only 10 minutes");
  }
}
