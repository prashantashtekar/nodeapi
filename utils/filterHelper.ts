import { Model } from "mongoose";
import { startOfDay, parseISO, endOfDay } from "date-fns";
import { Document, Query } from "mongoose";
import { AppError } from "./appError";

export interface IFilterHelper<D extends Document, M extends Model<D>> {
  filter(gsFn: GlobalSearchFilterHandler);
  sort();
  limitFields();
  populate();
  paginate(): any | Promise<IFilterHelper<D, M>>;
}

export interface IGlobalSearch {
  matchMode: string;
  value: string;
}

export type GlobalSearchFilterHandler = (globalSearch: IGlobalSearch) => any;

// - - - - - - - - - - -
// Default filter helper
// - - - - - - - - - - -
export interface IQueryString {
  sort: string;
  fields: string;
  page: number;
  limit: number;
  [x: string | number | symbol]: any;
}

export class DefaultFilterHelper<D extends Document, M extends Model<D>> implements IFilterHelper<D, M> {
  public query: Query<D[], D>;
  queryString: IQueryString;
  filtersObj;
  totalRecords: number;
  model: M;

  constructor(queryString, model: M, query = null) {
    this.model = model;
    // @ts-ignore
    this.query = query ? query : model.find();
    this.queryString = queryString;
  }

  populate() {
    if (this.queryString.populate) {
      const populate = this.queryString.populate.split(",").join(" ");

      this.query = this.query.populate(populate);
    }

    return this;
  }

  filter(gsFn: GlobalSearchFilterHandler = null) {
    const queryObj = { ...this.queryString };
    const excludedFields = ["page", "sort", "limit", "fields"];
    excludedFields.forEach(el => delete queryObj[el]);

    const queryString = JSON.stringify(queryObj).replace(/\b(gt|lt|gte|lte)\b/g, match => `$${match}`);
    this.filtersObj = JSON.parse(queryString);
    this.query = this.query.find(this.filtersObj);

    return this;
  }

  sort() {
    if (this.queryString.sort) {
      const sortBy = this.queryString.sort.split(",").join(" ");

      this.query = this.query.sort(sortBy);
    } else {
      this.query = this.query.sort("-createdAt");
    }

    return this;
  }

  limitFields() {
    if (this.queryString.fields) {
      const fields = this.queryString.fields.split(",").join(" ");

      this.query = this.query.select(fields);
    } else {
      this.query = this.query.select("-__v");
    }

    return this;
  }

  async paginate() {
    const page = this.queryString.page * 1 || 1;
    const limit = this.queryString.limit * 1 || 100;
    const skip = (page - 1) * limit;
    this.query = this.query.skip(skip).limit(limit);
    this.totalRecords = await this.model.find(this.filtersObj).countDocuments();

    return this;
  }
}

// - - - - - - - - - - -
// Prime filter helper
// - - - - - - - - - - -
// Interfaces used to handle the prime ng filter object.
export interface IPrimeSortMeta {
  field: string;
  order: number;
}

export interface IPrimeFilterMetadata {
  value?: any;
  matchMode?: string;
  operator?: string;
}

export interface IPrimeLazyLoadEvent {
  first?: number;
  rows?: number;
  sortField?: string;
  sortOrder?: number;
  multiSortMeta?: IPrimeSortMeta[];
  filters?: {
    [s: string]: IPrimeFilterMetadata[];
  };
  globalFilter?: any;
  populate?: string[];
}

// Sample 1.
// {
//   "first": 0,
//   "rows": 20,
//   "sortField": null,
//   "sortOrder": 1,
//   "filters": {
//     "_id": [{
//       "value": "62d50a0000865aac8482f02e",
//       "matchMode": "equals",
//       "operator": "and"
//     }],
//     "name": [{
//       "value": "harish",
//       "matchMode": "startsWith",
//       "operator": "and"
//     }, {
//       "value": "kashif",
//       "matchMode": "startsWith",
//       "operator": "and"
//     }],
//     "email": [{
//       "value": "harish",
//       "matchMode": "startsWith",
//       "operator": "or"
//     }, {
//       "value": "kashif",
//       "matchMode": "startsWith",
//       "operator": "or"
//     }],
//     "configMenuType": [{
//       "value": [{
//         "label": "static",
//         "value": "static"
//       }, {
//         "label": "overlay",
//         "value": "overlay"
//       }, {
//         "label": "slim",
//         "value": "slim"
//       }],
//       "matchMode": "in",
//       "operator": "and"
//     }],
//     "configColorScheme": [{
//       "value": [{
//         "label": "light",
//         "value": "light"
//       }],
//       "matchMode": "in",
//       "operator": "and"
//     }]
//   },
//   "globalFilter": null,
//   "multiSortMeta": null
// }

// String
//   startsWith
//   contains
//   notContains
//   endsWith
//   equals
//   notEquals
//   in

// Number
//   lt
//   lte
//   gt
//   gte
//   in
//   between

// Date
//   dateIs
//   dateIsNot
//   before
//   after

// Boolean
//   equals

export class PrimeFilterHelper<D extends Document, M extends Model<D>> implements IFilterHelper<D, M> {
  query: Query<D[], D>;
  primeLazyLoadEvent: IPrimeLazyLoadEvent;
  filtersObj;
  totalRecords: number;
  model: M;

  constructor(primeLazyLoadEvent: IPrimeLazyLoadEvent, model: M, query = null) {
    this.model = model;
    // @ts-ignore
    this.query = query ? query : model.find();
    this.primeLazyLoadEvent = primeLazyLoadEvent;
  }

  populate() {
    if (this.primeLazyLoadEvent.populate) {
      const populate = this.primeLazyLoadEvent.populate.join(" ");

      this.query = this.query.populate(populate);
    }

    return this;
  }

  limitFields() {
    throw new Error("Method not implemented.");
  }

  filter(gsFn: GlobalSearchFilterHandler = null) {
    const filters = this.primeLazyLoadEvent.filters;
    let filtersObj = {};
    for (const [field, filterValues] of Object.entries(filters)) {
      const fieldFilterObj = [];
      let operator = "$and";

      if (field === "global") {
        if (!gsFn) {
          throw new AppError('Global search not implemented on this entity', 400);
        }
        const globalFilter: IGlobalSearch = {
          matchMode: filterValues["matchMode"],
          value: filterValues["value"]
        };
        fieldFilterObj.push(gsFn(globalFilter));
      }
      // For all other values other than global we use the below to apply to the filter.
      else {
        for (const filterValue of filterValues) {
          // console.log(`Field ${field}, Value ${JSON.stringify(filterValue)}`);
          // console.log(`Field ${field} metadata: ${UserSchema.path(field)}`);

          operator = filterValue.operator === "or" ? "$or" : "$and";

          const matchMode = filterValue.matchMode;

          if (!filterValue.value) {
            continue;
          }

          switch (matchMode) {
            // UserSchema.path(field) instanceof mongoose.Schema.Types.String
            case "startsWith":
              fieldFilterObj.push({
                [field]: { $regex: `^${filterValue.value}.*`, $options: "i" }
              });
              break;
            // UserSchema.path(field) instanceof mongoose.Schema.Types.String
            case "contains":
              fieldFilterObj.push({
                [field]: { $regex: `${filterValue.value}`, $options: "i" }
              });
              break;
            // UserSchema.path(field) instanceof mongoose.Schema.Types.String
            case "notContains":
              fieldFilterObj.push({
                [field]: {
                  $not: { $regex: `${filterValue.value}`, $options: "i" }
                }
              });
              break;
            // UserSchema.path(field) instanceof mongoose.Schema.Types.String
            case "endsWith":
              fieldFilterObj.push({
                [field]: { $regex: `.*${filterValue.value}$`, $options: "i" }
              });
              break;
            // UserSchema.path(field) instanceof mongoose.Schema.Types.String
            // UserSchema.path(field) instanceof mongoose.Schema.Types.Boolean
            // UserSchema.path(field) instanceof mongoose.Schema.Types.ObjectId
            // UserSchema.path(field) instanceof mongoose.Schema.Types.Number
            case "equals":
              fieldFilterObj.push({ [field]: { $eq: filterValue.value } });
              break;
            // UserSchema.path(field) instanceof mongoose.Schema.Types.String
            // UserSchema.path(field) instanceof mongoose.Schema.Types.Boolean
            // UserSchema.path(field) instanceof mongoose.Schema.Types.ObjectId
            // UserSchema.path(field) instanceof mongoose.Schema.Types.Number
            case "notEquals":
              fieldFilterObj.push({ [field]: { $ne: filterValue.value } });
              break;
            // UserSchema.path(field) instanceof mongoose.Schema.Types.String
            // UserSchema.path(field) instanceof mongoose.Schema.Types.Number
            case "in":
              const actualValues = filterValue.value.map(v => v.value);
              if (actualValues && actualValues.length > 0) {
                fieldFilterObj.push({ [field]: { $in: actualValues } });
              }
              break;
            // UserSchema.path(field) instanceof mongoose.Schema.Types.Number
            case "lt":
              fieldFilterObj.push({ [field]: { $lt: filterValue.value } });
              break;
            // UserSchema.path(field) instanceof mongoose.Schema.Types.Number
            case "lte":
              fieldFilterObj.push({ [field]: { $lte: filterValue.value } });
              break;
            // UserSchema.path(field) instanceof mongoose.Schema.Types.Number
            case "gt":
              fieldFilterObj.push({ [field]: { $gt: filterValue.value } });
              break;
            // UserSchema.path(field) instanceof mongoose.Schema.Types.Number
            case "gte":
              fieldFilterObj.push({ [field]: { $gte: filterValue.value } });
              console.log("");
              break;
            // UserSchema.path(field) instanceof mongoose.Schema.Types.Number
            case "between":
              fieldFilterObj.push({ [field]: { $gte: filterValue.value[0] } });
              fieldFilterObj.push({ [field]: { $lte: filterValue.value[1] } });
              break;
            // UserSchema.path(field) instanceof mongoose.Schema.Types.Date
            case "dateIs":
              fieldFilterObj.push({
                [field]: {
                  $gte: startOfDay(parseISO(filterValue.value)),
                  $lte: endOfDay(parseISO(filterValue.value))
                }
              });
              break;
            // UserSchema.path(field) instanceof mongoose.Schema.Types.Date
            case "dateIsNot":
              fieldFilterObj.push({
                [field]: {
                  $not: {
                    $gte: startOfDay(parseISO(filterValue.value)),
                    $lte: endOfDay(parseISO(filterValue.value))
                  }
                }
              });
              break;
            // UserSchema.path(field) instanceof mongoose.Schema.Types.Date
            case "dateBefore":
              fieldFilterObj.push({
                [field]: { $lt: parseISO(filterValue.value) }
              });
              break;
            // UserSchema.path(field) instanceof mongoose.Schema.Types.Date
            case "dateAfter":
              fieldFilterObj.push({
                [field]: { $gt: parseISO(filterValue.value) }
              });
              break;
          }
        }
      }

      if (fieldFilterObj.length > 0) {
        if (operator in filtersObj) {
          filtersObj[operator] = [...filtersObj[operator], ...fieldFilterObj];
        } else {
          filtersObj[operator] = fieldFilterObj;
        }
      }
    }

    // Finally fire the query.
    // console.log(`Doing final filter using:`, JSON.stringify(filtersObj));
    this.filtersObj = filtersObj;
    this.query.find(filtersObj);

    return this;
  }

  sort() {
    if (this.primeLazyLoadEvent.sortField) {
      this.query = this.query.sort(`${this.primeLazyLoadEvent.sortOrder === -1 ? "-" : ""}${this.primeLazyLoadEvent.sortField}`);
    } else {
      this.query = this.query.sort("-createdAt");
    }

    return this;
  }

  async paginate() {
    const limit = this.primeLazyLoadEvent.rows || 20;
    const skip = this.primeLazyLoadEvent.first;
    this.query = this.query.skip(skip).limit(limit);
    this.totalRecords = await this.model.find(this.filtersObj).countDocuments();

    return this;
  }
}
