import express from "express";

import BscDiscountCoverController from "../controllers/bscDiscountCoverController";
import authController from "../controllers/authController";
import { _USER_ROLES } from "../models/userModel";
import bscDiscountCoverController from "../controllers/bscDiscountCoverController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/bsc-discount-cover:
 *   get:
 *     description: Fetches all the BSC Discount Covers.
 *     tags:
 *       - BSC Discount Cover
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: BSC Discount Covers.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscDiscountCover'
 */
router.route("/").get(BscDiscountCoverController.getAllBscDiscountCovers);
router.route("/prime").post(bscDiscountCoverController.getAllBscDiscountCoverPrime);

router.route("/batch-delete").delete(bscDiscountCoverController.batchDeleteBscDiscountCover);
/**
 * @swagger
 * /api/v1/bsc-discount-cover:
 *   post:
 *     description: Endpoint used to create a BSC Discount Cover.
 *     tags:
 *       - BSC Discount Cover
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               bscType:
 *                 type: string
 *               fromSI:
 *                 type: number
 *               toSI:
 *                 type: number
 *               ratePerMile:
 *                 type: number
 *               productId:
 *                 type: Schema.Types.ObjectId
 *               applicableFrom:
 *                 type: date
 *               applicableTo:
 *                 type: date
 *
 *     response:
 *       200:
 *         description: New BSC Discount Cover is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscDiscountCover'
 */
router.route("/").post(authController.restrictTo(_USER_ROLES.SuperAdmin), BscDiscountCoverController.createBscDiscountCover);

/**
 * @swagger
 * /api/v1/bsc-discount-cover/{id}:
 *   get:
 *     description: Fetches the BSC Discount Cover of specified ID.
 *     tags:
 *       - BSC Discount Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: BSC Discount Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscDiscountCover'
 */
router.route("/:id").get(BscDiscountCoverController.getBscDiscountCover);

/**
 * @swagger
 * /api/v1/bsc-discount-cover/{id}:
 *   patch:
 *     description: Updates the BSC Discount Cover of specified ID.
 *     tags:
 *       - BSC Discount Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               bscType:
 *                 type: string
 *               fromSI:
 *                 type: number
 *               toSI:
 *                 type: number
 *               ratePerMile:
 *                 type: number
 *               productId:
 *                 type: Schema.Types.ObjectId
 *               applicableFrom:
 *                 type: date
 *               applicableTo:
 *                 type: date
 *
 *     responses:
 *       200:
 *         description: BSC Discount Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscDiscountCover'
 */
router.route("/:id").patch(authController.restrictTo(_USER_ROLES.SuperAdmin), BscDiscountCoverController.updateBscDiscountCover);

/**
 * @swagger
 * /api/v1/bsc-discount-cover/{id}:
 *   delete:
 *     description: Deletes the BSC Discount Cover of specified ID.
 *     tags:
 *       - BSC Discount Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: BSC Discount Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscDiscountCover'
 */
router.route("/:id").delete(authController.restrictTo(_USER_ROLES.SuperAdmin), BscDiscountCoverController.deleteBscDiscountCover);
