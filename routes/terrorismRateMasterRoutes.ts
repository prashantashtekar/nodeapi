import express from "express";

import terrorismRateMasterController from "../controllers/terrorismRateMasterController";
import authController from "../controllers/authController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/terrorism-rate-masters:
 *   get:
 *     description: Fetches all the terrorism rate masters.
 *     tags:
 *       - Terrorism Rate Master
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Terrorism rate masters.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ITerrorismRateMaster'
 */
router.route("/").get(terrorismRateMasterController.getAllTerrorismRateMaster);
router.route("/prime").post(terrorismRateMasterController.getAllTerrorismRateMastersPrime);
router.route("/batch-delete").delete(terrorismRateMasterController.batchDeleteTerrorismRateMasters);
/**
 * @swagger
 * /api/v1/terrorism-rate-masters:
 *   post:
 *     description: Endpoint used to create a terrorism rate master.
 *     tags:
 *       - Terrorism Rate Master
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               industryType:
 *                 type: string
 *               fromSI:
 *                 type: number
 *               toSI:
 *                 type: number
 *               ratePerMile:
 *                 type: number
 *               addValue:
 *                 type: number
 *               remark:
 *                 type: string
 *               productId:
 *                 type: Schema.Types.ObjectId
 *               applicableFrom:
 *                 type: date
 *               applicableTo:
 *                 type: date
 *
 *     response:
 *       200:
 *         description: New terrorism rate master is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ITerrorismRateMaster'
 */
router.route("/").post(authController.restrictTo("admin"), terrorismRateMasterController.createTerrorismRateMaster);

/**
 * @swagger
 * /api/v1/terrorism-rate-masters/{id}:
 *   get:
 *     description: Fetches the terrorism rate master of specified ID.
 *     tags:
 *       - Terrorism Rate Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Terrorism rate master.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ITerrorismRateMaster'
 */
router.route("/:id").get(terrorismRateMasterController.getTerrorismRateMaster);

/**
 * @swagger
 * /api/v1/terrorism-rate-masters/{id}:
 *   patch:
 *     description: Updates the terrorism rate master of specified ID.
 *     tags:
 *       - Terrorism Rate Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               industryType:
 *                 type: string
 *               fromSI:
 *                 type: number
 *               toSI:
 *                 type: number
 *               ratePerMile:
 *                 type: number
 *               addValue:
 *                 type: number
 *               remark:
 *                 type: string
 *               productId:
 *                 type: Schema.Types.ObjectId
 *               applicableFrom:
 *                 type: date
 *               applicableTo:
 *                 type: date
 *
 *     responses:
 *       200:
 *         description: Terrorism rate master.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ITerrorismRateMaster'
 */
router.route("/:id").patch(authController.restrictTo("admin"), terrorismRateMasterController.updateTerrorismRateMaster);

/**
 * @swagger
 * /api/v1/terrorism-rate-masters/{id}:
 *   delete:
 *     description: Deletes the terrorism rate master of specified ID.
 *     tags:
 *       - Terrorism Rate Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: terrorism rate master.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ITerrorismRateMaster'
 */
router.route("/:id").delete(authController.restrictTo("admin"), terrorismRateMasterController.deleteTerrorismRateMaster);
