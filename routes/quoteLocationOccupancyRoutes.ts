import express from "express";

import quoteSlipController from "../controllers/quoteLocationOccupancyController";
import authController from "../controllers/authController";
import { _USER_ROLES } from "../models/userModel";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/quote-locations:
 *   get:
 *     description: Fetches all the quote locations occupancies.
 *     tags:
 *       - Quote Location Occupancy
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Quote Location Occupancies.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IQuoteLocationOccupancy'
 */
router.route("/").get(quoteSlipController.getAllQuoteLocationOccupancy);
router.route("/prime").post(quoteSlipController.getAllQuoteLocationOccupanciesPrime);
router.route("/batch-delete").delete(quoteSlipController.batchDeleteQuoteLocationOccupancies);
/**
 * @swagger
 * /api/v1/quote-locations:
 *   post:
 *     description: Endpoint used to create a quote locations occupancy.
 *     tags:
 *       - Quote Location Occupancy
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               clientLocationId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *
 *               occupancyId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *
 *               quoteId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *
 *               sumAssured:
 *                 type: number
 *                 required: true
 *
 *     response:
 *       200:
 *         description: New quote locations occupancy is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IQuoteLocationOccupancy'
 */
router.route("/").post(authController.restrictTo("admin", _USER_ROLES.Creator, _USER_ROLES.insurer_admin), quoteSlipController.createQuoteLocationOccupancy);

/**
 * @swagger
 * /api/v1/quote-locations/{id}:
 *   get:
 *     description: Fetches the quote locations occupancy of specified ID.
 *     tags:
 *       - Quote Location Occupancy
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: quote locations occupancy.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IQuoteLocationOccupancy'
 */
router.route("/:id").get(quoteSlipController.getQuoteLocationOccupancy);

/**
 * @swagger
 * /api/v1/quote-locations/{id}:
 *   patch:
 *     description: Updates the quote locations occupancy of specified ID.
 *     tags:
 *       - Quote Location Occupancy
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               clientLocationId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *
 *               occupancyId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *
 *               quoteId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *
 *               sumAssured:
 *                 type: number
 *                 required: true
 *     responses:
 *       200:
 *         description: quote locations occupancy.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IQuoteLocationOccupancy'
 */
router.route("/:id").patch(authController.restrictTo("admin"), quoteSlipController.updateQuoteLocationOccupancy);

/**
 * @swagger
 * /api/v1/quote-locations/{id}:
 *   delete:
 *     description: Deletes the quote locations occupancy of specified ID.
 *     tags:
 *       - Quote Location Occupancy
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: quote locations occupancy.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IQuoteLocationOccupancy'
 */
router.route("/:id").delete(authController.restrictTo("admin"), quoteSlipController.deleteQuoteLocationOccupancy);
