import express from "express";

import reviewController from "../controllers/reviewController";
import authController from "../controllers/authController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

router.route("/").get(reviewController.setTourInQuery, reviewController.getAllReviews);

router.route("/").post(authController.restrictTo("user", "broker_admin"), reviewController.setTourUserIds, reviewController.createReview);

router.route("/:id").get(reviewController.getReview);

router.route("/:id").patch(authController.restrictTo("user", "admin"), reviewController.updateReview);

router.route("/:id").delete(authController.restrictTo("user", "admin"), reviewController.deleteReview);
