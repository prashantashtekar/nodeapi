import express from "express";
import quoteLocationAddonCoversController from "../controllers/quoteLocationAddonCoversController";
import authController from "../controllers/authController";
import quoteSlipController from "../controllers/quoteSlipController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/quote-location-addon-covers:
 *   get:
 *     description: Fetches all the Quote Location Addon Covers.
 *     tags:
 *       - Quote Location Addon Covers
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Quote Location Addon Covers.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IQuoteLocationAddonCovers'
 */
router.route("/").get(quoteLocationAddonCoversController.getAllQuoteLocationAddonCovers);
router.route("/prime").post(quoteLocationAddonCoversController.getAllQuoteLocationAddonCoversPrime);
router.route("/batch-delete").delete(quoteLocationAddonCoversController.batchDeleteQuoteLocationAddonCovers);
/**
 * @swagger
 * /api/v1/quote-location-addon-covers:
 *   post:
 *     description: Endpoint used to create a Quote Location Addon Cover.
 *     tags:
 *       - Quote Location Addon Covers
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 required: true
 *
 *               countryId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *
 *     response:
 *       200:
 *         description: New Quote Location Addon Cover is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IQuoteLocationAddonCovers'
 */
router.route("/").post(authController.restrictTo("admin"), quoteLocationAddonCoversController.createQuoteLocationAddonCover);

/**
 * @swagger
 * /api/v1/quote-location-addon-covers/{id}:
 *   get:
 *     description: Fetches the Quote Location Addon Cover of specified ID.
 *     tags:
 *       - Quote Location Addon Covers
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Quote Location Addon Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IQuoteLocationAddonCovers'
 */
router.route("/:id").get(quoteLocationAddonCoversController.getQuoteLocationAddonCover);

/**
 * @swagger
 * /api/v1/quote-location-addon-covers/{id}:
 *   patch:
 *     description: Updates the Quote Location Addon Cover of specified ID.
 *     tags:
 *       - Quote Location Addon Covers
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 required: true
 *
 *               countryId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *     responses:
 *       200:
 *         description: Quote Location Addon Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IQuoteLocationAddonCovers'
 */
router.route("/:id").patch(authController.restrictTo("admin"), quoteLocationAddonCoversController.updateQuoteLocationAddonCover);

/**
 * @swagger
 * /api/v1/quote-location-addon-covers/{id}:
 *   delete:
 *     description: Deletes the Quote Location Addon Cover of specified ID.
 *     tags:
 *       - Quote Location Addon Covers
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: Quote Location Addon Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IQuoteLocationAddonCovers'
 */
router.route("/:id").delete(authController.restrictTo("admin"), quoteLocationAddonCoversController.deleteQuoteLocationAddonCover);
