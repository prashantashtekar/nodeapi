import express from "express";

import partnerController from "../controllers/partnerController";
import authController from "../controllers/authController";
import userController from "../controllers/userController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/partners:
 *   get:
 *     description: Endpoint used to create a partner.
 *     tags:
 *       - Partner
 *
 *     response:
 *       200:
 *         description: Get all partners.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IPartner'
 */
router.route("/").get(partnerController.getAllPartners);
router.route("/prime").post(partnerController.getAllPartnersPrime);

router.route("/batch-delete").delete(partnerController.batchDeletePartners);
/**
 * @swagger
 * /api/v1/partners:
 *   post:
 *     description: Endpoint used to create a partner.
 *     tags:
 *       - Partner
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               parterType:
 *                 type: string
 *                 description: User's name
 *                 required: true
 *
 *               name:
 *                 type: string
 *                 required: true
 *
 *               shortName:
 *                 type: string
 *                 description: User's email which is used for registering
 *                 required: true
 *
 *               address:
 *                 required: true
 *                 type: string
 *                 example: abc
 *
 *               cityId:
 *                 required: true
 *                 type: string
 *                 example: city1234
 *
 *               districtId:
 *                 required: true
 *                 type: string
 *                 example: district1234
 *
 *               stateId:
 *                 required: true
 *                 type: string
 *                 example: state1234
 *
 *               pincodeId:
 *                 required: true
 *                 type: string
 *                 example: pincode1234
 *
 *               countryId:
 *                 required: true
 *                 type: string
 *                 example: country1234
 *
 *               pan:
 *                 required: true
 *                 type: string
 *                 example: 1234
 *
 *               gstin:
 *                 required: true
 *                 type: string
 *                 example: 1234
 *
 *               cin:
 *                 required: true
 *                 type: string
 *                 example: 1234
 *
 *               contactPerson:
 *                 type: string
 *                 example: test
 *
 *               designation:
 *
 *                 type: string
 *                 example: admin
 *
 *               mobileNumber:
 *                 type: string
 *                 example: 9234567890
 *               status:
 *                 type: string
 *                 example: valid
 *
 *
 *     response:
 *       200:
 *         description: The new partner id created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IPartner'
 */
router.route("/").post(authController.restrictTo("admin"), partnerController.createPartner);

/**
 * @swagger
 * /api/v1/partners/{id}:
 *   get:
 *     description: Endpoint used to get a partner.
 *     tags:
 *       - Partner
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *
 *     response:
 *       200:
 *         description: Get one partner.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IPartner'
 *
 */
router.route("/:id").get(partnerController.getPartner);

router.route("/:id").patch(authController.restrictTo("admin"), partnerController.updatePartner);
router.route("/:id").delete(authController.restrictTo("admin"), partnerController.deletePartner);
router.route("/delete-many").delete(authController.restrictTo("admin"), partnerController.deleteManyPartners);
router.route("/diff/:id").post(userController.getUserDiffHistory);
