import express from "express";

import countryMasterController from "../controllers/countryMasterController";
import authController from "../controllers/authController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/countries:
 *   get:
 *     description: Fetches all the countries.
 *     tags:
 *       - Country Master
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Countries.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ICountryMaster'
 */
router.route("/").get(countryMasterController.getAllCountryMasters);
router.route("/prime").post(countryMasterController.getAllCountryMasterPrime);
/**
 * @swagger
 * /api/v1/countries:
 *   post:
 *     description: Endpoint used to create a country.
 *     tags:
 *       - Country Master
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 required: true
 *
 *     response:
 *       200:
 *         description: New country is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ICountryMaster'
 */
router.route("/").post(authController.restrictTo("admin"), countryMasterController.createCountryMaster);

/**
 * @swagger
 * /api/v1/countries/{id}:
 *   get:
 *     description: Fetches the country of specified ID.
 *     tags:
 *       - Country Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Country.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ICountryMaster'
 */
router.route("/:id").get(countryMasterController.getCountryMaster);

/**
 * @swagger
 * /api/v1/countries/{id}:
 *   patch:
 *     description: Updates the country of specified ID.
 *     tags:
 *       - Country Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 required: true
 *     responses:
 *       200:
 *         description: Country.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ICountryMaster'
 */
router.route("/:id").patch(authController.restrictTo("admin"), countryMasterController.updateCountryMaster);

/**
 * @swagger
 * /api/v1/countries/{id}:
 *   delete:
 *     description: Deletes the country of specified ID.
 *     tags:
 *       - Country Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: Country.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ICountryMaster'
 */
router.route("/:id").delete(authController.restrictTo("admin"), countryMasterController.deleteCountryMaster);
