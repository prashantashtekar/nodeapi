import express from "express";

import tourController from "../controllers/tourController";
import authController from "../controllers/authController";
import { router as reviewRouter } from "./reviewRoutes";
import { _USER_ROLES } from "../models/userModel";

export const router = express.Router();

// merge routes. this is same as mounting a router.
// since we are mounting the below inside this file, it will automatically create
// urls which are prefixed with /tours
router.use("/:tourId/reviews", reviewRouter);

router.route("/top-5-cheap").get(tourController.aliasTopTours, tourController.getAllTours);

router.route("/tour-stats").get(tourController.getTourStats);

router
  .route("/monthly-plan/:year")
  .get(authController.protect, authController.restrictTo("admin", _USER_ROLES.Creator, _USER_ROLES.insurer_admin), tourController.getMonthlyPlan);

router.route("/").get(authController.protect, tourController.getAllTours);

router.route("/").post(authController.protect, authController.restrictTo("admin", _USER_ROLES.Creator, _USER_ROLES.insurer_admin), tourController.createTour);

router.route("/:id").get(authController.protect, tourController.getTour);

router.route("/:id").patch(authController.protect, authController.restrictTo("admin", _USER_ROLES.Creator, _USER_ROLES.insurer_admin), tourController.updateTour);

router.route("/:id").delete(authController.protect, authController.restrictTo("admin", _USER_ROLES.Creator, _USER_ROLES.insurer_admin), tourController.deleteTour);
