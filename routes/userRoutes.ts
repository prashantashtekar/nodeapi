import express from "express";
import userController from "../controllers/userController";
import authController from "../controllers/authController";

export const router = express.Router();

// Authentication related routes.

/**
 * @swagger
 * /api/v1/users/signup:
 *   post:
 *     description: Endpoint used to register a user.
 *     tags:
 *       - User
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: User's name
 *                 required: true
 *                 example: John
 *
 *
 *               mobileNumber:
 *                 type: string
 *                 required: true
 *
 *               email:
 *                 type: string
 *                 description: User's email which is used for registering
 *                 required: true
 *                 example: test@test.com
 *
 *               password:
 *                 required: true
 *                 type: string
 *                 example: test1234
 *
 *               passwordConfirm:
 *                 required: true
 *                 type: string
 *                 example: test1234
 *               role:
 *                 required: true
 *                 type: string
 *                 example: admin
 *               partnerId:
 *                 required: true
 *                 type: string
 *                 example: 1234
 *
 *
 *     responses:
 *       200:
 *         description: The user object who just signed in.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IUser'
 */

router.post("/signup", authController.signup);

/**
 * @swagger
 * /api/v1/users/login:
 *   post:
 *     description: Endpoint used to generate a token, which can then be used for other endpoints.
 *     tags:
 *       - User
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *                 description: The user's email, which is used as the username.
 *                 example: jay.mehta@gmail.com
 *               password:
 *                 type: string
 *                 description: The user's password.
 *                 example: test1234
 *     responses:
 *       200:
 *         description: The user object who just logged in.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IUser'
 */
router.post("/login", authController.login);

router.post("/forgot-password", authController.forgotPassword);
router.patch("/reset-password/:token", authController.resetPassword);

// Adding the authentication endpoint here.
// All the handlers that come after this point will automatically have the "protect" middleware added to them.
router.use(authController.protect);

router.patch("/update-my-password", authController.updatePassword);

/**
 * @swagger
 * /api/v1/users/me:
 *   get:
 *     description: Fetches the currently logged in users, User object.
 *     tags:
 *       - User
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: The user object who just logged in.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IUser'
 */
router.get("/me", userController.getMe, userController.getUser);

// /**
//  * @swagger
//  * /api/v1/users/update-me:
//  *   patch:
//  *     description: Endpoint used to update data.
//  *     tags:
//  *       - User
//  *     requestBody:
//  *       required: true
//  *       content:
//  *         application/json:
//  *           schema:
//  *             type: object
//  *             properties:
//  *               name:
//  *                 type: string
//  *                 description: User's name
//  *                 required: true
//  *                 example: John
//  *
//  *
//  *               mobileNumber:
//  *                 type: string
//  *                 required: true
//  *
//  *               email:
//  *                 type: string
//  *                 description: User's email which is used for registering
//  *                 required: true
//  *                 example: test@test.com
//  *
//  *               password:
//  *                 required: true
//  *                 type: string
//  *                 example: test1234
//  *
//  *               passwordConfirm:
//  *                 required: true
//  *                 type: string
//  *                 example: test1234
//  *               role:
//  *                 required: true
//  *                 type: string
//  *                 example: admin
//  *               partnerId:
//  *                 required: true
//  *                 type: string
//  *                 example: 1234
//  *     responses:
//  *       200:
//  *         description: The user object who just logged in.
//  *         content:
//  *           application/json:
//  *             schema:
//  *               $ref: '#/components/schemas/IUser'
//  */
router.patch("/update-me", userController.uploadUserPhoto, userController.updateMe);
router.patch("/update-photo/:id", userController.uploadUserPhoto, userController.updatePhoto);

/**
 * @swagger
 * /api/v1/users/delete-me:
 *   delete:
 *     description: deletes the currently logged in user.
 *     tags:
 *       - User
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: The user object who just logged in.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IUser'
 */
router.delete("/delete-me", userController.deleteMe);

// Adding one more middleware here, everything below this will have this as  the middleware added.
router.use(authController.restrictTo("admin"));

// User routes

/**
 * @swagger
 * /api/v1/users:
 *   get:
 *     description: Fetches the users, User object.
 *     tags:
 *       - User
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: All the user objects.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IUser'
 */
router.route("/").get(userController.getAllUsers);
router.route("/prime").post(userController.getAllUsersPrime);

/**
 * @swagger
 * /api/v1/users:
 *   post:
 *     description: Endpoint used to create a user.
 *     tags:
 *       - User
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 description: User's name
 *                 required: true
 *                 example: John
 *
 *
 *               mobileNumber:
 *                 type: string
 *                 required: true
 *
 *               email:
 *                 type: string
 *                 description: User's email which is used for registering
 *                 required: true
 *                 example: test@test.com
 *
 *               password:
 *                 required: true
 *                 type: string
 *                 example: test1234
 *
 *               passwordConfirm:
 *                 required: true
 *                 type: string
 *                 example: test1234
 *               role:
 *                 required: true
 *                 type: string
 *                 example: admin
 *               partnerId:
 *                 required: true
 *                 type: string
 *                 example: 1234
 *
 *
 *     responses:
 *       200:
 *         description: The user object who just got created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IUser'
 */
router.route("/").post(userController.createUser);

/**
 * @swagger
 * /api/v1/users/{id}:
 *   get:
 *     description: Fetches the user with the mentioned ID.
 *     tags:
 *       - User
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: User with the specified ID.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IUser'
 */
router.route("/:id").get(userController.getUser);

/**
 * @swagger
 * /api/v1/users/{id}:
 *   delete:
 *     description: deletes the user with the mentioned ID.
 *     tags:
 *       - User
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: User with the specified ID.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IUser'
 */
router.route("/:id").delete(userController.deleteUser);
router.route("/batch-delete").delete(userController.batchDeleteUsers);
// router.route("/delete-many").delete(authController.restrictTo("admin"), userController.deleteManyUsers);

router.route("/:id").patch(userController.updateUser);
router.route("/diff/:id").post(userController.getUserDiffHistory);

