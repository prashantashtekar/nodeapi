import express from "express";
import authController from "../controllers/authController";
import { _USER_ROLES } from "../models/userModel";
import clientGroupMasterController from "../controllers/clientGroupMasterController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/client-groups:
 *   get:
 *     description: Fetches all client groups.
 *     tags:
 *       - Client Group Master
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Client Group master.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClientGroupMaster'
 *
 */
router.route("/").get(clientGroupMasterController.getAllGroupMaster);
router.route("/prime").post(clientGroupMasterController.getAllGroupMastersPrime);
router.route("/batch-delete").delete(clientGroupMasterController.batchDeleteGroupMasters);
/**
 * @swagger
 * /api/v1/client-groups:
 *   post:
 *     description: Endpoint used to create a client group.
 *     tags:
 *       - Client Group Master
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               vin:
 *                 type: string
 *                 required: true
 *
 *               clientGroupName:
 *                 type: string
 *                 required: true
 *
 *               clientGroupContactName:
 *                 type: string
 *                 required: true
 *
 *               address:
 *                 required: true
 *                 type: string
 *                 example: abc
 *
 *               cityId:
 *                 required: true
 *                 type: string
 *                 example: city1234
 *
 *               pincodeId:
 *                 required: true
 *                 type: string
 *
 *               email:
 *                 required: true
 *                 type: string
 *
 *     response:
 *       200:
 *         description: New client group is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClientGroupMaster'
 */
router.route("/").post(authController.restrictTo("admin"), clientGroupMasterController.createGroupMaster);

/**
 * @swagger
 * /api/v1/client-groups/{id}:
 *   get:
 *     description: Fetches the client group with matching ID.
 *     tags:
 *       - Client Group Master
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Client Group master with matching ID.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClientGroupMaster'
 */
router.route("/:id").get(clientGroupMasterController.getGroupMaster);

/**
 * @swagger
 * /api/v1/client-groups/{id}:
 *   patch:
 *     description: Endpoint used to update client Group.
 *     tags:
 *       - Client Group Master
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               vin:
 *                 type: string
 *                 required: true
 *
 *               clientGroupName:
 *                 type: string
 *                 required: true
 *
 *               clientGroupContactName:
 *                 type: string
 *                 required: true
 *
 *               address:
 *                 required: true
 *                 type: string
 *                 example: abc
 *
 *               cityId:
 *                 required: true
 *                 type: string
 *                 example: city1234
 *
 *               pincodeId:
 *                 required: true
 *                 type: string
 *
 *               email:
 *                 required: true
 *                 type: string
 *
 *     response:
 *       200:
 *         description: Client group updated.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClientGroupMaster'
 */
router.route("/:id").patch(authController.restrictTo("admin"), clientGroupMasterController.updateGroupMaster);

/**
 * @swagger
 * /api/v1/client-groups/{id}:
 *   delete:
 *     description: Deletes the client group with matching ID.
 *     tags:
 *       - Client Group Master
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Client Group master with matching ID deleted.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClientGroupMaster'
 */
router.route("/:id").delete(authController.restrictTo("admin"), clientGroupMasterController.deleteGroupMaster);

router
  .route("/match")
  .get(authController.restrictTo(_USER_ROLES.SuperAdmin, _USER_ROLES.Creator, _USER_ROLES.insurer_admin), clientGroupMasterController.getMatchingClientGroups);

router.route("/resolve").post(authController.restrictTo("admin", _USER_ROLES.Creator, _USER_ROLES.insurer_admin), clientGroupMasterController.resolveClientGroup);
