import express from "express";

import authController from "../controllers/authController";
import occupancyController from "../controllers/occupancyController";
import { _USER_ROLES } from "../models/userModel";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/occupancy:
 *   get:
 *     description: Fetches all the occupancies.
 *     tags:
 *       - Occupancy
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Occupancies.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IOccupancy'
 */
router.route("/").get(occupancyController.getAllOccupencies);
router.route("/prime").post(occupancyController.getAllOccupanciesPrime);
router.route("/batch-delete").delete(occupancyController.batchDeleteOccupancies);
/**
 * @swagger
 * /api/v1/occupancy:
 *   post:
 *     description: Endpoint used to create an occupancy.
 *     tags:
 *       - Occupancy
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               occupancyType:
 *                 type: string
 *                 required: true
 *               hazardCategoryId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *               gradedRetention:
 *                 type: boolean
 *               tacCode:
 *                 type: string
 *               flexaiib:
 *                 type: number
 *               stfiRate:
 *                 type: number
 *               section:
 *                 type: string
 *                 required: true
 *               riskCode:
 *                 type: number
 *               rateCode:
 *                 type: number
 *               policyType:
 *                 type: string
 *               specialRemark:
 *                 type: string
 *               specialExcessGIC:
 *                 type: string
 *               iibOccupancyCode:
 *                 type: string
 *               effectiveFrom:
 *                 type: date
 *               effectiveTo:
 *                 type: date
 *               productId:
 *                 type: Schema.Types.ObjectId
 *               applicableFrom:
 *                  type: date
 *               applicableTo:
 *                  type: date
 *
 *     response:
 *       200:
 *         description: New occupancy is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IOccupancy'
 */
router.route("/").post(authController.restrictTo("admin", _USER_ROLES.Creator, _USER_ROLES.insurer_admin), occupancyController.createOccupancy);

/**
 * @swagger
 * /api/v1/occupancy/{id}:
 *   get:
 *     description: Fetches the occupancy of specified ID.
 *     tags:
 *       - Occupancy
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: occupancy.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IOccupancy'
 */
router.route("/:id").get(occupancyController.getOccupancy);

/**
 * @swagger
 * /api/v1/occupancy/{id}:
 *   patch:
 *     description: Updates the occupancy of specified ID.
 *     tags:
 *       - Occupancy
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               occupancyType:
 *                 type: string
 *                 required: true
 *               hazardCategoryId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *               gradedRetention:
 *                 type: boolean
 *               tacCode:
 *                 type: string
 *               flexaiib:
 *                 type: number
 *               stfiRate:
 *                 type: number
 *               section:
 *                 type: string
 *                 required: true
 *               riskCode:
 *                 type: number
 *               rateCode:
 *                 type: number
 *               policyType:
 *                 type: string
 *               specialRemark:
 *                 type: string
 *               specialExcessGIC:
 *                 type: string
 *               iibOccupancyCode:
 *                 type: string
 *               effectiveFrom:
 *                 type: date
 *               effectiveTo:
 *                 type: date
 *               productId:
 *                 type: Schema.Types.ObjectId
 *               applicableFrom:
 *                  type: date
 *               applicableTo:
 *                  type: date
 *
 *     responses:
 *       200:
 *         description: occupancy.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IOccupancy'
 */
router.route("/:id").patch(authController.restrictTo("admin"), occupancyController.updateOccupancy);

/**
 * @swaggerschema
 * /api/v1/occupancy/{id}:
 *   delete:
 *     description: Deletes the occupancy of specified ID.
 *     tags:
 *       - Occupancy
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: occupancy.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IOccupancy'
 */
router.route("/:id").delete(authController.restrictTo("admin"), occupancyController.deleteOccupancy);

/**
 * @swagger
 * /api/v1/occupancy/match:
 *   get:
 *     description: Fetches the occupancy of specified ID.
 *     tags:
 *       - Occupancy
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: occupancy.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IOccupancy'
 */
router.route("/match").get(authController.restrictTo("admin", _USER_ROLES.Creator, _USER_ROLES.insurer_admin), occupancyController.getMatchingOccupancies);
