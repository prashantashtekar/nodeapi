import express from "express";

import sectorMasterController from "../controllers/sectorMasterController";
import authController from "../controllers/authController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/sector-masters:
 *   get:
 *     description: Endpoint to get all sectors.
 *     tags:
 *       - Sector Master
 *     security:
 *       - bearerAuth: []
 *
 *     responses:
 *       200:
 *         description: Get all sectors.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ISectorMaster'
 */
router.route("/").get(sectorMasterController.getAllSectorMasters);
router.route("/prime").post(sectorMasterController.getAllSectorMastersPrime);
router.route("/batch-delete").delete(sectorMasterController.batchDeleteSectorMasters);
/**
 * @swagger
 * /api/v1/sector-masters:
 *   post:
 *     description: Endpoint to create a Sector.
 *     tags:
 *       - Sector Master
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *
 *             properties:
 *               name:
 *                 type: string
 *                 description: Sector name
 *                 required: true
 *
 *     responses:
 *       200:
 *         description: Sector that was just created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ISectorMaster'
 */
router.route("/").post(authController.restrictTo("admin"), sectorMasterController.createSectorMaster);

/**
 * @swagger
 * /api/v1/sector-masters/{id}:
 *   get:
 *     description: Endpoint to get a sector.
 *     tags:
 *       - Sector Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *
 *     responses:
 *       200:
 *         description: Sector with specified ID.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ISectorMaster'
 */
router.route("/:id").get(sectorMasterController.getSectorMaster);

/**
 * @swagger
 * /api/v1/sector-masters/{id}:
 *   patch:
 *     description: Endpoint to update a Sector.
 *     tags:
 *       - Sector Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *
 *             properties:
 *               name:
 *                 type: string
 *                 description: Sector name
 *                 required: true
 *
 *     responses:
 *       200:
 *         description: Sector that was just updated.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ISectorMaster'
 */
router.route("/:id").patch(authController.restrictTo("admin"), sectorMasterController.updateSectorMaster);

/**
 * @swagger
 * /api/v1/sector-masters/{id}:
 *   delete:
 *     description: Endpoint to delete a Sector.
 *     tags:
 *       - Sector Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *
 *     responses:
 *       200:
 *         description: Sector with specified ID deleted.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ISectorMaster'
 */
router.route("/:id").delete(authController.restrictTo("admin"), sectorMasterController.deleteSectorMaster);
