import express from "express";

import bscBurglaryHousebreakingCoverController from "../controllers/bscBurglaryHousebreakingCoverController";
import authController from "../controllers/authController";
import { _USER_ROLES } from "../models/userModel";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/bsc-burglary-housebreaking-cover:
 *   get:
 *     description: Fetches all the BSC Burglary House Breaking Cover.
 *     tags:
 *       - BSC Burglary House Breaking Cover
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: BSC Burglary House Breaking Covers.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscBurglaryHousebreakingCover'
 */
router.route("/").get(bscBurglaryHousebreakingCoverController.getAllBscBurglaryHousebreakingCovers);
router.route("/prime").post(bscBurglaryHousebreakingCoverController.getAllBscBurglaryHousebreakingCoverPrime);
router.route("/batch-delete").delete(bscBurglaryHousebreakingCoverController.batchDeleteBscBurglaryHousebreakingCover);
/**
 * @swagger
 * /api/v1/bsc-burglary-housebreaking-cover:
 *   post:
 *     description: Endpoint used to create a BSC Burglary House Breaking Cover.
 *     tags:
 *       - BSC Burglary House Breaking Cover
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               locationId:
 *                 type: Schema.Types.ObjectId
 *               firstLoss:
 *                 type: Number
 *               stocks:
 *                 type: Number
 *               otherContents:
 *                 type: Number
 *               firstLossSumInsured:
 *                 type: Number
 *               rsmd:
 *                 type: boolean
 *               theft:
 *                 type: boolean
 *               total:
 *                 type: Number
 *
 *     response:
 *       200:
 *         description: New BSC Burglary House Breaking Cover Cover is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscBurglaryHousebreakingCover'
 */
router.route("/").post(authController.restrictTo(_USER_ROLES.SuperAdmin), bscBurglaryHousebreakingCoverController.createBscBurglaryHousebreakingCover);

/**
 * @swagger
 * /api/v1/bsc-burglary-housebreaking-cover/{id}:
 *   get:
 *     description: Fetches the BSC Burglary House Breaking Cover of specified ID.
 *     tags:
 *       - BSC Burglary House Breaking Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: BSC Burglary House Breaking Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscBurglaryHousebreakingCover'
 */
router.route("/:id").get(bscBurglaryHousebreakingCoverController.getBscBurglaryHousebreakingCover);

/**
 * @swagger
 * /api/v1/bsc-burglary-housebreaking-cover/{id}:
 *   patch:
 *     description: Updates the BSC Burglary House Breaking Cover of specified ID.
 *     tags:
 *       - BSC Burglary House Breaking Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               locationId:
 *                 type: Schema.Types.ObjectId
 *               firstLoss:
 *                 type: Number
 *               stocks:
 *                 type: Number
 *               otherContents:
 *                 type: Number
 *               firstLossSumInsured:
 *                 type: Number
 *               rsmd:
 *                 type: boolean
 *               theft:
 *                 type: boolean
 *               total:
 *                 type: Number
 *     responses:
 *       200:
 *         description: BSC Burglary House Breaking Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscBurglaryHousebreakingCover'
 */
router.route("/:id").patch(authController.restrictTo(_USER_ROLES.SuperAdmin), bscBurglaryHousebreakingCoverController.updateBscBurglaryHousebreakingCover);

/**
 * @swagger
 * /api/v1/bsc-burglary-housebreaking-cover/{id}:
 *   delete:
 *     description: Deletes the BSC Burglary House Breaking Cover of specified ID.
 *     tags:
 *       - BSC Burglary House Breaking Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: BSC Burglary House Breaking Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscBurglaryHousebreakingCover'
 */
router.route("/:id").delete(authController.restrictTo(_USER_ROLES.SuperAdmin), bscBurglaryHousebreakingCoverController.deleteBscBurglaryHousebreakingCover);
