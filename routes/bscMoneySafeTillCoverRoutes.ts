import express from "express";

import bscMoneySafeTillCoverController from "../controllers/bscMoneySafeTillCoverController";
import authController from "../controllers/authController";
import { _USER_ROLES } from "../models/userModel";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/bsc-money-safe-till-cover:
 *   get:
 *     description: Fetches all the BSC Money Safe Till Covers.
 *     tags:
 *       - BSC Money Safe Till Cover
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: BSC Money Safe Till Covers.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscMoneySafeTillCover'
 */
router.route("/").get(bscMoneySafeTillCoverController.getAllBscMoneySafeTillCovers);
router.route("/prime").post(bscMoneySafeTillCoverController.getAllBscMoneySafeTillCoversPrime);
router.route("/batch-delete").delete(bscMoneySafeTillCoverController.batchDeleteBscMoneySafeTillCovers);
/**
 * @swagger
 * /api/v1/bsc-money-safe-till-cover:
 *   post:
 *     description: Endpoint used to create a BSC Money Safe Till Cover.
 *     tags:
 *       - BSC Money Safe Till Cover
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 required: true
 *
 *               countryId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *
 *     response:
 *       200:
 *         description: New BSC Money Safe Till Cover is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscMoneySafeTillCover'
 */
router.route("/").post(authController.restrictTo(_USER_ROLES.SuperAdmin), bscMoneySafeTillCoverController.createBscMoneySafeTillCover);

/**
 * @swagger
 * /api/v1/bsc-money-safe-till-cover/{id}:
 *   get:
 *     description: Fetches the BSC Money Safe Till Cover of specified ID.
 *     tags:
 *       - BSC Money Safe Till Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: BSC Money Safe Till Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscMoneySafeTillCover'
 */
router.route("/:id").get(bscMoneySafeTillCoverController.getBscMoneySafeTillCover);

/**
 * @swagger
 * /api/v1/bsc-money-safe-till-cover/{id}:
 *   patch:
 *     description: Updates the BSC Money Safe Till Cover of specified ID.
 *     tags:
 *       - BSC Money Safe Till Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 required: true
 *
 *               countryId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *     responses:
 *       200:
 *         description: BSC Money Safe Till Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscMoneySafeTillCover'
 */
router.route("/:id").patch(authController.restrictTo(_USER_ROLES.SuperAdmin), bscMoneySafeTillCoverController.updateBscMoneySafeTillCover);

/**
 * @swagger
 * /api/v1/bsc-money-safe-till-cover/{id}:
 *   delete:
 *     description: Deletes the BSC Money Safe Till Cover of specified ID.
 *     tags:
 *       - BSC Money Safe Till Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: BSC Money Safe Till Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscMoneySafeTillCover'
 */
router.route("/:id").delete(authController.restrictTo(_USER_ROLES.SuperAdmin), bscMoneySafeTillCoverController.deleteBscMoneySafeTillCover);
