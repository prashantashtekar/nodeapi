import express from "express";
import addonCoversController from "../controllers/addonCoversController";

import addOnCoverageIndustryController from "../controllers/addonCoverSectorController";
import authController from "../controllers/authController";
import { _USER_ROLES } from "../models/userModel";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/addOn-cover-sectors:
 *   get:
 *     description: Fetches all the Addon cover sectors.
 *     tags:
 *       - Addon Cover Sector
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Addon cover sectors.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IAddOnCoverSector'
 */
router.route("/").get(addOnCoverageIndustryController.getAllAddOnCoverSectors);
router.route("/prime").post(addOnCoverageIndustryController.getAllAddOnCoverSectorsPrime);
router.route("/batch-delete").delete(addOnCoverageIndustryController.batchDeleteAddOnCoverSectors);
/**
 * @swagger
 * /api/v1/addOn-cover-sectors:
 *   post:
 *     description: Endpoint used to create a Addon cover sector.
 *     tags:
 *       - Addon Cover Sector
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               addOnCoverId:
 *                 type: Schema.Types.ObjectId
 *               sectorId:
 *                 type: Schema.Types.ObjectId
 *               isApplicable:
 *                 type: boolean
 *               status:
 *                 type: boolean
 *
 *     response:
 *       200:
 *         description: New Addon cover sector is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IAddOnCoverSector'
 */
router.route("/").post(authController.restrictTo(_USER_ROLES.SuperAdmin), addOnCoverageIndustryController.createAddOnCoverSector);

/**
 * @swagger
 * /api/v1/addOn-cover-sectors/{id}:
 *   get:
 *     description: Fetches the Addon cover sector of specified ID.
 *     tags:
 *       - Addon Cover Sector
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Addon cover sector.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IAddOnCoverSector'
 */
router.route("/:id").get(addOnCoverageIndustryController.getAddOnCoverSector);

/**
 * @swagger
 * /api/v1/addOn-cover-sectors/{id}:
 *   patch:
 *     description: Updates the Addon cover sector of specified ID.
 *     tags:
 *       - Addon Cover Sector
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               addOnCoverId:
 *                 type: Schema.Types.ObjectId
 *               sectorId:
 *                 type: Schema.Types.ObjectId
 *               isApplicable:
 *                 type: boolean
 *               status:
 *                 type: boolean
 *     responses:
 *       200:
 *         description: Addon cover sector.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IAddOnCoverSector'
 */
router.route("/:id").patch(authController.restrictTo(_USER_ROLES.SuperAdmin), addOnCoverageIndustryController.updateAddOnCoverSector);

/**
 * @swagger
 * /api/v1/addOn-cover-sectors/{id}:
 *   delete:
 *     description: Deletes the Addon cover sector of specified ID.
 *     tags:
 *       - Addon Cover Sector
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: Addon cover sector.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IAddOnCoverSector'
 */
router.route("/:id").delete(authController.restrictTo(_USER_ROLES.SuperAdmin), addOnCoverageIndustryController.deleteAddOnCoverSector);
