import express from "express";

import bscElectronicEquipmentsCoverController from "../controllers/bscElectronicEquipmentsCoverController";
import authController from "../controllers/authController";
import { _USER_ROLES } from "../models/userModel";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/bsc-electronic-equipments-cover:
 *   get:
 *     description: Fetches all the BSC Electronic Equipments Covers.
 *     tags:
 *       - BSC Electronic Equipments Cover
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: BSC Electronic Equipments Covers.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscElectronicEquipmentsCover'
 */
router.route("/").get(bscElectronicEquipmentsCoverController.getAllBscElectronicEquipmentsCovers);
router.route("/prime").post(bscElectronicEquipmentsCoverController.getAllBscElectronicEquipmentsCoverPrime);
router.route("/batch-delete").delete(bscElectronicEquipmentsCoverController.batchDeleteBscElectronicEquipmentsCover);
/**
 * @swagger
 * /api/v1/bsc-electronic-equipments-cover:
 *   post:
 *     description: Endpoint used to create a BSC Electronic Equipments Cover.
 *     tags:
 *       - BSC Electronic Equipments Cover
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               locationId:
 *                 type: Schema.Types.ObjectId
 *               descriptionEquipments:
 *                 type: string
 *               sumInsured:
 *                 type: number
 *               total:
 *                 type: number
 *
 *     response:
 *       200:
 *         description: New BSC Electronic Equipments Cover is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscElectronicEquipmentsCover'
 */
router.route("/").post(authController.restrictTo(_USER_ROLES.SuperAdmin), bscElectronicEquipmentsCoverController.createBscElectronicEquipmentsCover);

/**
 * @swagger
 * /api/v1/bsc-electronic-equipments-cover/{id}:
 *   get:
 *     description: Fetches the BSC Electronic Equipments Cover of specified ID.
 *     tags:
 *       - BSC Electronic Equipments Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: BSC Electronic Equipments Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscElectronicEquipmentsCover'
 */
router.route("/:id").get(bscElectronicEquipmentsCoverController.getBscElectronicEquipmentsCover);

/**
 * @swagger
 * /api/v1/bsc-electronic-equipments-cover/{id}:
 *   patch:
 *     description: Updates the BSC Electronic Equipments Cover of specified ID.
 *     tags:
 *       - BSC Electronic Equipments Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               locationId:
 *                 type: Schema.Types.ObjectId
 *               descriptionEquipments:
 *                 type: string
 *               sumInsured:
 *                 type: number
 *               total:
 *                 type: number
 *
 *     responses:
 *       200:
 *         description: BSC Electronic Equipments Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscElectronicEquipmentsCover'
 */
router.route("/:id").patch(authController.restrictTo(_USER_ROLES.SuperAdmin), bscElectronicEquipmentsCoverController.updateBscElectronicEquipmentsCover);

/**
 * @swagger
 * /api/v1/bsc-electronic-equipments-cover/{id}:
 *   delete:
 *     description: Deletes the BSC Electronic Equipments Cover of specified ID.
 *     tags:
 *       - BSC Electronic Equipments Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: BSC Electronic Equipments Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscElectronicEquipmentsCover'
 */
router.route("/:id").delete(authController.restrictTo(_USER_ROLES.SuperAdmin), bscElectronicEquipmentsCoverController.deleteBscElectronicEquipmentsCover);
