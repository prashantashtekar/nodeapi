import express from "express";

import bscFireLossOfProfitCoverController from "../controllers/bscFireLossOfProfitCoverController";
import authController from "../controllers/authController";
import { _USER_ROLES } from "../models/userModel";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/bsc-fireLossOfProfit-cover:
 *   get:
 *     description: Fetches all the BSC Loss of Profit Covers.
 *     tags:
 *       - BSC Loss of Profit Cover
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: BSC Loss of Profit Covers.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscFireLossOfProfitCover'
 */
router.route("/").get(bscFireLossOfProfitCoverController.getAllBscFireLossOfProfitCovers);
router.route("/prime").post(bscFireLossOfProfitCoverController.getAllBscFireLossOfProfitCoverPrime);
router.route("/batch-delete").delete(bscFireLossOfProfitCoverController.batchDeleteBscFireLossOfProfitCover);
/**
 * @swagger
 * /api/v1/bsc-fireLossOfProfit-cover:
 *   post:
 *     description: Endpoint used to create a BSC Loss of Profit Cover.
 *     tags:
 *       - BSC Loss of Profit Cover
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               grossProfit:
 *                 type: string
 *               indmenityPeriod:
 *                 type: string
 *               terrorism:
 *                 type: boolean
 *               auditorsFees:
 *                 type: number
 *
 *     response:
 *       200:
 *         description: New BSC Loss of Profit Cover is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscFireLossOfProfitCover'
 */
router.route("/").post(authController.restrictTo(_USER_ROLES.SuperAdmin), bscFireLossOfProfitCoverController.createBscFireLossOfProfitCover);

/**
 * @swagger
 * /api/v1/bsc-fireLossOfProfit-cover/{id}:
 *   get:
 *     description: Fetches the BSC Loss of Profit Cover of specified ID.
 *     tags:
 *       - BSC Loss of Profit Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: BSC Loss of Profit Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscFireLossOfProfitCover'
 */
router.route("/:id").get(bscFireLossOfProfitCoverController.getBscFireLossOfProfitCover);

/**
 * @swagger
 * /api/v1/bsc-fireLossOfProfit-cover/{id}:
 *   patch:
 *     description: Updates the BSC Loss of Profit Cover of specified ID.
 *     tags:
 *       - BSC Loss of Profit Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               grossProfit:
 *                 type: string
 *               indmenityPeriod:
 *                 type: string
 *               terrorism:
 *                 type: boolean
 *               auditorsFees:
 *                 type: number
 *     responses:
 *       200:
 *         description: BSC Loss of Profit Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscFireLossOfProfitCover'
 */
router.route("/:id").patch(authController.restrictTo(_USER_ROLES.SuperAdmin), bscFireLossOfProfitCoverController.updateBscFireLossOfProfitCover);

/**
 * @swagger
 * /api/v1/bsc-fireLossOfProfit-cover/{id}:
 *   delete:
 *     description: Deletes the BSC Loss of Profit Cover of specified ID.
 *     tags:
 *       - BSC Loss of Profit Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: BSC Loss of Profit Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscFireLossOfProfitCover'
 */
router.route("/:id").delete(authController.restrictTo(_USER_ROLES.SuperAdmin), bscFireLossOfProfitCoverController.deleteBscFireLossOfProfitCover);
