import express from "express";

import bscSignageCoverController from "../controllers/bscSignageCoverController";
import authController from "../controllers/authController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/bsc-signage-cover:
 *   get:
 *     description: Fetches all the BSC Signage Covers.
 *     tags:
 *       - BSC Signage Cover
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: BSC Signage Covers.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscSignageCover'
 */
router.route("/").get(bscSignageCoverController.getAllBscSignageCovers);
router.route("/prime").post(bscSignageCoverController.getAllBscSignageCoversPrime);
router.route("/batch-delete").delete(bscSignageCoverController.batchDeleteBscSignageCovers);
/**
 * @swagger
 * /api/v1/bsc-signage-cover:
 *   post:
 *     description: Endpoint used to create a BSC Signage Cover.
 *     tags:
 *       - BSC Signage Cover
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               signageType:
 *                 type: string
 *               signageDescription:
 *                 type: string
 *               sumInsured:
 *                 type: number
 *               total:
 *                 type: number
 *
 *     response:
 *       200:
 *         description: New BSC Signage Cover is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscSignageCover'
 */
router.route("/").post(authController.restrictTo("admin"), bscSignageCoverController.createBscSignageCover);

/**
 * @swagger
 * /api/v1/bsc-signage-cover/{id}:
 *   get:
 *     description: Fetches the BSC Signage Cover of specified ID.
 *     tags:
 *       - BSC Signage Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: BSC Signage Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscSignageCover'
 */
router.route("/:id").get(bscSignageCoverController.getBscSignageCover);

/**
 * @swagger
 * /api/v1/bsc-signage-cover/{id}:
 *   patch:
 *     description: Updates the BSC Signage Cover of specified ID.
 *     tags:
 *       - BSC Signage Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               signageType:
 *                 type: string
 *               signageDescription:
 *                 type: string
 *               sumInsured:
 *                 type: number
 *               total:
 *                 type: number
 *     responses:
 *       200:
 *         description: BSC Signage Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscSignageCover'
 */
router.route("/:id").patch(authController.restrictTo("admin"), bscSignageCoverController.updateBscSignageCover);

/**
 * @swagger
 * /api/v1/bsc-signage-cover/{id}:
 *   delete:
 *     description: Deletes the BSC Signage Cover of specified ID.
 *     tags:
 *       - BSC Signage Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: BSC Signage Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscSignageCover'
 */
router.route("/:id").delete(authController.restrictTo("admin"), bscSignageCoverController.deleteBscSignageCover);
