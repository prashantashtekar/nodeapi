import express from "express";

import bscPortableEquipmentsCoverController from "../controllers/bscPortableEquipmentsCoverController";
import authController from "../controllers/authController";
import { _USER_ROLES } from "../models/userModel";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/bsc-portable-equipments-cover:
 *   get:
 *     description: Fetches all the BSC Portable Equipments Covers.
 *     tags:
 *       - BSC Portable Equipments Cover
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: BSC Portable Equipments Covers.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscPortableEquipmentsCover'
 */
router.route("/").get(bscPortableEquipmentsCoverController.getAllBscPortableEquipmentsCovers);
router.route("/prime").post(bscPortableEquipmentsCoverController.getAllBscPortableEquipmentsCoversPrime);
router.route("/batch-delete").delete(bscPortableEquipmentsCoverController.batchDeleteBscPortableEquipmentsCovers);
/**
 * @swagger
 * /api/v1/bsc-portable-equipments-cover:
 *   post:
 *     description: Endpoint used to create a BSC Portable Equipments Cover.
 *     tags:
 *       - BSC Portable Equipments Cover
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               equipmentType:
 *                 type: string
 *               equipmentDescription:
 *                 type: string
 *               geography:
 *                 type: string
 *               sumInsured:
 *                 type: number
 *               total:
 *                 type: number
 *
 *     response:
 *       200:
 *         description: New BSC Portable Equipments Cover is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscPortableEquipmentsCover'
 */
router.route("/").post(authController.restrictTo(_USER_ROLES.SuperAdmin), bscPortableEquipmentsCoverController.createBscPortableEquipmentsCover);

/**
 * @swagger
 * /api/v1/bsc-portable-equipments-cover/{id}:
 *   get:
 *     description: Fetches the BSC Portable Equipments Cover of specified ID.
 *     tags:
 *       - BSC Portable Equipments Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: BSC Portable Equipments Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscPortableEquipmentsCover'
 */
router.route("/:id").get(bscPortableEquipmentsCoverController.getBscPortableEquipmentsCover);

/**
 * @swagger
 * /api/v1/bsc-portable-equipments-cover/{id}:
 *   patch:
 *     description: Updates the BSC Portable Equipments Cover of specified ID.
 *     tags:
 *       - BSC Portable Equipments Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               equipmentType:
 *                 type: string
 *               equipmentDescription:
 *                 type: string
 *               geography:
 *                 type: string
 *               sumInsured:
 *                 type: number
 *               total:
 *                 type: number
 *     responses:
 *       200:
 *         description: BSC Portable Equipments Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscPortableEquipmentsCover'
 */
router.route("/:id").patch(authController.restrictTo(_USER_ROLES.SuperAdmin), bscPortableEquipmentsCoverController.updateBscPortableEquipmentsCover);

/**
 * @swagger
 * /api/v1/bsc-portable-equipments-cover/{id}:
 *   delete:
 *     description: Deletes the BSC Portable Equipments Cover of specified ID.
 *     tags:
 *       - BSC Portable Equipments Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: BSC Portable Equipments Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscPortableEquipmentsCover'
 */
router.route("/:id").delete(authController.restrictTo(_USER_ROLES.SuperAdmin), bscPortableEquipmentsCoverController.deleteBscPortableEquipmentsCover);
