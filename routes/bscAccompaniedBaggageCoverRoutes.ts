import express from "express";

import bscAccompaniedBaggageCoverController from "../controllers/bscAccompaniedBaggageCoverController";
import authController from "../controllers/authController";
import { _USER_ROLES } from "../models/userModel";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/bsc-accompanied-baggage-cover:
 *   get:
 *     description: Fetches all the BSC accompanied baggage covers.
 *     tags:
 *       - BSC Accompanied Baggage Cover
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: BSC accompanied baggage covers.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscAccompaniedBaggageCover'
 */
router.route("/").get(bscAccompaniedBaggageCoverController.getAllBscAccompaniedBaggageCovers);
router.route("/prime").post(bscAccompaniedBaggageCoverController.getAllbscAccompaniedBaggageCoverPrime);
router.route("/batch-delete").delete(bscAccompaniedBaggageCoverController.batchDeleteBscAccompaniedBaggageCover);
/**
 * @swagger
 * /api/v1/bsc-accompanied-baggage-cover:
 *   post:
 *     description: Endpoint used to create a BSC accompanied baggage cover.
 *     tags:
 *       - BSC Accompanied Baggage Cover
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               baggageType:
 *                 type: string
 *               baggageDescription:
 *                 type: string
 *               sumInsured:
 *                 type: number
 *               total:
 *                 type: number
 *
 *     response:
 *       200:
 *         description: New BSC accompanied baggage cover is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscAccompaniedBaggageCover'
 */
router.route("/").post(authController.restrictTo(_USER_ROLES.SuperAdmin), bscAccompaniedBaggageCoverController.createBscAccompaniedBaggageCover);

/**
 * @swagger
 * /api/v1/bsc-accompanied-baggage-cover/{id}:
 *   get:
 *     description: Fetches the BSC accompanied baggage cover of specified ID.
 *     tags:
 *       - BSC Accompanied Baggage Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: BSC accompanied baggage cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscAccompaniedBaggageCover'
 */
router.route("/:id").get(bscAccompaniedBaggageCoverController.getBscAccompaniedBaggageCover);

/**
 * @swagger
 * /api/v1/bsc-accompanied-baggage-cover/{id}:
 *   patch:
 *     description: Updates the BSC accompanied baggage cover of specified ID.
 *     tags:
 *       - BSC Accompanied Baggage Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               baggageType:
 *                 type: string
 *               baggageDescription:
 *                 type: string
 *               sumInsured:
 *                 type: number
 *               total:
 *                 type: number
 *     responses:
 *       200:
 *         description: BSC accompanied baggage cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscAccompaniedBaggageCover'
 */
router.route("/:id").patch(authController.restrictTo(_USER_ROLES.SuperAdmin), bscAccompaniedBaggageCoverController.updateBscAccompaniedBaggageCover);

/**
 * @swagger
 * /api/v1/bsc-accompanied-baggage-cover/{id}:
 *   delete:
 *     description: Deletes the BSC Accompanied Baggage Cover of specified ID.
 *     tags:
 *       - BSC Accompanied Baggage Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: BSC accompanied baggage cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscAccompaniedBaggageCover'
 */
router.route("/:id").delete(authController.restrictTo(_USER_ROLES.SuperAdmin), bscAccompaniedBaggageCoverController.deleteBscAccompaniedBaggageCover);
