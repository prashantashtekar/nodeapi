import express from "express";

import bscFidelityGuaranteeCoverController from "../controllers/bscFidelityGuaranteeCoverController";
import authController from "../controllers/authController";
import { _USER_ROLES } from "../models/userModel";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/bsc-fidelity-guarantee-cover:
 *   get:
 *     description: Fetches all the BSC Fidelity Gurantee Covers.
 *     tags:
 *       - BSC Fidelity Gurantee Cover
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: BSC Fidelity Gurantee Covers.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscFidelityGuaranteeCover'
 */
router.route("/").get(bscFidelityGuaranteeCoverController.getAllBscFidelityGuaranteeCovers);
router.route("/prime").post(bscFidelityGuaranteeCoverController.getAllBscFidelityGuaranteeCoverPrime);
router.route("/batch-delete").delete(bscFidelityGuaranteeCoverController.batchDeleteBscFidelityGuaranteeCover);
/**
 * @swagger
 * /api/v1/bsc-fidelity-guarantee-cover:
 *   post:
 *     description: Endpoint used to create a BSC Fidelity Gurantee Cover.
 *     tags:
 *       - BSC Fidelity Gurantee Cover
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               riskType:
 *                 type: string
 *               riskDescription:
 *                 type: string
 *               sumInsured:
 *                 type: number
 *               total:
 *                 type: number
 *
 *     response:
 *       200:
 *         description: New BSC Fidelity Gurantee Cover is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscFidelityGuaranteeCover'
 */
router.route("/").post(authController.restrictTo(_USER_ROLES.SuperAdmin), bscFidelityGuaranteeCoverController.createBscFidelityGuaranteeCover);

/**
 * @swagger
 * /api/v1/bsc-fidelity-guarantee-cover/{id}:
 *   get:
 *     description: Fetches the BSC Fidelity Gurantee Cover of specified ID.
 *     tags:
 *       - BSC Fidelity Gurantee Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: BSC Fidelity Gurantee Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscFidelityGuaranteeCover'
 */
router.route("/:id").get(bscFidelityGuaranteeCoverController.getBscFidelityGuaranteeCover);

/**
 * @swagger
 * /api/v1/bsc-fidelity-guarantee-cover/{id}:
 *   patch:
 *     description: Updates the BSC Fidelity Gurantee Cover of specified ID.
 *     tags:
 *       - BSC Fidelity Gurantee Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               riskType:
 *                 type: string
 *               riskDescription:
 *                 type: string
 *               sumInsured:
 *                 type: number
 *               total:
 *                 type: number
 *
 *     responses:
 *       200:
 *         description: BSC Fidelity Gurantee Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscFidelityGuaranteeCover'
 */
router.route("/:id").patch(authController.restrictTo(_USER_ROLES.SuperAdmin), bscFidelityGuaranteeCoverController.updateBscFidelityGuaranteeCover);

/**
 * @swagger
 * /api/v1/bsc-fidelity-guarantee-cover/{id}:
 *   delete:
 *     description: Deletes the BSC Fidelity Gurantee Cover of specified ID.
 *     tags:
 *       - BSC Fidelity Gurantee Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: BSC Fidelity Gurantee Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscFidelityGuaranteeCover'
 */
router.route("/:id").delete(authController.restrictTo(_USER_ROLES.SuperAdmin), bscFidelityGuaranteeCoverController.deleteBscFidelityGuaranteeCover);
