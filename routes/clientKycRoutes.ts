import express from "express";

import clientKycController from "../controllers/clientKycMasterController";
import authController from "../controllers/authController";
import userController from "../controllers/userController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/client-KYCs:
 *   get:
 *     description: Fetches all the Client KYCs.
 *     tags:
 *       - Client KYC
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Client KYCs.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClientKycMaster'
 */
router.route("/").get(clientKycController.getAllClientKycMasters);
router.route("/prime").post(clientKycController.getAllClientKycMastersPrime);
router.route("/batch-delete").delete(clientKycController.batchDeleteClientKycMasters);
/**
 * @swagger
 * /api/v1/client-KYCs:
 *   post:
 *     description: Endpoint used to create a client KYC.
 *     tags:
 *       - Client KYC
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               clientGroupName:
 *                 type: string
 *                 required: true
 *
 *               clientName:
 *                 type: string
 *                 required: true
 *
 *               pan:
 *                 type: string
 *                 required: true
 *
 *               gst:
 *                 required: true
 *                 type: string
 *                 example: abc
 *
 *     response:
 *       200:
 *         description: New client KYC is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClientKycMaster'
 */
router.route("/").post(authController.restrictTo("admin"), clientKycController.createClientKycMaster);

/**
 * @swagger
 * /api/v1/client-KYCs/{id}:
 *   get:
 *     description: Fetches the Client KYC of specified ID.
 *     tags:
 *       - Client KYC
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Client KYC.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClientKycMaster'
 */
router.route("/:id").get(clientKycController.getClientKycMaster);

/**
 * @swagger
 * /api/v1/client-KYCs/{id}:
 *   patch:
 *     description: Endpoint used to update a client KYC.
 *     tags:
 *       - Client KYC
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               clientGroupName:
 *                 type: string
 *                 required: true
 *
 *               clientName:
 *                 type: string
 *                 required: true
 *
 *               pan:
 *                 type: string
 *                 required: true
 *
 *               gst:
 *                 required: true
 *                 type: string
 *                 example: abc
 *
 *     response:
 *       200:
 *         description: Client KYC is updated.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClientKycMaster'
 */
router.route("/:id").patch(authController.restrictTo("admin"), clientKycController.updateClientKycMaster);

/**
 * @swagger
 * /api/v1/client-KYCs/{id}:
 *   delete:
 *     description: Endpoint used to delete a client KYC.
 *     tags:
 *       - Client KYC
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     response:
 *       200:
 *         description: Client KYC is deleted.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClientKycMaster'
 */
router.route("/:id").delete(authController.restrictTo("admin"), clientKycController.deleteClientKycMaster);
router.route("/diff/:id").post(userController.getUserDiffHistory);
