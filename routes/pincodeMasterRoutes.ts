import express from "express";

import pincodeMasterController from "../controllers/pincodeMasterController";
import authController from "../controllers/authController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/pincodes:
 *   get:
 *     description: Fetches all the pincodes.
 *     tags:
 *       - Pincode Master
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Pincodes.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IPincodeMaster'
 */
router.route("/").get(pincodeMasterController.getAllPincodeMasters);
router.route("/prime").post(pincodeMasterController.getAllPincodeMastersPrime);
router.route("/batch-delete").delete(pincodeMasterController.batchDeletePincodeMasters);
/**
 * @swagger
 * /api/v1/pincodes:
 *   post:
 *     description: Endpoint used to create a pincode.
 *     tags:
 *       - Pincode Master
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 required: true
 *               districtId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *               cityId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *               stateId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *               countryId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *               earthquakeZone:
 *                 type: string
 *                 required: true
 *
 *     response:
 *       200:
 *         description: New pincode is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IPincodeMaster'
 */
router.route("/").post(authController.restrictTo("admin"), pincodeMasterController.createPincodeMaster);

/**
 * @swagger
 * /api/v1/pincodes/{id}:
 *   get:
 *     description: Fetches the pincode of specified ID.
 *     tags:
 *       - Pincode Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Pincode.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IPincodeMaster'
 */
router.route("/:id").get(pincodeMasterController.getPincodeMaster);

/**
 * @swagger
 * /api/v1/pincodes/{id}:
 *   patch:
 *     description: Updates the pincode of specified ID.
 *     tags:
 *       - Pincode Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 required: true
 *               districtId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *               cityId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *               stateId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *               countryId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *               earthquakeZone:
 *                 type: string
 *                 required: true
 *     responses:
 *       200:
 *         description: Pincode.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IPincodeMaster'
 */
router.route("/:id").patch(authController.restrictTo("admin"), pincodeMasterController.updatePincodeMaster);

/**
 * @swagger
 * /api/v1/pincodes/{id}:
 *   delete:
 *     description: Deletes the pincode of specified ID.
 *     tags:
 *       - Pincode Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: Pincode.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IPincodeMaster'
 */
router.route("/:id").delete(authController.restrictTo("admin"), pincodeMasterController.deletePincodeMaster);
