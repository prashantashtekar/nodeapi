import express from "express";

import bscMoneyTransitCoverController from "../controllers/bscMoneyTransitCoverController";
import authController from "../controllers/authController";
import { _USER_ROLES } from "../models/userModel";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/bsc-money-transit-cover:
 *   get:
 *     description: Fetches all the BSC Money Transit Covers.
 *     tags:
 *       - BSC Money Transit Cover
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: BSC Money Transit Covers.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscMoneyTransitCover'
 */
router.route("/").get(bscMoneyTransitCoverController.getAllBscMoneyTransitCovers);
router.route("/prime").post(bscMoneyTransitCoverController.getAllBscMoneyTransitCoversPrime);
router.route("/batch-delete").delete(bscMoneyTransitCoverController.batchDeleteBscMoneyTransitCovers);
/**
 * @swagger
 * /api/v1/bsc-money-transit-cover:
 *   post:
 *     description: Endpoint used to create a BSC Money Transit Cover.
 *     tags:
 *       - BSC Money Transit Cover
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               transitFrom:
 *                 type: string
 *               transitTo:
 *                 type: string
 *               singleCarryingLimit:
 *                 type: number
 *               annualTurnover:
 *                 type: number
 *               total:
 *                 type: number
 *
 *     response:
 *       200:
 *         description: New BSC Money Transit Cover is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscMoneyTransitCover'
 */
router.route("/").post(authController.restrictTo(_USER_ROLES.SuperAdmin), bscMoneyTransitCoverController.createBscMoneyTransitCover);

/**
 * @swagger
 * /api/v1/bsc-money-transit-cover/{id}:
 *   get:
 *     description: Fetches the BSC Money Transit Cover of specified ID.
 *     tags:
 *       - BSC Money Transit Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: BSC Money Transit Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscMoneyTransitCover'
 */
router.route("/:id").get(bscMoneyTransitCoverController.getBscMoneyTransitCover);

/**
 * @swagger
 * /api/v1/bsc-money-transit-cover/{id}:
 *   patch:
 *     description: Updates the BSC Money Transit Cover of specified ID.
 *     tags:
 *       - BSC Money Transit Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               transitFrom:
 *                 type: string
 *               transitTo:
 *                 type: string
 *               singleCarryingLimit:
 *                 type: number
 *               annualTurnover:
 *                 type: number
 *               total:
 *                 type: number
 *
 *     responses:
 *       200:
 *         description: BSC Money Transit Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscMoneyTransitCover'
 */
router.route("/:id").patch(authController.restrictTo(_USER_ROLES.SuperAdmin), bscMoneyTransitCoverController.updateBscMoneyTransitCover);

/**
 * @swagger
 * /api/v1/bsc-money-transit-cover/{id}:
 *   delete:
 *     description: Deletes the BSC Money Transit Cover of specified ID.
 *     tags:
 *       - BSC Money Transit Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: BSC Money Transit Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscMoneyTransitCover'
 */
router.route("/:id").delete(authController.restrictTo(_USER_ROLES.SuperAdmin), bscMoneyTransitCoverController.deleteBscMoneyTransitCover);
