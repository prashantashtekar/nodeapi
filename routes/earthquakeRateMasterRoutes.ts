import express from "express";

import earthquakeRateMasterController from "../controllers/earthquakeRateMasterController";
import authController from "../controllers/authController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/earthquake-rate-masters:
 *   get:
 *     description: Fetches all the earthquake rate masters.
 *     tags:
 *       - Earthquake Rate Master
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: earthquake rate masters.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IEarthquakeRateMaster'
 */
router.route("/").get(earthquakeRateMasterController.getAllEarthquakeRateMaster);
router.route("/prime").post(earthquakeRateMasterController.getAllEarthquakeRateMastersPrime);
router.route("/batch-delete").delete(earthquakeRateMasterController.batchDeleteEarthquakeRateMasters);
/**
 * @swagger
 * /api/v1/earthquake-rate-masters:
 *   post:
 *     description: Endpoint used to create a earthquake rate master.
 *     tags:
 *       - Earthquake Rate Master
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               industryType:
 *                 type: string
 *               zone:
 *                 type: string
 *               rate:
 *                 type: number
 *               productId:
 *                 type: Schema.Types.ObjectId
 *               applicableFrom:
 *                 type: date
 *               applicableTo:
 *                 type: date
 *
 *     response:
 *       200:
 *         description: New earthquake rate master is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IEarthquakeRateMaster'
 */
router.route("/").post(authController.restrictTo("admin"), earthquakeRateMasterController.createEarthquakeRateMaster);

/**
 * @swagger
 * /api/v1/earthquake-rate-masters/{id}:
 *   get:
 *     description: Fetches the earthquake rate master of specified ID.
 *     tags:
 *       - Earthquake Rate Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Earthquake Rate master.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IEarthquakeRateMaster'
 */
router.route("/:id").get(earthquakeRateMasterController.getEarthquakeRateMaster);

/**
 * @swagger
 * /api/v1/earthquake-rate-masters/{id}:
 *   patch:
 *     description: Updates the earthquake rate master of specified ID.
 *     tags:
 *       - Earthquake Rate Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               industryType:
 *                 type: string
 *               zone:
 *                 type: string
 *               rate:
 *                 type: number
 *               productId:
 *                 type: Schema.Types.ObjectId
 *               applicableFrom:
 *                 type: date
 *               applicableTo:
 *                 type: date
 *     responses:
 *       200:
 *         description: earthquake rate master.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IEarthquakeRateMaster'
 */
router.route("/:id").patch(authController.restrictTo("admin"), earthquakeRateMasterController.updateEarthquakeRateMaster);

/**
 * @swagger
 * /api/v1/earthquake-rate-masters/{id}:
 *   delete:
 *     description: Deletes the earthquake rate master of specified ID.
 *     tags:
 *       - Earthquake Rate Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: earthquake rate master.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IEarthquakeRateMaster'
 */
router.route("/:id").delete(authController.restrictTo("admin"), earthquakeRateMasterController.deleteEarthquakeRateMaster);
