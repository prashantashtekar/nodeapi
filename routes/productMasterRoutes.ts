import express from "express";

import productMasterController from "../controllers/productMasterController";
import authController from "../controllers/authController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/products:
 *   get:
 *     description: Fetches all the products.
 *     tags:
 *       - Product Master
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Products.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IProductMaster'
 */
router.route("/").get(productMasterController.getAllProducts);
router.route("/prime").post(productMasterController.getAllProductsPrime);
router.route("/batch-delete").delete(productMasterController.batchDeleteProducts);

/**
 * @swagger
 * /api/v1/products:
 *   post:
 *     description: Endpoint used to create a product.
 *     tags:
 *       - Product Master
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               type:
 *                 type: string
 *                 required: true
 *               category:
 *                 type: string
 *                 required: true
 *               status:
 *                 type: boolean
 *                 required: true
 *
 *     response:
 *       200:
 *         description: New product is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IProductMaster'
 */
router.route("/").post(authController.restrictTo("admin"), productMasterController.createProduct);

/**
 * @swagger
 * /api/v1/products/{id}:
 *   get:
 *     description: Fetches the product of specified ID.
 *     tags:
 *       - Product Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Product.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IProductMaster'
 */
router.route("/:id").get(productMasterController.getProduct);

/**
 * @swagger
 * /api/v1/products/{id}:
 *   patch:
 *     description: Updates the product of specified ID.
 *     tags:
 *       - Product Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               type:
 *                 type: string
 *                 required: true
 *               category:
 *                 type: string
 *                 required: true
 *               status:
 *                 type: boolean
 *                 required: true
 *     responses:
 *       200:
 *         description: Product.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IProductMaster'
 */
router.route("/:id").patch(authController.restrictTo("admin"), productMasterController.updateProduct);

/**
 * @swagger
 * /api/v1/products/{id}:
 *   delete:
 *     description: Deletes the product of specified ID.
 *     tags:
 *       - Product Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: Product.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IProductMaster'
 */
router.route("/:id").delete(authController.restrictTo("admin"), productMasterController.deleteProduct);
