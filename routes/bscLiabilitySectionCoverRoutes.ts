import express from "express";

import bscLiabilitySectionCoverController from "../controllers/bscLiabilitySectionCoverController";
import authController from "../controllers/authController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/bsc-liability-section-cover:
 *   get:
 *     description: Fetches all the BSC Liability Section Covers.
 *     tags:
 *       - BSC Liability Section Cover
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: BSC Liability Section Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscLiabilitySectionCover'
 */
router.route("/").get(bscLiabilitySectionCoverController.getAllBscLiabilitySectionCovers);

router.route("/prime").post(bscLiabilitySectionCoverController.getAllBscLiabilitySectionCoversPrime);

router.route("/batch-delete").delete(bscLiabilitySectionCoverController.batchDeleteBscLiabilitySectionCovers);
/**
 * @swagger
 * /api/v1/bsc-liability-section-cover:
 *   post:
 *     description: Endpoint used to create a BSC Liability Section Cover.
 *     tags:
 *       - BSC Liability Section Cover
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               riskType:
 *                 type: string
 *               riskDescription:
 *                 type: string
 *               sumInsured:
 *                 type: number
 *               total:
 *                 type: number
 *
 *     response:
 *       200:
 *         description: New BSC Liability Section Cover is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscLiabilitySectionCover'
 */
router.route("/").post(authController.restrictTo("admin"), bscLiabilitySectionCoverController.createBscLiabilitySectionCover);

/**
 * @swagger
 * /api/v1/bsc-liability-section-cover/{id}:
 *   get:
 *     description: Fetches the BSC Liability Section Cover of specified ID.
 *     tags:
 *       - BSC Liability Section Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: BSC Liability Section Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscLiabilitySectionCover'
 */
router.route("/:id").get(bscLiabilitySectionCoverController.getBscLiabilitySectionCover);

/**
 * @swagger
 * /api/v1/bsc-liability-section-cover/{id}:
 *   patch:
 *     description: Updates the BSC Liability Section Cover of specified ID.
 *     tags:
 *       - BSC Liability Section Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               riskType:
 *                 type: string
 *               riskDescription:
 *                 type: string
 *               sumInsured:
 *                 type: number
 *               total:
 *                 type: number
 *
 *     responses:
 *       200:
 *         description: BSC Liability Section Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscLiabilitySectionCover'
 */
router.route("/:id").patch(authController.restrictTo("admin"), bscLiabilitySectionCoverController.updateBscLiabilitySectionCover);

/**
 * @swagger
 * /api/v1/bsc-liability-section-cover/{id}:
 *   delete:
 *     description: Deletes the BSC Liability Section Cover of specified ID.
 *     tags:
 *       - BSC Liability Section Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: BSC Liability Section Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscLiabilitySectionCover'
 */
router.route("/:id").delete(authController.restrictTo("admin"), bscLiabilitySectionCoverController.deleteBscLiabilitySectionCover);
