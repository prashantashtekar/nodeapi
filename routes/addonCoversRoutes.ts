import express from "express";
import addonCoversController from "../controllers/addonCoversController";

import addOnCoversController from "../controllers/addonCoversController";
import authController from "../controllers/authController";
import { _USER_ROLES } from "../models/userModel";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/addOn-covers:
 *   get:
 *     description: Fetches all the AddOn Covers.
 *     tags:
 *       - Addon Cover
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: AddOn Covers.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IAddOnCover'
 */
router.route("/").get(addOnCoversController.getAllAddOnCovers);
router.route("/prime").post(addonCoversController.getAllAddOnCoversPrime);

router.route("/batch-delete").delete(addonCoversController.batchDeleteAddOnCovers);
/**
 * @swagger
 * /api/v1/addOn-covers:
 *   post:
 *     description: Endpoint used to create a AddOn Cover.
 *     tags:
 *       - Addon Cover
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               category:
 *                 type: string
 *               subCategory:
 *                 type: string
 *               name:
 *                 type: string
 *               isFree:
 *                 type: boolean
 *               isTariff:
 *                 type: boolean
 *               claimClause:
 *                 type: string
 *               monthOrDays:
 *                 type: boolean
 *               siLimit:
 *                 type: boolean
 *               rate:
 *                 type: number
 *               categoryOfImportance:
 *                 type: string
 *               sequenceNo:
 *                 type: string
 *               policyType:
 *                 type: string
 *               addOnType:
 *                 type: string
 *               sumInsured:
 *                 type: number
 *               isAddonSelected:
 *                 type: boolean
 *               rateType:
 *                 type: string
 *               rateParameter:
 *                 type: string
 *               freeUpToFlag:
 *                 type: string
 *               freeUpToParameter:
 *                 type: string
 *               productId:
 *                 type: Schema.Types.ObjectId
 *               calculatedIndicativePremium:
 *                 type: number
 *
 *     response:
 *       200:
 *         description: New AddOn Cover is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IAddOnCover'
 */
router.route("/").post(authController.restrictTo(_USER_ROLES.SuperAdmin, _USER_ROLES.Creator, _USER_ROLES.Approver), addOnCoversController.createAddOnCover);

/**
 * @swagger
 * /api/v1/addOn-covers/{id}:
 *   get:
 *     description: Fetches the AddOn Cover of specified ID.
 *     tags:
 *       - Addon Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: AddOn Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IAddOnCover'
 */
router.route("/:id").get(addOnCoversController.getAddOnCover);

/**
 * @swagger
 * /api/v1/addOn-covers/{id}:
 *   patch:
 *     description: Updates the AddOn Cover of specified ID.
 *     tags:
 *       - Addon Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               category:
 *                 type: string
 *               subCategory:
 *                 type: string
 *               name:
 *                 type: string
 *               isFree:
 *                 type: boolean
 *               isTariff:
 *                 type: boolean
 *               claimClause:
 *                 type: string
 *               monthOrDays:
 *                 type: boolean
 *               siLimit:
 *                 type: boolean
 *               rate:
 *                 type: number
 *               categoryOfImportance:
 *                 type: string
 *               sequenceNo:
 *                 type: string
 *               policyType:
 *                 type: string
 *               addOnType:
 *                 type: string
 *               sumInsured:
 *                 type: number
 *               isAddonSelected:
 *                 type: boolean
 *               rateType:
 *                 type: string
 *               rateParameter:
 *                 type: string
 *               freeUpToFlag:
 *                 type: string
 *               freeUpToParameter:
 *                 type: string
 *               productId:
 *                 type: Schema.Types.ObjectId
 *               calculatedIndicativePremium:
 *                 type: number
 *     responses:
 *       200:
 *         description: AddOn Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IAddOnCover'
 */
router.route("/:id").patch(authController.restrictTo(_USER_ROLES.SuperAdmin), addOnCoversController.updateAddOnCover);

/**
 * @swagger
 * /api/v1/addOn-covers/{id}:
 *   delete:
 *     description: Deletes the AddOn Cover of specified ID.
 *     tags:
 *       - Addon Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: AddOn Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IAddOnCover'
 */
router.route("/:id").delete(authController.restrictTo(_USER_ROLES.SuperAdmin), addOnCoversController.deleteAddOnCover);
