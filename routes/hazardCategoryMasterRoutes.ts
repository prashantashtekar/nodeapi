import express from "express";

import hazardCategoryMasterController from "../controllers/hazardCategoryMasterController";
import authController from "../controllers/authController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/hazard-categories:
 *   get:
 *     description: Fetches all the hazard-categories.
 *     tags:
 *       - Hazard Category Master
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Hazard-categories.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IHazardCategoryMaster'
 */
router.route("/").get(hazardCategoryMasterController.getAllHazardCategoryMasters);
router.route("/prime").post(hazardCategoryMasterController.getAllHazardCategoryMastersPrime);
router.route("/batch-delete").delete(hazardCategoryMasterController.batchDeleteHazardCategoryMasters);
/**
 * @swagger
 * /api/v1/hazard-categories:
 *   post:
 *     description: Endpoint used to create a hazard-category.
 *     tags:
 *       - Hazard Category Master
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               category:
 *                 type: string
 *                 required: true
 *               score:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *               status:
 *                 type: boolean
 *                 required: true
 *
 *     response:
 *       200:
 *         description: New hazard-category is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IHazardCategoryMaster'
 */
router.route("/").post(authController.restrictTo("admin"), hazardCategoryMasterController.createHazardCategoryMaster);

/**
 * @swagger
 * /api/v1/hazard-categories/{id}:
 *   get:
 *     description: Fetches the hazard-category of specified ID.
 *     tags:
 *       - Hazard Category Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Hazard Category.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IHazardCategoryMaster'
 */
router.route("/:id").get(hazardCategoryMasterController.getHazardCategoryMaster);

/**
 * @swagger
 * /api/v1/hazard-categories/{id}:
 *   patch:
 *     description: Updates the hazard-categories of specified ID.
 *     tags:
 *       - Hazard Category Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               category:
 *                 type: string
 *                 required: true
 *               score:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *               status:
 *                 type: boolean
 *                 required: true
 *     responses:
 *       200:
 *         description: hazard-categories.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IHazardCategoryMaster'
 */
router.route("/:id").patch(authController.restrictTo("admin"), hazardCategoryMasterController.updateHazardCategoryMaster);

/**
 * @swagger
 * /api/v1/hazard-categories/{id}:
 *   delete:
 *     description: Deletes the hazard-category of specified ID.
 *     tags:
 *       - Hazard Category Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: hazard-category.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IHazardCategoryMaster'
 */
router.route("/:id").delete(authController.restrictTo("admin"), hazardCategoryMasterController.deleteHazardCategoryMaster);
