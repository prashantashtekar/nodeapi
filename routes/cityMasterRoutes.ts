import express from "express";

import cityMasterController from "../controllers/cityMasterController";
import authController from "../controllers/authController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/cities:
 *   get:
 *     description: Endpoint to get all cities.
 *     tags:
 *       - City
 *
 *     responses:
 *       200:
 *         description: Get all cities.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ICityMaster'
 */
router.route("/").get(cityMasterController.getAllCityMasters);
router.route("/prime").post(cityMasterController.getAllCityMastersPrime);
router.route("/batch-delete").delete(cityMasterController.batchDeleteCityMasters);
/**
 * @swagger
 * /api/v1/cities:
 *   post:
 *     description: Endpoint to create a city.
 *     tags:
 *       - City
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *
 *             properties:
 *               name:
 *                 type: string
 *                 description: City name
 *                 required: true
 *                 example: Mumbai
 *
 *               stateId:
 *                 type: string
 *                 required: true
 *
 *
 *     responses:
 *       200:
 *         description: City that was just created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ICityMaster'
 */
router.route("/").post(authController.restrictTo("admin"), cityMasterController.createCityMaster);

/**
 * @swagger
 * /api/v1/cities/{id}:
 *   get:
 *     description: Endpoint to get a city.
 *     tags:
 *       - City
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *
 *     responses:
 *       200:
 *         description: City with specified ID.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ICityMaster'
 */
router.route("/:id").get(cityMasterController.getCityMaster);

/**
 * @swagger
 * /api/v1/cities/{id}:
 *   patch:
 *     description: Endpoint to update a city.
 *     tags:
 *       - City
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *
 *             properties:
 *               name:
 *                 type: string
 *                 description: City name
 *                 required: true
 *                 example: Mumbai
 *
 *               stateId:
 *                 type: string
 *                 required: true
 *
 *
 *     responses:
 *       200:
 *         description: City that was just updated.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ICityMaster'
 */
router.route("/:id").patch(authController.restrictTo("admin"), cityMasterController.updateCityMaster);

/**
 * @swagger
 * /api/v1/cities/{id}:
 *   delete:
 *     description: Endpoint to delete a city.
 *     tags:
 *       - City
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *
 *     responses:
 *       200:
 *         description: City with specified ID deleted.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ICityMaster'
 */
router.route("/:id").delete(authController.restrictTo("admin"), cityMasterController.deleteCityMaster);
