import express from "express";

import buggyController from "../controllers/buggyController";
import authController from "../controllers/authController";

import { _USER_ROLES } from "../models/userModel";

export const router = express.Router();

router.use(authController.protect);

router.route("/notfound").get(buggyController.notFound);
router.route("/servererror").get(buggyController.serverError);
router.route("/badrequest").get(buggyController.badRequest);
