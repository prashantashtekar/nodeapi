import express from "express";

import clientController from "../controllers/clientController";
import authController from "../controllers/authController";
import { _USER_ROLES } from "../models/userModel";
import userController from "../controllers/userController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/clients:
 *   get:
 *     description: Fetches all the clients.
 *     tags:
 *       - Client
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: clients.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClient'
 */
router.route("/").get(clientController.getAllClients);
router.route("/prime").post(clientController.getAllClientsPrime);
router.route("/batch-delete").delete(clientController.batchDeleteClients);
/**
 * @swagger
 * /api/v1/clients:
 *   post:
 *     description: Endpoint used to create a client.
 *     tags:
 *       - Client
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               clientType:
 *                 type: string
 *               name:
 *                 type: string
 *               shortName:
 *                 type: string
 *               active:
 *                 type: boolean
 *               clientGroupId:
 *                 type: Schema.Types.ObjectId
 *               pan:
 *                 type: string
 *               copyOfPan:
 *                 type: string
 *               vin:
 *                 type: string
 *               leadGenerator:
 *                 type: string
 *               natureOfBusiness:
 *                 type: string
 *               creationDate:
 *                 type: date
 *               claimsManager:
 *                 type: string
 *               referralRM:
 *                 type: string
 *               referredCompany:
 *                 type: string
 *               employeeStrength:
 *                 type: string
 *               sameAddressVerification:
 *                 type: boolean
 *               contactPerson:
 *                 type: string
 *               designation:
 *                 type: string
 *               phone:
 *                 type: string
 *               email:
 *                 type: string
 *               mobile:
 *                 type: string
 *               address:
 *                 type: string
 *               stateId:
 *                 type: Schema.Types.ObjectId
 *               cityId:
 *                 type: Schema.Types.ObjectId
 *               pincodeId:
 *                 type: Schema.Types.ObjectId
 *               visitingCard:
 *                 type: string
 *               narration:
 *                 type: string
 *               clientKycMasterId:
 *                 type: Schema.Types.ObjectId
 *
 *     response:
 *       200:
 *         description: New client is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClient'
 */
router.route("/").post(authController.restrictTo("admin"), clientController.createClient);

/**
 * @swagger
 * /api/v1/clients/{id}:
 *   get:
 *     description: Fetches the client of specified ID.
 *     tags:
 *       - Client
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: client.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClient'
 */
router.route("/:id").get(clientController.getClient);

/**
 * @swagger
 * /api/v1/clients/{id}:
 *   patch:
 *     description: Updates the client of specified ID.
 *     tags:
 *       - Client
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               clientType:
 *                 type: string
 *               name:
 *                 type: string
 *               shortName:
 *                 type: string
 *               active:
 *                 type: boolean
 *               clientGroupId:
 *                 type: Schema.Types.ObjectId
 *               pan:
 *                 type: string
 *               copyOfPan:
 *                 type: string
 *               vin:
 *                 type: string
 *               leadGenerator:
 *                 type: string
 *               natureOfBusiness:
 *                 type: string
 *               creationDate:
 *                 type: date
 *               claimsManager:
 *                 type: string
 *               referralRM:
 *                 type: string
 *               referredCompany:
 *                 type: string
 *               employeeStrength:
 *                 type: string
 *               sameAddressVerification:
 *                 type: boolean
 *               contactPerson:
 *                 type: string
 *               designation:
 *                 type: string
 *               phone:
 *                 type: string
 *               email:
 *                 type: string
 *               mobile:
 *                 type: string
 *               address:
 *                 type: string
 *               stateId:
 *                 type: Schema.Types.ObjectId
 *               cityId:
 *                 type: Schema.Types.ObjectId
 *               pincodeId:
 *                 type: Schema.Types.ObjectId
 *               visitingCard:
 *                 type: string
 *               narration:
 *                 type: string
 *               clientKycMasterId:
 *                 type: Schema.Types.ObjectId
 *
 *     responses:
 *       200:
 *         description: client.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClient'
 */
router.route("/:id").patch(authController.restrictTo("admin"), clientController.updateClient);

/**
 * @swagger
 * /api/v1/clients/{id}:
 *   delete:
 *     description: Deletes the client of specified ID.
 *     tags:
 *       - Client
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: Client.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClient'
 */
router.route("/:id").delete(authController.restrictTo("admin"), clientController.deleteClient);

/**
 * @swagger
 * /api/v1/clients/match:
 *   get:
 *     description: Fetches the client.
 *     tags:
 *       - Client
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Client.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClient'
 */
router.route("/match").get(authController.restrictTo("admin", _USER_ROLES.Creator, _USER_ROLES.insurer_admin), clientController.getMatchingClients);

/**
 * @swagger
 * /api/v1/clients/resolve:
 *   get:
 *     description:
 *     tags:
 *       - Client
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Client.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClient'
 */
router.route("/resolve").post(authController.restrictTo("admin", _USER_ROLES.Creator, _USER_ROLES.insurer_admin), clientController.resolveClient);
router.route("/diff/:id").post(userController.getUserDiffHistory);
