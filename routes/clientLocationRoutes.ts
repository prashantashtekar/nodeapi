import express from "express";

import clientLocationController from "../controllers/clientLocationController";
import authController from "../controllers/authController";
import userController from "../controllers/userController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/client-locations:
 *   get:
 *     description: Fetches all the Client Locations.
 *     tags:
 *       - Client Locations
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Client-Locations.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClientLocation'
 */
router.route("/").get(clientLocationController.getAllClientLocations);
router.route("/prime").post(clientLocationController.getAllClientLocationsPrime);
router.route("/batch-delete").delete(clientLocationController.batchDeleteClientLocations);
/**
 * @swagger
 * /api/v1/client-locations:
 *   post:
 *     description: Endpoint used to create a client location.
 *     tags:
 *       - Client Locations
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               address:
 *                 type: string
 *                 required: true
 *
 *               cityId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *
 *               stateId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *
 *               pincodeId:
 *                 required: Schema.Types.ObjectId
 *                 type: string
 *                 example: abc
 *
 *               locationName:
 *                 type: string
 *                 example: abc
 *
 *               clientId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *
 *     response:
 *       200:
 *         description: New client location is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClientLocation'
 */
router.route("/").post(authController.restrictTo("admin"), clientLocationController.createClientLocation);

/**
 * @swagger
 * /api/v1/client-locations/{id}:
 *   get:
 *     description: Fetches the Client Location of specified ID.
 *     tags:
 *       - Client Locations
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Client Location.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClientLocation'
 */
router.route("/:id").get(clientLocationController.getClientLocation);

/**
 * @swagger
 * /api/v1/client-locations:
 *   patch:
 *     description: Endpoint used to create a client location.
 *     tags:
 *       - Client Locations
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               address:
 *                 type: string
 *                 required: true
 *
 *               cityId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *
 *               stateId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *
 *               pincodeId:
 *                 required: Schema.Types.ObjectId
 *                 type: string
 *                 example: abc
 *
 *               locationName:
 *                 type: string
 *                 example: abc
 *
 *               clientId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *
 *     response:
 *       200:
 *         description: New client location is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClientLocation'
 */
router.route("/").patch(authController.restrictTo("admin"), clientLocationController.updateClientLocation);

/**
 * @swagger
 * /api/v1/client-locations/{id}:
 *   delete:
 *     description: Fetches the Client Location of specified ID.
 *     tags:
 *       - Client Locations
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: Client Location.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IClientLocation'
 */
router.route("/").delete(authController.restrictTo("admin"), clientLocationController.deleteClientLocation);
router.route("/diff/:id").post(userController.getUserDiffHistory);
