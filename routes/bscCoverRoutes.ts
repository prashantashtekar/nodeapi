import express from "express";

import BscCoverController from "../controllers/bscCoverController";
import authController from "../controllers/authController";
import { _USER_ROLES } from "../models/userModel";
import bscCoverController from "../controllers/bscCoverController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/bsc-cover:
 *   get:
 *     description: Fetches all the BSC covers.
 *     tags:
 *       - BSC Cover
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: BSC covers.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscCover'
 */
router.route("/").get(BscCoverController.getAllBscCovers);
router.route("/prime").post(bscCoverController.getALlBscCoverPrime);
router.route("/batch-delete").delete(bscCoverController.batchDeleteBscCover);
/**
 * @swagger
 * /api/v1/bsc-cover:
 *   post:
 *     description: Endpoint used to create a BSC cover.
 *     tags:
 *       - BSC Cover
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               bscType:
 *                 type: string
 *               fromSI:
 *                 type: number
 *               toSI:
 *                 type: number
 *               ratePerMile:
 *                 type: number
 *               productId:
 *                 type: Schema.Types.ObjectId
 *               applicableFrom:
 *                  type: date
 *               applicableTo:
 *                  type: date
 *
 *     response:
 *       200:
 *         description: New BSC cover is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscCover'
 */
router.route("/").post(authController.restrictTo(_USER_ROLES.SuperAdmin), BscCoverController.createBscCover);

/**
 * @swagger
 * /api/v1/bsc-cover/{id}:
 *   get:
 *     description: Fetches the BSC cover of specified ID.
 *     tags:
 *       - BSC Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: BSC cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscCover'
 */
router.route("/:id").get(BscCoverController.getBscCover);

/**
 * @swagger
 * /api/v1/bsc-cover/{id}:
 *   patch:
 *     description: Updates the BSC cover of specified ID.
 *     tags:
 *       - BSC Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               bscType:
 *                 type: string
 *               fromSI:
 *                 type: number
 *               toSI:
 *                 type: number
 *               ratePerMile:
 *                 type: number
 *               productId:
 *                 type: Schema.Types.ObjectId
 *               applicableFrom:
 *                  type: date
 *               applicableTo:
 *                  type: date
 *
 *     responses:
 *       200:
 *         description: BSC cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscCover'
 */
router.route("/:id").patch(authController.restrictTo(_USER_ROLES.SuperAdmin), BscCoverController.updateBscCover);

/**
 * @swagger
 * /api/v1/bsc-cover/{id}:
 *   delete:
 *     description: Deletes the BSC cover of specified ID.
 *     tags:
 *       - BSC Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: BSC cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscCover'
 */
router.route("/:id").delete(authController.restrictTo(_USER_ROLES.SuperAdmin), BscCoverController.deleteBscCover);
