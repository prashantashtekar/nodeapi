import express from "express";

import bscFixedPlateGlassCoverController from "../controllers/bscFixedPlateGlassCoverController";
import authController from "../controllers/authController";
import { _USER_ROLES } from "../models/userModel";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/bsc-fixed-plate-glass-cover:
 *   get:
 *     description: Fetches all the BSC Fixed Plate Glass Covers.
 *     tags:
 *       - BSC Fixed Plate Glass Cover
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: BSC Fixed Plate Glass Covers.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscFixedPlateGlassCover'
 */
router.route("/").get(bscFixedPlateGlassCoverController.getAllBscFixedPlateGlassCovers);
router.route("/prime").post(bscFixedPlateGlassCoverController.getAllBscFixedPlateGlassCoverPrime);
router.route("/batch-delete").delete(bscFixedPlateGlassCoverController.batchDeleteBscFixedPlateGlassCover);

/**
 * @swagger
 * /api/v1/bsc-fixed-plate-glass-cover:
 *   post:
 *     description: Endpoint used to create a BSC Fixed Plate Glass Cover.
 *     tags:
 *       - BSC Fixed Plate Glass Cover
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               locationId:
 *                 type: Schema.Types.ObjectId
 *               plateGlassType:
 *                 type: string
 *               description:
 *                 type: string
 *               sumInsured:
 *                 type: number
 *               total:
 *                 type: number
 *
 *     response:
 *       200:
 *         description: New BSC Fixed Plate Glass Cover is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscFixedPlateGlassCover'
 */
router.route("/").post(authController.restrictTo(_USER_ROLES.SuperAdmin), bscFixedPlateGlassCoverController.createBscFixedPlateGlassCover);

/**
 * @swagger
 * /api/v1/bsc-fixed-plate-glass-cover/{id}:
 *   get:
 *     description: Fetches the BSC Fixed Plate Glass Cover of specified ID.
 *     tags:
 *       - BSC Fixed Plate Glass Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: BSC Fixed Plate Glass Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscFixedPlateGlassCover'
 */
router.route("/:id").get(bscFixedPlateGlassCoverController.getBscFixedPlateGlassCover);

/**
 * @swagger
 * /api/v1/bsc-fixed-plate-glass-cover/{id}:
 *   patch:
 *     description: Updates the BSC Fixed Plate Glass Cover of specified ID.
 *     tags:
 *       - BSC Fixed Plate Glass Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               locationId:
 *                 type: Schema.Types.ObjectId
 *               plateGlassType:
 *                 type: string
 *               description:
 *                 type: string
 *               sumInsured:
 *                 type: number
 *               total:
 *                 type: number
 *
 *     responses:
 *       200:
 *         description: BSC Fixed Plate Glass Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscFixedPlateGlassCover'
 */
router.route("/:id").patch(authController.restrictTo(_USER_ROLES.SuperAdmin), bscFixedPlateGlassCoverController.updateBscFixedPlateGlassCover);

/**
 * @swagger
 * /api/v1/bsc-fixed-plate-glass-cover/{id}:
 *   delete:
 *     description: Deletes the BSC Fixed Plate Glass Cover of specified ID.
 *     tags:
 *       - BSC Fixed Plate Glass Cover
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: BSC Fixed Plate Glass Cover.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IBscFixedPlateGlassCover'
 */
router.route("/:id").delete(authController.restrictTo(_USER_ROLES.SuperAdmin), bscFixedPlateGlassCoverController.deleteBscFixedPlateGlassCover);
