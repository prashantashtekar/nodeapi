import express from "express";

import stateMasterController from "../controllers/stateMasterController";
import authController from "../controllers/authController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/states:
 *   get:
 *     description: Fetches all the states.
 *     tags:
 *       - State Master
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: States.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IStateMaster'
 */
router.route("/").get(stateMasterController.getAllStateMasters);
router.route("/prime").post(stateMasterController.getAllStateMastersPrime);
router.route("/batch-delete").delete(stateMasterController.batchDeleteStateMasters);
/**
 * @swagger
 * /api/v1/states:
 *   post:
 *     description: Endpoint used to create a state.
 *     tags:
 *       - State Master
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 required: true
 *
 *               countryId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *
 *     response:
 *       200:
 *         description: New state is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IStateMaster'
 */
router.route("/").post(authController.restrictTo("admin"), stateMasterController.createStateMaster);

/**
 * @swagger
 * /api/v1/states/{id}:
 *   get:
 *     description: Fetches the state of specified ID.
 *     tags:
 *       - State Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: State.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IStateMaster'
 */
router.route("/:id").get(stateMasterController.getStateMaster);

/**
 * @swagger
 * /api/v1/states/{id}:
 *   patch:
 *     description: Updates the state of specified ID.
 *     tags:
 *       - State Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 required: true
 *
 *               countryId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *     responses:
 *       200:
 *         description: State.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IStateMaster'
 */
router.route("/:id").patch(authController.restrictTo("admin"), stateMasterController.updateStateMaster);

/**
 * @swagger
 * /api/v1/states/{id}:
 *   delete:
 *     description: Deletes the state of specified ID.
 *     tags:
 *       - State Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: State.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IStateMaster'
 */
router.route("/:id").delete(authController.restrictTo("admin"), stateMasterController.deleteStateMaster);
