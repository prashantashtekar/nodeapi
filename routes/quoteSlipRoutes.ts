import express from "express";

import quoteSlipController from "../controllers/quoteSlipController";
import authController from "../controllers/authController";
import { _USER_ROLES } from "../models/userModel";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/quotes:
 *   get:
 *     description: Fetches all the quote slips.
 *     tags:
 *       - Quote slips
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Quote slips.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IQuoteSlip'
 */
router.route("/").get(quoteSlipController.getAllQuoteSlip);
router.route("/prime").post(quoteSlipController.getAllQuoteSlipsPrime);
router.route("/batch-delete").delete(quoteSlipController.batchDeleteQuoteSlips);
/**
 * @swagger
 * /api/v1/quotes:
 *   post:
 *     description: Endpoint used to create a quote slip.
 *     tags:
 *       - Quote slips
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               quoteNo:
 *                 type: string
 *               quoteType:
 *                 type: Schema.Types.ObjectId
 *               renewalPolicyPeriod:
 *                 type: string
 *               insurredBusiness:
 *                 type: string
 *               status:
 *                 type: string
 *               deductiblesExcess:
 *                 type: string
 *               existingBrokerCurrentYear:
 *                 type: string
 *               otherTerms:
 *                 type: string
 *               additionalInfo:
 *                 type: string
 *               claim1NoOfClaims:
 *                 type: string
 *               claim1Nature:
 *                 type: string
 *               approvedBy:
 *                 type: string
 *               approvedOn:
 *                 type: string
 *               createdBy:
 *                 type: string
 *               createdOn:
 *                 type: string
 *               clientAddress:
 *                 type: string
 *               totalIndictiveQuoteAmt:
 *                 type: number
 *               brokerages:
 *                 type: number
 *               targetPremium:
 *                 type: number
 *               claimYear1:
 *                 type: number
 *               claimToYear1:
 *                 type: number
 *               claim1PremiumPaid:
 *                 type: number
 *               claim1ClaimAmount:
 *                 type: number
 *               revNo:
 *                 type: number
 *               sameAsPremium:
 *                 type: boolean
 *               quoteSubmissionDate:
 *                 type: date
 *               sectorId:
 *                 type: Schema.Types.ObjectId
 *               clientId:
 *                 type: Schema.Types.ObjectId
 *               productId:
 *                 type: Schema.Types.ObjectId
 *
 *     response:
 *       200:
 *         description: New Quote slip is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IQuoteSlip'
 */
router.route("/").post(authController.restrictTo(_USER_ROLES.SuperAdmin, _USER_ROLES.Creator), quoteSlipController.createQuoteSlip);

/**
 * @swagger
 * /api/v1/quotes/{id}:
 *   get:
 *     description: Fetches the Quote slip of specified ID.
 *     tags:
 *       - Quote slips
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Quote slips.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IQuoteSlip'
 */
router.route("/:id").get(quoteSlipController.getQuoteSlip);

/**
 * @swagger
 * /api/v1/quotes/{id}:
 *   patch:
 *     description: Updates the Quote slip of specified ID.
 *     tags:
 *       - Quote slips
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               quoteNo:
 *                 type: string
 *               quoteType:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *               renewalPolicyPeriod:
 *                 type: string
 *               insurredBusiness:
 *                 type: string
 *               status:
 *                 type: string
 *               deductiblesExcess:
 *                 type: string
 *               existingBrokerCurrentYear:
 *                 type: string
 *               otherTerms:
 *                 type: string
 *               additionalInfo:
 *                 type: string
 *               claim1NoOfClaims:
 *                 type: string
 *               claim1Nature:
 *                 type: string
 *               approvedBy:
 *                 type: string
 *               approvedOn:
 *                 type: string
 *               createdBy:
 *                 type: string
 *               createdOn:
 *                 type: string
 *               clientAddress:
 *                 type: string
 *               totalIndictiveQuoteAmt:
 *                 type: number
 *               brokerages:
 *                 type: number
 *               targetPremium:
 *                 type: number
 *               claimYear1:
 *                 type: number
 *               claimToYear1:
 *                 type: number
 *               claim1PremiumPaid:
 *                 type: number
 *               claim1ClaimAmount:
 *                 type: number
 *               revNo:
 *                 type: number
 *               sameAsPremium:
 *                 type: boolean
 *               quoteSubmissionDate:
 *                 type: date
 *               sectorId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *               clientId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *               productId:
 *                 type: Schema.Types.ObjectId
 *     responses:
 *       200:
 *         description: Quote slips.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IQuoteSlip'
 */
router.route("/:id").patch(authController.restrictTo(_USER_ROLES.SuperAdmin, _USER_ROLES.Creator), quoteSlipController.updateQuoteSlip);

/**
 * @swagger
 * /api/v1/quotes/{id}:
 *   delete:
 *     description: Deletes the Quote slip of specified ID.
 *     tags:
 *       - Quote slips
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: quote slip.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IQuoteSlip'
 */
router.route("/:id").delete(authController.restrictTo(_USER_ROLES.SuperAdmin, _USER_ROLES.Creator), quoteSlipController.deleteQuoteSlip);

/**
 * @swagger
 * /api/v1/quotes/bulk-import-sample/{quoteId}:
 *   get:
 *     description: Fetches all the quote slips.
 *     tags:
 *       - Quote slips
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: quoteId
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Quote slips.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IQuoteSlip'
 */
router
  .route("/bulk-import-sample/:quoteId")
  .get(authController.restrictTo(_USER_ROLES.SuperAdmin, _USER_ROLES.Creator, _USER_ROLES.Approver), quoteSlipController.downloadClientLocationBulkImportExcel);
