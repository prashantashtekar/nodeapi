import express from "express";

import districtMasterController from "../controllers/districtMasterController";
import authController from "../controllers/authController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/districts:
 *   get:
 *     description: Fetches all the districts.
 *     tags:
 *       - District Master
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: States.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IDistrictMaster'
 */
router.route("/").get(districtMasterController.getAllDistrictMasters);
router.route("/prime").post(districtMasterController.getAllDistrictMastersPrime);

router.route("/batch-delete").delete(districtMasterController.batchDeleteDistrictMasters);
/**
 * @swagger
 * /api/v1/districts:
 *   post:
 *     description: Endpoint used to create a district.
 *     tags:
 *       - District Master
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 required: true
 *
 *               stateId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *
 *     response:
 *       200:
 *         description: New district is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IDistrictMaster'
 */
router.route("/").post(authController.restrictTo("admin"), districtMasterController.createDistrictMaster);

/**
 * @swagger
 * /api/v1/districts/{id}:
 *   get:
 *     description: Fetches the district of specified ID.
 *     tags:
 *       - District Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: District.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IDistrictMaster'
 */
router.route("/:id").get(districtMasterController.getDistrictMaster);

/**
 * @swagger
 * /api/v1/districts/{id}:
 *   patch:
 *     description: Updates the district of specified ID.
 *     tags:
 *       - District Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 required: true
 *
 *               stateId:
 *                 type: Schema.Types.ObjectId
 *                 required: true
 *     responses:
 *       200:
 *         description: District.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IDistrictMaster'
 */
router.route("/:id").patch(authController.restrictTo("admin"), districtMasterController.updateDistrictMaster);

/**
 * @swagger
 * /api/v1/districts/{id}:
 *   delete:
 *     description: Deletes the district of specified ID.
 *     tags:
 *       - District Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: District.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IDistrictMaster'
 */
router.route("/:id").delete(authController.restrictTo("admin"), districtMasterController.deleteDistrictMaster);
