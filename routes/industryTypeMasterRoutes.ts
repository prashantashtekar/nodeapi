import express from "express";

import industryTypeMasterController from "../controllers/industryMasterController";
import authController from "../controllers/authController";

// By default each router only has access to url params of their own route.
// using the below we are able to access the parameters defined in the tour router, here also.
export const router = express.Router({ mergeParams: true });

// Adding authentication here, all routes below this are now authenticated.
router.use(authController.protect);

/**
 * @swagger
 * /api/v1/industryTypes:
 *   get:
 *     description: Fetches all the industry types.
 *     tags:
 *       - Industry Type Master
 *     security:
 *       - bearerAuth: []
 *     responses:
 *       200:
 *         description: Industry Types.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IIndustryTypeMaster'
 */
router.route("/").get(industryTypeMasterController.getAllIndustryTypeMasters);
router.route("/prime").post(industryTypeMasterController.getAllIndustryTypeMastersPrime);
router.route("/batch-delete").delete(industryTypeMasterController.batchDeleteIndustryTypeMasters);
/**
 * @swagger
 * /api/v1/industryTypes:
 *   post:
 *     description: Endpoint used to create a industry type.
 *     tags:
 *       - Industry Type Master
 *     security:
 *       - bearerAuth: []
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               industryTypeName:
 *                 type: string
 *                 required: true
 *
 *     response:
 *       200:
 *         description: New industry type is created.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IIndustryTypeMaster'
 */
router.route("/").post(authController.restrictTo("admin"), industryTypeMasterController.createIndustryTypeMaster);

/**
 * @swagger
 * /api/v1/industryTypes/{id}:
 *   get:
 *     description: Fetches the industryType of specified ID.
 *     tags:
 *       - Industry Type Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Industry Types.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IIndustryTypeMaster'
 */
router.route("/:id").get(industryTypeMasterController.getIndustryTypeMaster);

/**
 * @swagger
 * /api/v1/industryTypes/{id}:
 *   patch:
 *     description: Updates the industryTypes of specified ID.
 *     tags:
 *       - Industry Type Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               industryTypeName:
 *                 type: string
 *                 required: true
 *     responses:
 *       200:
 *         description: industryTypes.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IIndustryTypeMaster'
 */
router.route("/:id").patch(authController.restrictTo("admin"), industryTypeMasterController.updateIndustryTypeMaster);

/**
 * @swagger
 * /api/v1/industryTypes/{id}:
 *   delete:
 *     description: Deletes the industryType of specified ID.
 *     tags:
 *       - Industry Type Master
 *     security:
 *       - bearerAuth: []
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: industryTypes.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/IIndustryTypeMaster'
 */
router.route("/:id").delete(authController.restrictTo("admin"), industryTypeMasterController.deleteIndustryTypeMaster);
