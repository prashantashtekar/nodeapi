# Notes: 
- https://bitsteps.in/swagger/index.html
- Soft delete everywhere.
- Effective date - from / to dates on a few entities which will be again used commonly.
- Tenant aware: For all entities add the "Entity" parent reference, ones which are common we can store the ID = of a common master entity. 
- Audit trail - created_at, updated_at, created_by, updated_by, CUD of each entity old and new values. Here we need to make provision for custom business audit entries, which will be stored in the audit trail but with special types associated. 

# Entity: Represents the company which is going to use the system. 

TODO: Pickup rest of the fields from the swagger. 

- Type: Current types are Insurer, Broker, Agent, Banca (partner financial institutes like banks, NBFCs etc), Corporate Agent 

- Status: Active | In Active | Suspended
- City: child referencing
- District: child referencing
- State: child referencing
- Country: child referencing


# CityMaster
(NonTenantAware)
TODO: Pickup rest of the fields from the swagger. 

# DistrictMaster
(NonTenantAware)
TODO: Pickup rest of the fields from the swagger. 

# StateMaster 
(NonTenantAware)
TODO: Pickup rest of the fields from the swagger. 

# CountryMaster 
(NonTenantAware)
TODO: Pickup rest of the fields from the swagger. 

# PincodeMaster
(NonTenantAware)
Table needs to be created, does not exist in Swagger as of yet. 
Earthquake zone information is at the pincode level. 

# EarthquakeZoneMaster
(NonTenantAware)
Table needs to be created, does not exist in Swagger as of yet. 
Earthquake zone information is at the pincode level. 


# User
(TenantAware)

- Entity: Parent referencing
- Roles: 
    admin
    operations
    insurer-admin
    insurer-underwriter
    insurer-rm (relationship manager)
    broker-admin
    broker-rm
    broker-creator
    broker-approver
    agent
    banca-admin
    banca-rm
    banca-creator
    banca-approver


# ClientGroupMaster
(NonTenantAware)


# ClientKycMaster
(NonTenantAware)
This will only contain KYC related attributes viz. PAN, GST etc. Allowing us to make a common database of companies. A reference of this table will be left on the "Client" collection so that in the future doing any kind of comparison revolving around common "Clients" is easy to do.


# Client
(TenantAware)


# ClientLocation
(TenantAware)

- ClientEntityId
- ... 
- ... 
- Location related fields. 



# EarthquakeRateMaster
- EqZone
- RiskType: industrial | non-industrial 
- Rate: 


# HazardCategoryMaster
(NonTenantAware)
TODO: Need more information on the score and how we will take its impact. 

- Category: Light | Ordinary | High | Extra High
- Score: 


# TerrorismRateMaster
Based on the industry type and the sum insured we will find the rate from the below table.

- IndustryType
- From
- To 
- RatePerMile (the per mile is basically per 1000)


# FreeUptoMaster 
TODO: Understand this later.



# OccupancyMaster
(TenantAware)



# IndustryTypeMaster 
(NonTenantAware)



# DecisionMatrix 
TODO: Needs clarity

# DecisionMatrixHazCat 
(TenantAware)

# DecisionMatrixQualityCat
(TenantAware)

# DecisionMatrixNatCat
(TenantAware)
