process.on("uncaughtException", err => {
  console.log(err.stack);
  console.log("Uncaught Exception, Shutting down!");
  console.log(err.name, err.message);

  // Hard shutdown.
  process.exit(1);
});
import dotenv from "dotenv";
import { install } from "source-map-support";
install();

dotenv.config({ path: "./config.dev.env" });

import { app } from "./app";
import mongoose from "mongoose";

// Get the connection URL
const DATABASE_URL = process.env.DATABASE_URL 
//?.replace("<DATABASE_USER>", process.env.DATABASE_USER).replace("<DATABASE_PASSWORD>", process.env.DATABASE_PASSWORD);
console.log(`Using Database URL: ${DATABASE_URL}`);

mongoose
  .connect(DATABASE_URL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  })
  .then(con => {
    console.log(`Database connection established (changed, again)...`);
  });

// Start the server..
const port = process.env.PORT || 3000;
const server = app.listen(port, () => {
  console.log(`App running on port ${port}...`);
});

process.on("unhandledRejection", (err: any) => {
  console.log(err.name, err.message);
  console.log("Unhandled Rejection, Shutting down!");

  // 0 for success, 1 for failure.
  // Hard shutdown.
  // process.exit(1);

  // Graceful shutdown.
  server.close(() => {
    process.exit(1);
  });
});
// console.log(x);
