import { IUser } from "./models/userModel";

export interface IResponseDto {
    status: string,
    message?: string,
    results?: number,
    data?: {};
    stack?: string,
    error?: any,
}

export interface ITokenResponseDto extends IResponseDto{
    token: string;
    expires: number;
}

/** Used for the async storage. */
export interface IAsyncStoreCtxt {
    tenantId: String;
    currentUser: IUser;
  }
  